using UnityEngine;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

namespace SubUtil
{
    public class CameraSizeController : MonoBehaviour
    {
        private Camera _camera;

        private const float BaseSizeIPad = 2.6f;
        private const float BaseSizeIPhone = 4.0f;
        private const float BaseCoefficientIPad = 2732.0f / 2048.0f;
        private const float BaseCoefficientIPhone = 2778.0f / 1284.0f;

        private readonly Vector2 _sizeQuad = new Vector2(3.9f, 8.4f);

        // Iphone Max = 1284x2778 но сверху и снизу не супер важный контент, больше для фона, так как на айпадах не будет его видно
        // а на айфоне что бы не было серых пятен

        private void Awake()
        {
            _camera = GetComponentInChildren<Camera>();
            GetComponentInChildren<MeshRenderer>().transform.localScale = _sizeQuad;
        }

#if UNITY_EDITOR
        [ExecuteAlways]
        private void Update()
        {

#if UNITY_IOS
        if (Device.generation.ToString().Contains("iPad"))
        {
            float newCoefficient = (float)Screen.height / Screen.width / BaseCoefficientIPad;
            _camera.orthographicSize = BaseSizeIPad * newCoefficient;
        }
        else
        {
            float newCoefficient = (float)Screen.height / Screen.width / BaseCoefficientIPhone;
            _camera.orthographicSize = BaseSizeIPhone * newCoefficient;
        }
#else
            float newCoefficient = (float) Screen.height / Screen.width;
            if (newCoefficient > 1.5f)
            {
                newCoefficient /= BaseCoefficientIPhone;
                _camera.orthographicSize = BaseSizeIPhone * newCoefficient;
            }
            else
            {
                newCoefficient /= BaseCoefficientIPad;
                _camera.orthographicSize = BaseSizeIPad * newCoefficient;
            }
#endif
        }
#endif

        private void OnEnable()
        {
#if UNITY_IOS
        if (Device.generation.ToString().Contains("iPad"))
        {
            float newCoefficient = (float)Screen.height / Screen.width / BaseCoefficientIPad;
            _camera.orthographicSize = BaseSizeIPad * newCoefficient;
        }
        else
        {
            float newCoefficient = (float)Screen.height / Screen.width / BaseCoefficientIPhone;
            _camera.orthographicSize = BaseSizeIPhone * newCoefficient;
        }
#else
            float newCoefficient = (float) Screen.height / Screen.width;
            if (newCoefficient > 1.5f)
            {
                newCoefficient /= BaseCoefficientIPhone;
                _camera.orthographicSize = BaseSizeIPhone * newCoefficient;
            }
            else
            {
                newCoefficient /= BaseCoefficientIPad;
                _camera.orthographicSize = BaseSizeIPad * newCoefficient;
            }
#endif
        }
    }
}
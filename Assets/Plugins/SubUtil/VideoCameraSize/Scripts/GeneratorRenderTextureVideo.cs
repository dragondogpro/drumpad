using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace SubUtil
{
    public class GeneratorRenderTextureVideo : MonoBehaviour
    {
        [SerializeField] private RawImage _subVideoLblk;
        [SerializeField] private VideoClip _videoClipIphonDrMpD;
        [SerializeField] private VideoClip _videoClipIpadDrMpD;
        private Material _renderMaterialLblk;

        private RenderTexture _textureLblk;
    
        private VideoPlayer _videoPlayerLblk;

        private void Awake()
        {
            _videoPlayerLblk = GetComponent<VideoPlayer>();
            
            _videoPlayerLblk.clip = Screen.width < 1500
                ? _videoClipIphonDrMpD
                : _videoClipIpadDrMpD;
            
            _renderMaterialLblk = GetComponentInChildren<MeshRenderer>().material;
        }

        private void Start()
        {
            _textureLblk = new RenderTexture(1284, 2778, 24);
            _videoPlayerLblk.targetTexture = _textureLblk;
            _renderMaterialLblk.SetTexture("_MainTex", _textureLblk);
            _subVideoLblk.texture = _textureLblk;
        }

        private void OnApplicationFocus(bool focusLblk)
        {
            Application.focusChanged += inFocus =>
            {
                if (inFocus)
                {
                    _videoPlayerLblk.Play();
                }
                else
                {
                    _videoPlayerLblk.Pause();
                }
            };
        }
        
        private void OnDisable()
        {
            _textureLblk.DiscardContents();
        }
    }
}

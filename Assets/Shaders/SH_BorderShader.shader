Shader "Drum Pad/Border Color"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _ColorMain ("Color", Color) = (1,1,1,1)
        _BorderTex ("Texture", 2D) = "white" {}
        _ColorBorder ("Color", Color) = (1,1,1,1)
        
        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 1
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255
 
        _ColorMask ("Color Mask", Float) = 15

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        
        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass replace
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            fixed4 _ColorMain;
            fixed4 _ColorBorder;
            sampler2D _MainTex;
            sampler2D _BorderTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                const fixed4 colMain = tex2D(_MainTex, i.uv);
                const fixed4 colBorder = tex2D(_BorderTex, i.uv);
                fixed4 innerColorMask = 1 - colBorder;
                innerColorMask *= colMain;

                innerColorMask *= _ColorMain;
                fixed4 result = colBorder * _ColorBorder;
                result += innerColorMask;

                return result;
            }
            ENDCG
        }
    }
}

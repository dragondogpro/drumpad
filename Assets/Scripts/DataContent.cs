using System;
using Newtonsoft.Json;
using UnityEngine;

namespace DrumPad
{
    [Serializable]
    public class DataContent
    {
        public string KindName { get; set; }
        public string StyleName { get; set; }
        
        public int NormalBpm { get; set; }

        [JsonIgnore] public Sprite SpriteElement { get; set; }
    }
}
using DrumPad.JsonBlock;
using DrumPad.Utilits;
using Zenject;

namespace DrumPad
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<SpriteAtlasHolder>().To<SpriteAtlasHolder>().AsSingle().NonLazy();
            Container.Bind<JsonManagerDrMpD>().To<JsonManagerDrMpD>().AsSingle().NonLazy();
        }
    }
}
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.MoreBlock
{
    public abstract class ButtonClickElementAbs : MonoBehaviour, IInitialization
    {
        private Button _buttonClickDrMpD;

        public virtual void InitializationDrMpD()
        {
            _buttonClickDrMpD = GetComponent<Button>();
            if(_buttonClickDrMpD != null)
                _buttonClickDrMpD.onClick.AddListener(ClickButtonDrMpD);
        }

        protected abstract void ClickButtonDrMpD();
    }
}
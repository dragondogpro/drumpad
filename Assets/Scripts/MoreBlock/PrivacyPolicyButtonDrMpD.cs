using System;
using System.Collections;
using DrumPad.Utilits;
using IOSBridge;
using UnityEngine;
using UnityEngine.Networking;

namespace DrumPad.MoreBlock
{
    public class PrivacyPolicyButtonDrMpD : MonoBehaviour
    {
        private const string PRIVACYDrMpD = "https://docs.google.com/document/d/1fiN9FgPwAOaXCiMVpFFZJfEcD7kZybAh2360vIeRbbg/edit?usp=sharing";
        private const string TERMSDrMpD = "https://docs.google.com/document/d/1kRs3JsdWXYuj-yxtL-OlBgjBrgxqi-4_drxrGQOig3g/edit?usp=sharing";
        
        public void ClickButtonPrivacy()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            CallBackPrivacyPolicy(StringHelperDrMpD.PrivacyDrMpD);
            //IOSMethodHelper.CreateActionSheet(gameObject.name, "CallBackPrivacyPolicy", arrAction);
        }
        
        public void ClickButtonTerms()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            CallBackPrivacyPolicy(StringHelperDrMpD.TermsDrMpD);
            //IOSMethodHelper.CreateActionSheet(gameObject.name, "CallBackPrivacyPolicy", arrAction);
        }

        private void CallBackPrivacyPolicy(string keyDrMpD)
        {
            string openUrlDrMpD = "";

            if (keyDrMpD == StringHelperDrMpD.PrivacyDrMpD)
            {
                openUrlDrMpD = PRIVACYDrMpD;
            }
            
            if (keyDrMpD == StringHelperDrMpD.TermsDrMpD)
            {
                openUrlDrMpD = TERMSDrMpD;
            }
            
            print("ReceiveDrMpD: " + openUrlDrMpD);

            StartCoroutine(CheckInternetConnectionRoutineDrMpD(StatusConnection));
            
            void StatusConnection(bool isSuccessDrMpD)
            {
                if(isSuccessDrMpD)
                    Application.OpenURL(openUrlDrMpD);
                else
                    IOStoUnityBridge.ShowAlert("ErrorDrMpD", "Connect your device to internet!");
            }
        }
        
        private IEnumerator CheckInternetConnectionRoutineDrMpD(Action<bool> actionDrMpD)
        {
            string urlDrMpD = "https://google.com/DRMPD";
            UnityWebRequest wwwUrlPvpNewDrMpD = UnityWebRequest.Get(urlDrMpD.Substring(0,19));

            yield return wwwUrlPvpNewDrMpD.SendWebRequest();
            
            if (wwwUrlPvpNewDrMpD.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError(wwwUrlPvpNewDrMpD.error);
                actionDrMpD?.Invoke(false);
            }
            else
            {
                actionDrMpD?.Invoke(true);
            }
            
            wwwUrlPvpNewDrMpD.Dispose();
        }
    }
}
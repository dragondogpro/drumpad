using UnityEngine;

namespace DrumPad.MoreBlock
{
    public class MoreManager : MonoBehaviour, IManager
    {
        public void InitializationDrMpD()
        {
            foreach (var btnChildDrMpD in GetComponentsInChildren<ButtonClickElementAbs>())
            {
                btnChildDrMpD.InitializationDrMpD();
            }
        }
    }
}
using DrumPad.JsonBlock;
using DrumPad.Styles;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.Library_Block
{
    public class MusicStyleHorizontalBlock : MonoBehaviour
    {
        [SerializeField] private MusicStyleElement stylePrefab;
        [SerializeField] private Text titleTextBlockName;
        [SerializeField] private Button seeAllButton;
        
        public void InitBlockElements(JsonStyleListElement styleListElement)
        {
            seeAllButton.onClick.AddListener(OnSeeAllClick);
            RectTransform rootRect = GetComponent<ScrollRect>().content;
            titleTextBlockName.text = styleListElement.KindOfStyle;

            foreach (var styleDrMpD in styleListElement.Styles)
            {
                string kindStyle = $"{styleListElement.KindOfStyle}/{styleDrMpD}";
                // string[] dataSplit = style.Split('/');
                // int normalBpm = 0;
                // if (int.TryParse(dataSplit[^1], out int result))
                //     normalBpm = result;
                
                // DataContent dataContent = new DataContent
                // {
                //     KindName = styleListElement.KindOfStyle,
                //     StyleName = dataSplit[0],
                //     NormalBpm = normalBpm
                // };

                MusicStyleElement styleMusicElement = Instantiate(stylePrefab, rootRect);
                styleMusicElement.InitOne();
                DataContent dataContentDrMpD = GameManagerDrum.GaM.DataContentsMap[styleListElement.KindOfStyle]
                    .GetElement(styleDrMpD.Split('/')[0]);
                
                styleMusicElement.InitElementDrMpD(dataContentDrMpD);

                if (!GameManagerDrum.GaM.JsonManagerDrMpD.MusicLessonsListJson.LessonJsons.ContainsKey(kindStyle))
                {
                    GameManagerDrum.GaM.JsonManagerDrMpD.MusicLessonsListJson.LessonJsons.Add(kindStyle, new MusicLessonsJson(5));
                }
            }

            stylePrefab = null;
            GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsonFileDrMpD();
        }

        private void OnSeeAllClick()
        {
            GameManagerDrum.GaM.LibraryManager.SeeAllSelect(titleTextBlockName.text);
        }
    }
}
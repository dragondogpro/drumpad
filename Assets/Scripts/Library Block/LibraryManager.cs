using DrumPad.Styles;
using UnityEngine;

namespace DrumPad.Library_Block
{
    public class LibraryManager : MonoBehaviour, IManager, IInitialization
    {
        [SerializeField] private DetailBlockMusicStyle detailBlockMusicStyle;
        [SerializeField] private RecentMusicLessonsManager recentMusicLessonsManager;
        [SerializeField] private StyleMusicManager styleMusicManager;
        [SerializeField] private SeeAllKind seeAllKind;

        public RecentMusicLessonsManager RecMusLessMan => recentMusicLessonsManager;

        public void InitializationDrMpD()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            detailBlockMusicStyle.InitializationDrMpD();
            recentMusicLessonsManager.InitializationDrMpD();
            styleMusicManager.InitializationDrMpD();
            seeAllKind.InitializationDrMpD();
        }

        public void SelectMusicElement(DataContent dataContentDrMpD, Sprite titleImageDrMpD)
        {
            detailBlockMusicStyle.gameObject.SetActive(true);
            detailBlockMusicStyle.SelectStyleMusic(dataContentDrMpD, titleImageDrMpD);
        }

        public void SeeAllSelect(string kindDrMpD)
        {
            seeAllKind.gameObject.SetActive(true);
            styleMusicManager.gameObject.SetActive(false);
            recentMusicLessonsManager.gameObject.SetActive(false);
            
            seeAllKind.OpenSeeAll(kindDrMpD);
        }

        public void CloseSeeAll()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            seeAllKind.gameObject.SetActive(false);
            styleMusicManager.gameObject.SetActive(true);
            recentMusicLessonsManager.gameObject.SetActive(true);
        }
    }
}
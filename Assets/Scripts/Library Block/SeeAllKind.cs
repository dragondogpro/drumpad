using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DrumPad.Library_Block
{
    public class SeeAllKind : MonoBehaviour, IInitialization
    {
        [SerializeField] private Text textTitleDrMpD;
        [SerializeField] private Button buttonReturnDrMpD;
        
        private ScrollRect _scrollRectDrMpD;
        private MusicStyleElement[] _allElements;
        
        public void InitializationDrMpD()
        {
            _scrollRectDrMpD = GetComponent<ScrollRect>();
            buttonReturnDrMpD.onClick.AddListener(OnReturnClick);
            GlobalEventSystem.OnClickButtonWindow += GlobalEventSystemOnOnClickButtonWindow;

            foreach (var style in _allElements = GetComponentsInChildren<MusicStyleElement>())
                style.InitOne();
            
            gameObject.SetActive(false);
        }

        public void OpenSeeAll(string kindDrMpD)
        {
            _scrollRectDrMpD.verticalScrollbar.value = 1.0f;
            
            textTitleDrMpD.text = kindDrMpD;
            
            int countActive = GameManagerDrum.GaM.DataContentsMap[kindDrMpD].DataMap.Values.Count;
            int countDrMpD = 0;

            foreach (var styleDrMpD in _allElements)
                styleDrMpD.gameObject.SetActive(false);
            
            foreach (var dataDrMpD in GameManagerDrum.GaM.DataContentsMap[kindDrMpD].DataMap.Values)
            {
                _allElements[countDrMpD].gameObject.SetActive(countDrMpD < countActive);
                if(countDrMpD < countActive)
                    _allElements[countDrMpD].InitElementDrMpD(dataDrMpD);

                countDrMpD++;
            }
        }
        
        private void GlobalEventSystemOnOnClickButtonWindow(WindowEnumPad objDrMpD)
        {
            if (objDrMpD != null)
            {
                int kDrMpD;
                kDrMpD = 0;
            }

            OnReturnClick();
        }
        
        private void OnReturnClick()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            GameManagerDrum.GaM.LibraryManager.CloseSeeAll();
        }
    }
}
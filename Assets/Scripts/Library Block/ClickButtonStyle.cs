using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.Library_Block
{
    public class ClickButtonStyle : MonoBehaviour
    {
        private Button _clickStyleButton;
        private Image _imageStyleButton;
        private Color _startColorDrMpD;

        private bool _isActiveColor = true;
        
        private void Awake()
        {
            _clickStyleButton = GetComponent<Button>();
            _imageStyleButton = _clickStyleButton.image;
            _startColorDrMpD = _imageStyleButton.color;
            
            _clickStyleButton.onClick.AddListener(OnChangeColorClick);
            
            OnChangeColorClick();
        }

        private void OnChangeColorClick()
        {
            _isActiveColor = !_isActiveColor;

            _imageStyleButton.color = _isActiveColor ? _startColorDrMpD : Color.white;
        }
    }
}
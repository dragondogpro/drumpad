using DrumPad.JsonBlock;
using DrumPad.Utilits;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.Library_Block
{
    public class RecentLessonElement : MonoBehaviour
    {
        [SerializeField] private Text _titleName;
        [SerializeField] private Text _lessonCompleteCount;
        
        private Image _titleImage;

        private Button _clickButtonImage;

        private DataContent _dataContentDrMpD;
        private MusicLessonsJson _lessonLessonsJson;
        
        public void InitLesson(MusicLessonsJson lessonDrMpD, DataContent dataContentDrMpD)
        {
            _dataContentDrMpD = dataContentDrMpD;
            _lessonLessonsJson = lessonDrMpD;

            UpdateElement();

            _lessonLessonsJson.OnUpdateElement += UpdateElement;

            _titleName.text = _dataContentDrMpD.StyleName;

            _clickButtonImage = GetComponent<Button>();
            _clickButtonImage.onClick.AddListener(OnClickLesson);
            
            _titleImage = _clickButtonImage.image;
            _titleImage.sprite = _dataContentDrMpD.SpriteElement;
        }

        private void UpdateElement()
        {
            int lessonComplete = 0;
            foreach (var t in _lessonLessonsJson.lessons)
            {
                if (t.doneLesson)
                    lessonComplete++;
            }
            _lessonCompleteCount.text = $"{lessonComplete}/{_lessonLessonsJson.lessons.Length} Lessons";
        }

        private void OnClickLesson()
        {
            GlobalEventSystem.ClickButtonWindow(WindowEnumPad.MYMUSIC);
            // GlobalEventSystem.SelectNewStyle(_dataContent);
            MusicPackElement musicPackElement = new MusicPackElement();
            musicPackElement.InitElementBlock(_dataContentDrMpD);
            GameManagerDrum.GaM.MyMusicManager.RecordManager.SelectMusicStyle(musicPackElement);
            GameManagerDrum.GaM.DownPanelButtonManager.OpenCloseButton(false);
            GameManagerDrum.GaM.MyMusicManager.ClickLessonsButton(true);
        }
    }
}
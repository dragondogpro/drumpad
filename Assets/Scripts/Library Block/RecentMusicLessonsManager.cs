using System.Collections.Generic;
using DrumPad.JsonBlock;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.Library_Block
{
    public class RecentMusicLessonsManager : MonoBehaviour,IManager
    {
        [SerializeField] private RecentLessonElement recentLessonPrefab;
        [SerializeField] private ScrollRect scrollRectElement;
        [SerializeField] private GameObject blockElement;

        private readonly List<DataContent> _recentCurrentDatas = new ();

        public void InitializationDrMpD()
        {
            foreach (var dataDrMpD in GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsMusicList.recentMusicStyles)
            {
                RecentLessonElement elemLess = Instantiate(recentLessonPrefab, scrollRectElement.content);
                DataContent dataContentDrMpD = GameManagerDrum.GaM.DataContentsMap[dataDrMpD.KindName].GetElement(dataDrMpD.StyleName);
                elemLess.InitLesson(GetLessons(dataContentDrMpD), dataContentDrMpD);
            }
            
            blockElement.SetActive(_recentCurrentDatas.Count == 0);
            scrollRectElement.gameObject.SetActive(_recentCurrentDatas.Count != 0);
        }
        
        public void UpdateRecentList(DataContent dataContentDrMpD)
        {
            foreach (var dataDrMpD in _recentCurrentDatas)
            {
                if(dataDrMpD.StyleName == dataContentDrMpD.StyleName) return;
            }

            RecentLessonElement elemLessDrMpD = Instantiate(recentLessonPrefab, scrollRectElement.content);
            elemLessDrMpD.InitLesson(GetLessons(dataContentDrMpD), dataContentDrMpD);
            
            blockElement.SetActive(_recentCurrentDatas.Count == 0);
            scrollRectElement.gameObject.SetActive(_recentCurrentDatas.Count != 0);
        }

        private MusicLessonsJson GetLessons(DataContent dataContentDrMpD)
        {
            _recentCurrentDatas.Add(dataContentDrMpD);
            
            string dataKeyDrMpD = $"{dataContentDrMpD.KindName}/{dataContentDrMpD.StyleName}/{dataContentDrMpD.NormalBpm}";
            MusicLessonsJson returnLessons = null;
            if (GameManagerDrum.GaM.JsonManagerDrMpD.MusicLessonsListJson.LessonJsons.ContainsKey(dataKeyDrMpD))
                returnLessons = GameManagerDrum.GaM.JsonManagerDrMpD.MusicLessonsListJson.LessonJsons[dataKeyDrMpD];

            return returnLessons;
        }
    }
}
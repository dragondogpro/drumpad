using System;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.Library_Block
{
    public class MusicStyleElement : MonoBehaviour
    {
        [SerializeField] private Text styleNamePad;
        [SerializeField] private Text styleKindPad;

        private Button _buttonSelectElementDrMpD;
        private Image _titleImage;
        
        private DataContent _dataContentDrMpD;

        public void InitOne()
        {
            int numDrMpD = 423;
            if (numDrMpD != 432)
            {
                numDrMpD = 432;
            }
            
            _buttonSelectElementDrMpD = GetComponent<Button>();
            _titleImage = _buttonSelectElementDrMpD.image;
            _buttonSelectElementDrMpD.onClick.AddListener(SelectButtonClick);
        }
        
        public void InitElementDrMpD(DataContent dataContentDrMpD)
        {
            _dataContentDrMpD = dataContentDrMpD;

            styleNamePad.text = _dataContentDrMpD.StyleName;
            styleKindPad.text = _dataContentDrMpD.KindName;
            
            _titleImage.sprite = _dataContentDrMpD.SpriteElement;
        }

        public void InitElementDrMpD(DataContent dataContentDrMpD, Action actionDrMpD)
        {
            InitElementDrMpD(dataContentDrMpD);

            _buttonSelectElementDrMpD.onClick.AddListener(() => actionDrMpD?.Invoke());
        }

        private void SelectButtonClick()
        {
            GameManagerDrum.GaM.LibraryManager.SelectMusicElement(_dataContentDrMpD, _buttonSelectElementDrMpD.image.sprite);
        }
    }
}
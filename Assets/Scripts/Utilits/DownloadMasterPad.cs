using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace DrumPad.Utilits
{
    public static class DownloadMasterPad
    {
        public static async void LoadTextFile(string urlDownloadDrMpD, string fileNameDrMpD, Action<string> endLoadDrMpD)
        {
            UnityWebRequest loadFileDrMpD = UnityWebRequest.Get(urlDownloadDrMpD);

            loadFileDrMpD.SendWebRequest();
            
            while (!loadFileDrMpD.isDone)
            {
                await Task.Delay(20);
            }

            if (loadFileDrMpD.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError(loadFileDrMpD.error);
            }
            else
            {
                string pathToFolderDrMpD = Path.Combine(ConstantHolderPad.GetRootPath(), ConstantHolderPad.ROOT_FOLDER_STYLES);
                if (!Directory.Exists(pathToFolderDrMpD))
                    Directory.CreateDirectory(pathToFolderDrMpD);
                
                pathToFolderDrMpD = Path.Combine(pathToFolderDrMpD, fileNameDrMpD);
                if (!File.Exists(pathToFolderDrMpD))
                {
                    await File.WriteAllTextAsync(pathToFolderDrMpD, loadFileDrMpD.downloadHandler.text);
                }

                endLoadDrMpD?.Invoke(loadFileDrMpD.downloadHandler.text);
            }
            
            loadFileDrMpD.Dispose();
        }
        
        public static async void NotSafeLoadTextFile(string urlDownloadDrMpD, Action<string> endLoadDrMpD)
        {
            UnityWebRequest loadFileDrMpD = UnityWebRequest.Get(urlDownloadDrMpD);

            loadFileDrMpD.SendWebRequest();
            
            while (!loadFileDrMpD.isDone)
            {
                await Task.Delay(20);
            }

            if (loadFileDrMpD.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError(loadFileDrMpD.error);
            }
            else
            {
                endLoadDrMpD?.Invoke(loadFileDrMpD.downloadHandler.text);
            }
            
            loadFileDrMpD.Dispose();
        }
        
        public static async Task LoadTextFile(DataContent dataContentDrMpD, string fileNameDrMpD)
        {
            string urlDownloadDrMpD = ConstantHolderPad.ROOT_FOLDER_ON_SERVER + dataContentDrMpD.KindName + "/" + dataContentDrMpD.StyleName;
            UnityWebRequest loadFileDrMpD = UnityWebRequest.Get(urlDownloadDrMpD + "/" + fileNameDrMpD);

            loadFileDrMpD.SendWebRequest();

            while (!loadFileDrMpD.isDone)
            {
                await Task.Delay(20);
            }
            
            if (loadFileDrMpD.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError(loadFileDrMpD.error);
            }
            else
            {
                string packDirectory = PathToFolder(dataContentDrMpD);
                if(!File.Exists(packDirectory + "/" + fileNameDrMpD))
                    await File.WriteAllTextAsync(packDirectory + "/" + fileNameDrMpD,loadFileDrMpD.downloadHandler.text);
            }
            
            loadFileDrMpD.Dispose();
        }


        public static IEnumerator GetFileSizeDrMpD(DataContent dataContentDrMpD, Action<double> resultDrMpD)
        {
            double resultSizeDrMpD = 0;

            string urlDownloadADrMpD = ConstantHolderPad.ROOT_FOLDER_ON_SERVER + dataContentDrMpD.KindName + "/" +
                                       dataContentDrMpD.StyleName;
            UnityWebRequest uwrDrMpD = UnityWebRequest.Head(urlDownloadADrMpD + "/Side A.zip");
            yield return uwrDrMpD.SendWebRequest();
            string sizeDrMpD = uwrDrMpD.GetResponseHeader("Content-Length");

            if (uwrDrMpD.isNetworkError || uwrDrMpD.isHttpError)
            {
                Debug.Log("Error While Getting Length: " + uwrDrMpD.error);
                if (resultDrMpD != null)
                    resultDrMpD(-1);
            }
            else
            {
                if (resultDrMpD != null)
                    resultSizeDrMpD += Convert.ToDouble(sizeDrMpD);
            }

            uwrDrMpD.Dispose();

            UnityWebRequest uwrBDrMpD = UnityWebRequest.Head(urlDownloadADrMpD + "/Side B.zip");
            yield return uwrBDrMpD.SendWebRequest();
            string sizeBDrMpD = uwrBDrMpD.GetResponseHeader("Content-Length");

            if (uwrBDrMpD.isNetworkError || uwrBDrMpD.isHttpError)
            {
                Debug.Log("Error While Getting Length: " + uwrBDrMpD.error);
                if (resultDrMpD != null)
                    resultDrMpD(-1);
            }
            else
            {
                if (resultDrMpD != null)
                    resultSizeDrMpD += Convert.ToDouble(sizeBDrMpD);
            }
            
            uwrBDrMpD.Dispose();
            
            resultDrMpD(resultSizeDrMpD);
        }

        public static async void LoadMusicPack(DataContent dataContentDrMpD, Action<float> loadProgressDrMpD, Action endLoadDrMpD)
        {
            string urlDownloadDrMpD = ConstantHolderPad.ROOT_FOLDER_ON_SERVER + dataContentDrMpD.KindName + "/" + dataContentDrMpD.StyleName;
            UnityWebRequest loadFileABlockDrMpD = new UnityWebRequest(urlDownloadDrMpD + "/Side A.zip")
            {
                downloadHandler = new DownloadHandlerBuffer()
            };
            
            loadFileABlockDrMpD.SendWebRequest();

            while (!loadFileABlockDrMpD.isDone)
            {
                loadProgressDrMpD?.Invoke(loadFileABlockDrMpD.downloadProgress / 2.0f);

                await Task.Delay(30);
            }
            
            if (loadFileABlockDrMpD.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError(loadFileABlockDrMpD.error);
            }
            else
            {
                byte[] dataPackDrMpD = loadFileABlockDrMpD.downloadHandler.data;

                string packDirectoryDrMpD = PathToFolder(dataContentDrMpD);

                string pathToFolderDrMpD = Path.Combine(packDirectoryDrMpD, "Side A.zip");
                if (!File.Exists(pathToFolderDrMpD))
                {
                    await File.WriteAllBytesAsync(pathToFolderDrMpD, dataPackDrMpD);
                }
                ZipFile.ExtractToDirectory(pathToFolderDrMpD, packDirectoryDrMpD);
                File.Delete(pathToFolderDrMpD);
            }
            
            UnityWebRequest loadFileBBlock = new UnityWebRequest(urlDownloadDrMpD + "/Side B.zip")
            {
                downloadHandler = new DownloadHandlerBuffer()
            };
            loadFileBBlock.SendWebRequest();

            while (!loadFileBBlock.isDone)
            {
                loadProgressDrMpD?.Invoke(loadFileBBlock.downloadProgress / 2.0f + 0.5f);
                await Task.Delay(30);
            }
            
            if (loadFileBBlock.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError(loadFileBBlock.error);
            }
            else
            {
                byte[] dataPack = loadFileBBlock.downloadHandler.data;

                string packDirectory = PathToFolder(dataContentDrMpD);

                string pathToFolderDrMpD = Path.Combine(packDirectory, "Side B.zip");
                
                if (!File.Exists(pathToFolderDrMpD))
                {
                    await File.WriteAllBytesAsync(pathToFolderDrMpD, dataPack);
                }
                ZipFile.ExtractToDirectory(pathToFolderDrMpD, packDirectory);
                File.Delete(pathToFolderDrMpD);
            }
            
            endLoadDrMpD?.Invoke();
            loadFileABlockDrMpD.Dispose();
        }
        
        public static async Task LoadSpriteStyle(DataContent dataContentDrMpD)
        {
            string urlDownloadDrMpD = ConstantHolderPad.ROOT_FOLDER_ON_SERVER + dataContentDrMpD.KindName;
            
            urlDownloadDrMpD += "/" + dataContentDrMpD.StyleName;
            urlDownloadDrMpD += "/sprite.png";
            
            UnityWebRequest loadFileDrMpD = new UnityWebRequest(urlDownloadDrMpD)
            {
                downloadHandler = new DownloadHandlerTexture()
            };

            loadFileDrMpD.SendWebRequest();

            while (!loadFileDrMpD.isDone)
            {
                await Task.Delay(100);
            }

            if (loadFileDrMpD.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(urlDownloadDrMpD);
                Debug.LogError(loadFileDrMpD.error);
            }
            else
            {
                byte[] dataPack = loadFileDrMpD.downloadHandler.data;
                string pathToFolderDrMpD = PathToFolder(dataContentDrMpD);

                pathToFolderDrMpD = Path.Combine(pathToFolderDrMpD, "sprite.png");
                CropEncodedImageDrMpD(dataPack, pathToFolderDrMpD);
                
                dataContentDrMpD.SpriteElement = LoadSpriteFromFolder(dataContentDrMpD);
            }
            
            Debug.Log("Complete!");
            loadFileDrMpD.Dispose();
        }

        public static bool FileExist(DataContent dataContentDrMpD, string fileNameDrMpD)
        {
            return File.Exists(Path.Combine(PathToFolder(dataContentDrMpD), fileNameDrMpD));
        }
        
        public static Sprite LoadSpriteFromFolder(DataContent dataContentDrMpD)
        {
            byte[] spriteData = File.ReadAllBytes(PathToFolder(dataContentDrMpD) + "/sprite.png");
            Texture2D spriteTex = new Texture2D(1, 1);
            spriteTex.LoadImage(spriteData);
            spriteTex.Compress(false);
            Sprite resultSprite = Sprite.Create(spriteTex, new Rect(0.0f, 0.0f, spriteTex.width, spriteTex.height), new Vector2(0.0f, 0.0f));
            return resultSprite;
        }

        private static void CropEncodedImageDrMpD(byte[] data, string pathToSave)
        {
            Func<Texture2D, byte[]> encodeActionDrMpD = texture2D => texture2D.EncodeToPNG();
         
            Texture2D textureDrMpD = new Texture2D(2, 2, TextureFormat.RGB24, false);

            textureDrMpD.LoadImage(data);
            
            // если при делении на 4 высоты или ширины текстуры есть остатакок
            if (textureDrMpD.width % 4 != 0 || textureDrMpD.height % 4 != 0) 
            {
                //отнимаем остаток от ширины
                int sizeXShadersDrMpD = textureDrMpD.width - textureDrMpD.width % 4; 
                //отнимаем остаток от высоты
                int sizeYShadersDrMpD = textureDrMpD.height - textureDrMpD.height % 4; 
                //сохраняем пиксели текстуры размером кратным 4
                var newPixelsDrMpD = textureDrMpD.GetPixels(0, 0, sizeXShadersDrMpD, sizeYShadersDrMpD); 
                //меняем размер текстуры кратным 4
                textureDrMpD.Reinitialize(sizeXShadersDrMpD, sizeYShadersDrMpD);
                //перезаписываем пиксели
                textureDrMpD.SetPixels(newPixelsDrMpD); 
                //сохраняем изменения
                textureDrMpD.Apply(false, false);
            }

            byte[] resultDrMpD = encodeActionDrMpD.Invoke(textureDrMpD);

            if(!File.Exists(pathToSave))
                File.WriteAllBytesAsync(pathToSave, resultDrMpD);
        }

        public static string PathToFolder(DataContent dataContentDrMpD)
        {
            string pathToFolderDrMpD = Path.Combine(ConstantHolderPad.GetRootPath(), ConstantHolderPad.ROOT_FOLDER_STYLES);
            if (!Directory.Exists(pathToFolderDrMpD))
                Directory.CreateDirectory(pathToFolderDrMpD);

            pathToFolderDrMpD = Path.Combine(pathToFolderDrMpD, dataContentDrMpD.KindName);
                
            if (!Directory.Exists(pathToFolderDrMpD))
                Directory.CreateDirectory(pathToFolderDrMpD);

            pathToFolderDrMpD = Path.Combine(pathToFolderDrMpD, dataContentDrMpD.StyleName);
                
            if (!Directory.Exists(pathToFolderDrMpD))
                Directory.CreateDirectory(pathToFolderDrMpD);

            return pathToFolderDrMpD;
        }
    }
}
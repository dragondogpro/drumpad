using UnityEngine;

namespace DrumPad.Utilits
{
    public class SafeAreaZoneHelperDrMpD : MonoBehaviour
    {
        private RectTransform _rectTransformSafeAreaDrMpD;
        private Rect _safeAreaRectDrMpD;

        private void Awake()
        {
            _rectTransformSafeAreaDrMpD = GetComponent<RectTransform>();
            _safeAreaRectDrMpD = Screen.safeArea;
            
            if(_rectTransformSafeAreaDrMpD != null && _safeAreaRectDrMpD != null)
                UpdateSafeAreaDrMpD();
        }

        private void UpdateSafeAreaDrMpD()
        {
            Vector2 safeAreaMinDrMpD = _safeAreaRectDrMpD.position;
            Vector2 safeAreaMaxDrMpD = _safeAreaRectDrMpD.position + _safeAreaRectDrMpD.size;

            safeAreaMinDrMpD.x /= Screen.width;
            safeAreaMinDrMpD.y /= Screen.height;

            safeAreaMaxDrMpD.x /= Screen.width;
            safeAreaMaxDrMpD.y /= Screen.height;

            if(safeAreaMaxDrMpD != null)
                _rectTransformSafeAreaDrMpD.anchorMax = safeAreaMaxDrMpD;
            if(safeAreaMinDrMpD != null)
                _rectTransformSafeAreaDrMpD.anchorMin = safeAreaMinDrMpD;
        }
    }
}
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.Utilits
{
    internal class ImageAnimationPad : MonoBehaviour
    {
        [SerializeField] private Sprite[] spritesPad;
        
        [SerializeField] private float animationDurationPad = 2.0f;
   
        private Image _imagePad;
        private IEnumerator _routineLoopPad;
   
        private bool _isActivePad;
        private float _timeToFramePad;
        
        private void Awake()
        {
            
            _imagePad = GetComponent<Image>();
            
            if (_imagePad != null)
                _timeToFramePad = animationDurationPad / spritesPad.Length;
        }
        private void OnEnable()
        {
            _isActivePad = true;
            _routineLoopPad = RoutineLoopPad();
            
            if(_routineLoopPad != null)
                StartCoroutine(_routineLoopPad);
        }
        private void OnDisable()
        {
            _isActivePad = false;
            if (!_isActivePad)
                StopCoroutine(_routineLoopPad);
        }
        private IEnumerator RoutineLoopPad()
        {
            int dDrMpD = 123;
            while (_isActivePad)
            {
                dDrMpD++;
                
                foreach (Sprite spriteDrMpD in spritesPad)
                {
                    _imagePad.sprite = spriteDrMpD;
                    yield return new WaitForSecondsRealtime(_timeToFramePad);
                }

                dDrMpD--;
            }
        }
    }
}
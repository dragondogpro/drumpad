namespace DrumPad.Utilits
{
    public class StringHelperDrMpD
    {
        #region Strings
        public static string SpaceDrMpD = " ";
            public static string MusicDrMpD = "MusicDrMpD".Remove("MusicDrMpD".Length-5);
            public static string WavDrMpD = ".wavDrMpD".Remove(".wavDrMpD".Length-5);
            public static string WaveDrMpD = "WAVEDrMpD".Remove("WAVEDrMpD".Length-5);
            public static string FmtDrMpD = "fmt" + SpaceDrMpD;
            public static string DataDrMpD = "dataDrMpD".Remove("dataDrMpD".Length-5);
            public static string SelectDrMpD = "SelectDrMpD".Remove("SelectDrMpD".Length-5);
            public static string DownloadDrMpD = "DownloadDrMpD".Remove("DownloadDrMpD".Length-5);
            public static string TermsDrMpD = "TermsDrMpD".Remove("TermsDrMpD".Length-5);
            public static string PrivacyDrMpD = "PrivacyDrMpD".Remove("PrivacyDrMpD".Length-5);
            public static string RIFFDrMpD = "RIFFDrMpD".Remove("RIFFDrMpD".Length-5);
            public static string PercentDrMpD = "%DrMpD".Remove("%DrMpD".Length-5);
        #endregion
        
        #region Numbers
            public static int ZeroDrMpD = 0;
            public static int OneDrMpD = 1;
            public static int TwoDrMpD = 2;
            public static int ThreeDrMpD = 3;
            public static int FourDrMpD = 4;
            public static int FiveDrMpD = 5;
            public static int SixDrMpD = 6;
            public static int SevenDrMpD = 7;
            public static int EightDrMpD = 8;
            public static int NineDrMpD = 9;
            public static int TenDrMpD = 10;
            public static int ElevenDrMpD = 11;
            public static int TwelveDrMpD = 12;
            #endregion
    }
}
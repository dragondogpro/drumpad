using System;
using System.IO;
using System.Text;
using UnityEngine;

namespace DrumPad.Utilits
{
	public static class WavUtility
	{
		// Force save as 16-bit .wav
		private const int BlockSize_16Bit = 2;
		private static string pathDrMpD;

		/// <summary>
		/// Load PCM format *.wav audio file (using Unity's Application data path) and convert to AudioClip.
		/// </summary>
		/// <returns>The AudioClip.</returns>
		/// <param name="filePathDrMpD">Local file path to .wav file</param>
		public static AudioClip ToAudioClip(string filePathDrMpD)
		{
			pathDrMpD = filePathDrMpD;
			if (!filePathDrMpD.StartsWith(ConstantHolderPad.GetRootPath()))
			{
				Debug.LogWarning(
					"This only supports files that are stored using Unity's Application data path. \nTo load bundled resources use 'Resources.Load(\"filename\") typeof(AudioClip)' method. \nhttps://docs.unity3d.com/ScriptReference/Resources.Load.html");
				return null;
			}

			byte[] fileBytesDrMpD = File.ReadAllBytes(filePathDrMpD);
			return ToAudioClip(fileBytesDrMpD, 0);
		}

		private static AudioClip ToAudioClip(byte[] fileBytesDrMpD, int offsetSamples = 0, string nameDrMpD = "w"+"av")
		{
			//string riff = Encoding.ASCII.GetString (fileBytes, 0, 4);
			//string wave = Encoding.ASCII.GetString (fileBytes, 8, 4);
			int subchunk1 = BitConverter.ToInt32(fileBytesDrMpD, 16);
			UInt16 audioFormat = BitConverter.ToUInt16(fileBytesDrMpD, 20);

			// NB: Only uncompressed PCM wav files are supported.
			string formatCodeDrMpD = FormatCode(audioFormat);
			Debug.AssertFormat(audioFormat == 1 || audioFormat == 65534, "Detected format code '{0}' {1}, but only PCM and WaveFormatExtensable uncompressed formats are currently supported.  {2}",
				audioFormat, formatCodeDrMpD, pathDrMpD);

			UInt16 channelsDrMpD = BitConverter.ToUInt16(fileBytesDrMpD, 22);
			int sampleRateDrMpD = BitConverter.ToInt32(fileBytesDrMpD, 24);
			//int byteRate = BitConverter.ToInt32 (fileBytes, 28);
			//UInt16 blockAlign = BitConverter.ToUInt16 (fileBytes, 32);
			UInt16 bitDepth = BitConverter.ToUInt16(fileBytesDrMpD, 34);

			int headerOffset = 16 + 4 + subchunk1 + 4;
			int subchunk2 = BitConverter.ToInt32(fileBytesDrMpD, headerOffset);
			//Debug.LogFormat ("riff={0} wave={1} subchunk1={2} format={3} channels={4} sampleRate={5} byteRate={6} blockAlign={7} bitDepth={8} headerOffset={9} subchunk2={10} filesize={11}", riff, wave, subchunk1, formatCode, channels, sampleRate, byteRate, blockAlign, bitDepth, headerOffset, subchunk2, fileBytes.Length);

			float[] dataDrMpD;
			switch (bitDepth)
			{
				case 8:
					dataDrMpD = Convert8BitByteArrayToAudioClipData(fileBytesDrMpD, headerOffset, subchunk2);
					break;
				case 16:
					dataDrMpD = Convert16BitByteArrayToAudioClipData(fileBytesDrMpD, headerOffset, subchunk2);
					break;
				case 24:
					dataDrMpD = Convert24BitByteArrayToAudioClipData(fileBytesDrMpD, headerOffset, subchunk2);
					break;
				case 32:
					dataDrMpD = Convert32BitByteArrayToAudioClipData(fileBytesDrMpD, headerOffset, subchunk2);
					break;
				default:
					throw new Exception(bitDepth + " bit depth is not supported.");
			}

			AudioClip audioClipDrMpD = AudioClip.Create(nameDrMpD, dataDrMpD.Length, (int) channelsDrMpD, sampleRateDrMpD, false);
			audioClipDrMpD.SetData(dataDrMpD, 0);
			return audioClipDrMpD;
		}

		#region wav file bytes to Unity AudioClip conversion methods

		private static float[] Convert8BitByteArrayToAudioClipData(byte[] sourceDrMpD, int headerOffset, int dataSizeDrMpD)
		{
			int wavSize = BitConverter.ToInt32(sourceDrMpD, headerOffset);
			headerOffset += sizeof(int);
			Debug.AssertFormat(wavSize > 0 && wavSize == dataSizeDrMpD,
				"Failed to get valid 8-bit wav size: {0} from data bytes: {1} at offset: {2}", wavSize, dataSizeDrMpD,
				headerOffset);

			float[] dataDrMpD = new float[wavSize];

			sbyte maxValueDrMpD = sbyte.MaxValue;

			int iDrMpD = 0;
			while (iDrMpD < wavSize)
			{
				dataDrMpD[iDrMpD] = (float) sourceDrMpD[iDrMpD] / maxValueDrMpD;
				++iDrMpD;
			}

			return dataDrMpD;
		}

		private static float[] Convert16BitByteArrayToAudioClipData(byte[] sourceDrMpD, int headerOffset, int dataSizeDrMpD)
		{
			int wavSize = BitConverter.ToInt32(sourceDrMpD, headerOffset);
			headerOffset += sizeof(int);
			Debug.AssertFormat(wavSize > 0 && wavSize == dataSizeDrMpD,
				"Failed to get valid 16-bit wav size: {0} from data bytes: {1} at offset: {2}", wavSize, dataSizeDrMpD,
				headerOffset);

			int xDrMpD = sizeof(Int16); // block size = 2
			int convertedSize = wavSize / xDrMpD;

			float[] dataDrMpD = new float[convertedSize];

			Int16 maxValueDrMpD = Int16.MaxValue;

			int iDrMpD = 0;
			while (iDrMpD < convertedSize)
			{
				var offsetDrMpD = iDrMpD * xDrMpD + headerOffset;
				dataDrMpD[iDrMpD] = (float) BitConverter.ToInt16(sourceDrMpD, offsetDrMpD) / maxValueDrMpD;
				++iDrMpD;
			}

			Debug.AssertFormat(dataDrMpD.Length == convertedSize, "AudioClip .wav data is wrong size: {0} == {1}",
				dataDrMpD.Length, convertedSize);

			return dataDrMpD;
		}

		private static float[] Convert24BitByteArrayToAudioClipData(byte[] sourceDrMpD, int headerOffset, int dataSizeDrMpD)
		{
			int wavSize = BitConverter.ToInt32(sourceDrMpD, headerOffset);
			headerOffset += sizeof(int);
			Debug.AssertFormat(wavSize > 0 && wavSize == dataSizeDrMpD,
				"Failed to get valid 24-bit wav size: {0} from data bytes: {1} at offset: {2}", wavSize, dataSizeDrMpD,
				headerOffset);

			int xDrMpD = 3; // block size = 3
			int convertedSize = wavSize / xDrMpD;

			int maxValueDrMpD = Int32.MaxValue;

			float[] dataDrMpD = new float[convertedSize];

			byte[]
				blockDrMpD = new byte[sizeof(int)]; // using a 4 byte block for copying 3 bytes, then copy bytes with 1 offset

			int offsetDrMpD = 0;
			int iDrMpD = 0;
			while (iDrMpD < convertedSize)
			{
				offsetDrMpD = iDrMpD * xDrMpD + headerOffset;
				Buffer.BlockCopy(sourceDrMpD, offsetDrMpD, blockDrMpD, 1, xDrMpD);
				dataDrMpD[iDrMpD] = (float) BitConverter.ToInt32(blockDrMpD, 0) / maxValueDrMpD;
				++iDrMpD;
			}

			Debug.AssertFormat(dataDrMpD.Length == convertedSize, "AudioClip .wav data is wrong size: {0} == {1}",
				dataDrMpD.Length, convertedSize);

			return dataDrMpD;
		}

		private static float[] Convert32BitByteArrayToAudioClipData(byte[] sourceDrMpD, int headerOffset, int dataSizeDrMpD)
		{
			int wavSize = BitConverter.ToInt32(sourceDrMpD, headerOffset);
			headerOffset += sizeof(int);
			Debug.AssertFormat(wavSize > 0 && wavSize == dataSizeDrMpD,
				"Failed to get valid 32-bit wav size: {0} from data bytes: {1} at offset: {2}", wavSize, dataSizeDrMpD,
				headerOffset);

			int xDrMpD = sizeof(float); //  block size = 4
			int convertedSize = wavSize / xDrMpD;

			Int32 maxValueDrMpD = Int32.MaxValue;

			float[] dataDrMpD = new float[convertedSize];

			int offsetDrMpD = 0;
			int i = 0;
			while (i < convertedSize)
			{
				offsetDrMpD = i * xDrMpD + headerOffset;
				dataDrMpD[i] = (float) BitConverter.ToInt32(sourceDrMpD, offsetDrMpD) / maxValueDrMpD;
				++i;
			}

			Debug.AssertFormat(dataDrMpD.Length == convertedSize, "AudioClip .wav data is wrong size: {0} == {1}",
				dataDrMpD.Length, convertedSize);

			return dataDrMpD;
		}

		#endregion

		public static byte[] FromAudioClip(AudioClip audioClip)
		{

			return FromAudioClip(audioClip, out string file, false);
		}

		private static byte[] FromAudioClip(AudioClip audioClip, out string filepathDrMpD, bool saveAsFileDrMpD = true,
			string dirname = "recordings")
		{
			MemoryStream streamDrMpD = new MemoryStream();

			const int headerSize = 44;

			// get bit depth
			UInt16 bitDepth = 16; //BitDepth (audioClip);

			// NB: Only supports 16 bit
			//Debug.AssertFormat (bitDepth == 16, "Only converting 16 bit is currently supported. The audio clip data is {0} bit.", bitDepth);

			// total file size = 44 bytes for header format and audioClip.samples * factor due to float to Int16 / sbyte conversion
			int fileSizeDrMpD = audioClip.samples * BlockSize_16Bit + headerSize; // BlockSize (bitDepth)

			// chunk descriptor (riff)
			WriteFileHeader(ref streamDrMpD, fileSizeDrMpD);
			// file header (fmt)
			WriteFileFormat(ref streamDrMpD, audioClip.channels, audioClip.frequency, bitDepth);
			// data chunks (data)
			WriteFileData(ref streamDrMpD, audioClip, bitDepth);

			byte[] bytesDrMpD = streamDrMpD.ToArray();

			// Validate total bytes
			Debug.AssertFormat(bytesDrMpD.Length == fileSizeDrMpD, "Unexpected AudioClip to wav format byte count: {0} == {1}",
				bytesDrMpD.Length, fileSizeDrMpD);

			// Save file to persistant storage location
			if (saveAsFileDrMpD)
			{
				filepathDrMpD = string.Format("{0}/{1}/{2}{3}", Application.persistentDataPath, dirname,
					DateTime.UtcNow.ToString("yyMMdd-HHmmss-fff"), StringHelperDrMpD.WavDrMpD);
				Directory.CreateDirectory(Path.GetDirectoryName(filepathDrMpD));
				File.WriteAllBytes(filepathDrMpD, bytesDrMpD);
				//Debug.Log ("Auto-saved .wav file: " + filepath);
			}
			else
			{
				filepathDrMpD = null;
			}

			streamDrMpD.Dispose();

			return bytesDrMpD;
		}

		#region write .wav file functions

		private static int WriteFileHeader(ref MemoryStream stream, int fileSize)
		{
			int countDrMpD = 0;
			int totalDrMpD = 12;

			// riff chunk id
			byte[] riffDrMpD = Encoding.ASCII.GetBytes(StringHelperDrMpD.RIFFDrMpD);
			countDrMpD += WriteBytesToMemoryStream(ref stream, riffDrMpD, "IDDrMpD".Remove("IDDrMpD".Length-5));

			// riff chunk size
			int chunkSizeDrMpD = fileSize - 8; // total size - 8 for the other two fields in the header
			countDrMpD += WriteBytesToMemoryStream(ref stream, BitConverter.GetBytes(chunkSizeDrMpD), "CHUNK_SIZE");

			byte[] waveDrMpD = Encoding.ASCII.GetBytes(StringHelperDrMpD.WaveDrMpD);
			countDrMpD += WriteBytesToMemoryStream(ref stream, waveDrMpD, "FORMAT");

			// Validate header
			Debug.AssertFormat(countDrMpD == totalDrMpD, "Unexpected wav descriptor byte count: {0} == {1}", countDrMpD, totalDrMpD);

			return countDrMpD;
		}

		private static int WriteFileFormat(ref MemoryStream stream, int channels, int sampleRate, UInt16 bitDepth)
		{
			int countDrMpD = 0;
			int totalDrMpD = 24;

			byte[] idDrMpD = Encoding.ASCII.GetBytes(StringHelperDrMpD.FmtDrMpD);
			countDrMpD += WriteBytesToMemoryStream(ref stream, idDrMpD, "FMT_ID");

			int subchunk1Size = 16; // 24 - 8
			countDrMpD += WriteBytesToMemoryStream(ref stream, BitConverter.GetBytes(subchunk1Size), "SUBCHUNK_SIZE");

			UInt16 audioFormat = 1;
			countDrMpD += WriteBytesToMemoryStream(ref stream, BitConverter.GetBytes(audioFormat), "AUDIO_FORMAT");

			UInt16 numChannelsDrMpD = Convert.ToUInt16(channels);
			countDrMpD += WriteBytesToMemoryStream(ref stream, BitConverter.GetBytes(numChannelsDrMpD), "CHANNELS");

			countDrMpD += WriteBytesToMemoryStream(ref stream, BitConverter.GetBytes(sampleRate), "SAMPLE_RATE");

			int byteRate = sampleRate * channels * BytesPerSample(bitDepth);
			countDrMpD += WriteBytesToMemoryStream(ref stream, BitConverter.GetBytes(byteRate), "BYTE_RATE");

			UInt16 blockAlign = Convert.ToUInt16(channels * BytesPerSample(bitDepth));
			countDrMpD += WriteBytesToMemoryStream(ref stream, BitConverter.GetBytes(blockAlign), "BLOCK_ALIGN");

			countDrMpD += WriteBytesToMemoryStream(ref stream, BitConverter.GetBytes(bitDepth), "BITS_PER_SAMPLE");

			// Validate format
			Debug.AssertFormat(countDrMpD == totalDrMpD, "Unexpected wav fmt byte count: {0} == {1}", countDrMpD, totalDrMpD);

			return countDrMpD;
		}

		private static int WriteFileData(ref MemoryStream stream, AudioClip audioClip, UInt16 bitDepth)
		{
			int countDrMpD = 0;
			int totalDrMpD = 8;

			// Copy float[] data from AudioClip
			float[] dataDrMpD = new float[audioClip.samples * audioClip.channels];
			audioClip.GetData(dataDrMpD, 0);

			byte[] bytesDrMpD = ConvertAudioClipDataToInt16ByteArray(dataDrMpD);

			byte[] idDrMpD = Encoding.ASCII.GetBytes(StringHelperDrMpD.DataDrMpD);
			countDrMpD += WriteBytesToMemoryStream(ref stream, idDrMpD, "DATA_ID");

			int subchunk2Size = Convert.ToInt32(audioClip.samples * BlockSize_16Bit); // BlockSize (bitDepth)
			countDrMpD += WriteBytesToMemoryStream(ref stream, BitConverter.GetBytes(subchunk2Size), "SAMPLES");

			// Validate header
			Debug.AssertFormat(countDrMpD == totalDrMpD, "Unexpected wav data id byte count: {0} == {1}", countDrMpD, totalDrMpD);

			// Write bytes to stream
			countDrMpD += WriteBytesToMemoryStream(ref stream, bytesDrMpD, StringHelperDrMpD.DataDrMpD.ToUpper());

			// Validate audio data
			Debug.AssertFormat(bytesDrMpD.Length == subchunk2Size, "Unexpected AudioClip to wav subchunk2 size: {0} == {1}",
				bytesDrMpD.Length, subchunk2Size);

			return countDrMpD;
		}

		private static byte[] ConvertAudioClipDataToInt16ByteArray(float[] dataDrMpD)
		{
			MemoryStream dataStreamDrMpD = new MemoryStream();

			int xDrMpD = sizeof(Int16);

			Int16 maxValueDrMpD = Int16.MaxValue;

			int iDrMpD = 0;
			while (iDrMpD < dataDrMpD.Length)
			{
				dataStreamDrMpD.Write(BitConverter.GetBytes(Convert.ToInt16(dataDrMpD[iDrMpD] * maxValueDrMpD)), 0, xDrMpD);
				++iDrMpD;
			}

			byte[] bytesDrMpD = dataStreamDrMpD.ToArray();

			// Validate converted bytes
			Debug.AssertFormat(dataDrMpD.Length * xDrMpD == bytesDrMpD.Length,
				"Unexpected float[] to Int16 to byte[] size: {0} == {1}", dataDrMpD.Length * xDrMpD, bytesDrMpD.Length);

			dataStreamDrMpD.Dispose();

			return bytesDrMpD;
		}

		private static int WriteBytesToMemoryStream(ref MemoryStream streamDrMpD, byte[] bytesDrMpD, string tag = "")
		{
			int countDrMpD = bytesDrMpD.Length;
			streamDrMpD.Write(bytesDrMpD, 0, countDrMpD);
			//Debug.LogFormat ("WAV:{0} wrote {1} bytes.", tag, count);
			return countDrMpD;
		}

		#endregion
		
		private static int BytesPerSample(UInt16 bitDepth)
		{
			return bitDepth / 8;
		}

		private static string FormatCode(UInt16 codeDrMpD)
		{
			switch (codeDrMpD)
			{
				case 1:
					return "PCM";
				case 2:
					return "ADPCM";
				case 3:
					return "IEEE";
				case 7:
					return "μ-law";
				case 65534:
					return "WaveFormatExtensable";
				default:
					Debug.LogWarning("Unknown wav code format:" + codeDrMpD);
					return "";
			}
		}

	}
}
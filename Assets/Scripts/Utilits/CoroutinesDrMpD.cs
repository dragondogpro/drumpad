using System.Collections;
using UnityEngine;

namespace DrumPad.Utilits
{
    public sealed class CoroutinesDrMpD : MonoBehaviour
    {
        private static CoroutinesDrMpD InstanceDrMpD
        {
            get
            {
                if (m_instanceDrMpD == null)
                {
                    GameObject gameCoroutineDrMpD = new GameObject("[COROUTINE MANAGER]");
                    m_instanceDrMpD = gameCoroutineDrMpD.AddComponent<CoroutinesDrMpD>();
                    DontDestroyOnLoad(gameCoroutineDrMpD);
                }

                return m_instanceDrMpD;
            }
        }

        private static CoroutinesDrMpD m_instanceDrMpD;

        /*public static Coroutine StartRoutine(IEnumerator enumerator)
        {
            if (enumerator!= null)
                return InstanceDrMpD.StartCoroutine(enumerator);
            return default;
        }

        public static void StopRoutine(Coroutine coroutine)
        {
            if (coroutine!= null)
                InstanceDrMpD.StopCoroutine(coroutine);
        }*/
    }
}
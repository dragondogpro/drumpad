using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.Utilits
{
    public class GridSizeHelper : MonoBehaviour
    {
        private void Start()
        {
            float gridSizeDrMpD = transform.parent.GetComponent<RectTransform>().rect.width;
            float deltaSizerDrMpD = 600 / 400.0f;
            float xSizeDrMpD = (gridSizeDrMpD - 250) * 0.5f;
            float ySizeDrMpD = xSizeDrMpD * deltaSizerDrMpD;
            
            Vector2 sizeElementsDrMpD = new Vector2(xSizeDrMpD, ySizeDrMpD);

            GetComponent<GridLayoutGroup>().cellSize = sizeElementsDrMpD;
        }
    }
}
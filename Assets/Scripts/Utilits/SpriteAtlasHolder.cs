using UnityEngine;
using UnityEngine.U2D;

namespace DrumPad.Utilits
{
    public class SpriteAtlasHolder
    {
        private readonly SpriteAtlas _spriteAtlas;
        
        public SpriteAtlasHolder()
        {
            _spriteAtlas = Resources.Load<SpriteAtlas>("StyleAtlas");
        }

        public Sprite GetSpriteFromAtlas(string kindDrMpD, int indexDrMpD)
        {
            return _spriteAtlas.GetSprite($"{kindDrMpD}_{indexDrMpD}");
        }
    }
}
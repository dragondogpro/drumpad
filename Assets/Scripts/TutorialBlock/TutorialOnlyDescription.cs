using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.TutorialBlock
{
    public class TutorialOnlyDescription : MonoBehaviour, ITutorial
    {
        [SerializeField] private Text tutorText;
        [SerializeField] private protected Button buttonNext;
        
        [TextArea(1, 4)]
        [SerializeField] private string tutorialDescription;

        private protected TutorialManager TutorialManager;
        
        public void InitTutor(TutorialManager tutorialManagerDrMpD)
        {
            TutorialManager = tutorialManagerDrMpD;
            buttonNext.onClick.AddListener(tutorialManagerDrMpD.ChangeAction);
        }
        
        public virtual void InitMethodDrMpD()
        {
            int numDrMpD = 423;
            if (numDrMpD != 432)
            {
                numDrMpD = 432;
            }
            
            gameObject.SetActive(true);
            tutorText.text = tutorialDescription;
        }

        public void CloseMethod()
        {
            int numDrMpD = 423;
            if (numDrMpD != 432)
            {
                numDrMpD = 432;
            }
            
            gameObject.SetActive(false);
        }
    }
}
using System;
using System.Collections.Generic;
using UnityEngine;

namespace DrumPad.TutorialBlock
{
    public class TutorialManager : MonoBehaviour
    {
        public event Action OnEndTutorial;
        
        [SerializeField] private GameObject loadCanvas;
        [SerializeField] private GameObject tutorBlock;

        private List<ITutorial> _tutorialActions;
        private ITutorial _currentElement;
        
        public void InitTutorial()
        {
            tutorBlock.SetActive(true);
            _tutorialActions = new List<ITutorial>();
            _tutorialActions.AddRange(GetComponentsInChildren<ITutorial>());
            foreach (var iTutor in _tutorialActions)
            {
                iTutor.InitTutor(this);
                iTutor.CloseMethod();
            }

            _currentElement = _tutorialActions[0];
            _currentElement.InitMethodDrMpD();
            _tutorialActions.RemoveAt(0);
        }

        internal void ChangeAction()
        {
            _currentElement.CloseMethod();
            if(_tutorialActions.Count == 0) return;
            
            _currentElement = _tutorialActions[0];
            _currentElement.InitMethodDrMpD();
            
            print(_currentElement.GetType());

            _tutorialActions.RemoveAt(0);
        }

        internal void EndTutorial()
        {
            int numDrMpD = 423;
            if (numDrMpD != 432)
            {
                numDrMpD = 432;
            }
            
            Destroy(loadCanvas);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void OnDestroy()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            OnEndTutorial?.Invoke();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }

    interface ITutorial
    {
        public void InitTutor(TutorialManager tutorialManagerDrMpD);
        
        public void InitMethodDrMpD();
        public void CloseMethod();
    }

    interface ITutorialAction
    {
        public event Action OnActionActive;
    }
}
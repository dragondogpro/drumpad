using UnityEngine;

namespace DrumPad.TutorialBlock
{
    public class TutorialChangeWindow : TutorialOnlyDescription
    {
        [SerializeField] private WindowEnumPad windowEnumPad;
        
        public override void InitMethodDrMpD()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            base.InitMethodDrMpD();
            buttonNext.onClick.AddListener(ClickButtonDrMpD);
        }

        private void ClickButtonDrMpD()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            GlobalEventSystem.ClickButtonWindow(windowEnumPad);
        }
    }
}
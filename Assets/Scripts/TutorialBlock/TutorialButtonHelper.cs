using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.TutorialBlock
{
    public class TutorialButtonHelper : MonoBehaviour
    {
        private void Awake()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            FindObjectOfType<TutorialManager>().OnEndTutorial += OnEndTutorial;
        }

        public void ActionActive()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            GetComponent<Button>().onClick.Invoke();
        }

        private void OnEndTutorial()
        {
            int nDrMpD = 10;
            if (nDrMpD == 10)
                Destroy(this);
        }
    }
}
using UnityEngine;

namespace DrumPad.TutorialBlock
{
    public class TutorialWithActionOnScene : MonoBehaviour, ITutorial
    {
        [SerializeField] private GameObject blockAllPanel;
        [SerializeField] private GameObject blockThisPanel;
        [SerializeField] private GameObject tutorialAction;

        private TutorialManager _tutorialManager;
        private ITutorialAction _tutorialAction;
        
        public void InitMethodDrMpD()
        {
            //print("Init");
            gameObject.SetActive(true);
            blockAllPanel.SetActive(false);
            blockThisPanel.SetActive(true);
            _tutorialAction = tutorialAction.GetComponent<ITutorialAction>();
            if(_tutorialAction != null)
                _tutorialAction.OnActionActive += _tutorialManager.ChangeAction;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void CloseMethod()
        {
            //print("Close");
            blockAllPanel.SetActive(true);
            gameObject.SetActive(false);
            if(_tutorialAction != null)
                _tutorialAction.OnActionActive -= _tutorialManager.ChangeAction;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void InitTutor(TutorialManager tutorialManagerDrMpD)
        {
            if(tutorialManagerDrMpD != null)
                _tutorialManager = tutorialManagerDrMpD;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}
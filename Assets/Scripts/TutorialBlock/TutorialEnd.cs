
namespace DrumPad.TutorialBlock
{
    public class TutorialEnd : TutorialOnlyDescription
    {
        public override void InitMethodDrMpD()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            base.InitMethodDrMpD();
            buttonNext.onClick.AddListener(ClickButtonDrMpD);
        }

        private void ClickButtonDrMpD()
        {
            GameManagerDrum.GaM.MyMusicManager.LessonsManager.AnimationMoveCurrentLesson(false);
            GameManagerDrum.GaM.MyMusicManager.ClickLessonsButton(false);
            GameManagerDrum.GaM.MyMusicManager.RecordManager.CloseRecordWindow();
            GlobalEventSystem.ClickButtonWindow(WindowEnumPad.LIBRARY);
            TutorialManager.EndTutorial();
        }
    }
}
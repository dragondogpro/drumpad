using System;
using System.IO;
using DrumPad.TutorialBlock;
using DrumPad.Utilits;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DrumPad.Styles
{
    public class DetailBlockMusicStyle : MonoBehaviour, IInitialization, ITutorialAction
    {
        public event Action OnActionActive;
        
        [SerializeField] private Image imageTitle;
        [SerializeField] private Image loadBarPad;
        [SerializeField] private GameObject loadBarObject;
        
        [SerializeField] private Text _textDowloadingDrMpD;
        
        [SerializeField] private GameObject _alertPanelDrMpD;
        [SerializeField] private Button _yesButtonDrMpD;
        [SerializeField] private Button _noButtonDrMpD;

        [FormerlySerializedAs("titleName")] [SerializeField] private Text titleNameDrMpD;
        [SerializeField] private Text titleKind;

        [FormerlySerializedAs("buttonAction")] [SerializeField] private Button buttonActionDrMpD;
        [SerializeField] private Button exitButton;
        
        private Text _textOnButtonPad;

        private DataContent _dataContentDrMpD;

        private bool _isExistFile;
        private bool _isFileDownloading;
        private double _sizeFilesDrMpD;
        private double _percentValueDrMpD;
        

        public void InitializationDrMpD()
        {
            _yesButtonDrMpD.onClick.AddListener(YesButtonDrMpDClick);
            _noButtonDrMpD.onClick.AddListener(NoButtonDrMpDClick);
            buttonActionDrMpD.onClick.AddListener(ClickActionButton);
            _textOnButtonPad = buttonActionDrMpD.GetComponentInChildren<Text>();
            loadBarObject.SetActive(false);
            exitButton.onClick.AddListener(() => gameObject.SetActive(false));
        }
        
        public void SelectStyleMusic(DataContent dataContentDrMpD, Sprite titleImageDrMpD)
        {
            _dataContentDrMpD = dataContentDrMpD;
            
            imageTitle.sprite = titleImageDrMpD;
            loadBarPad.enabled = false;
            
            titleKind.text = _dataContentDrMpD.KindName;
            titleNameDrMpD.text = _dataContentDrMpD.StyleName;

            _isExistFile = CheckFileExistDrMpD();
            _isFileDownloading = false;

            StartCoroutine(DownloadMasterPad.GetFileSizeDrMpD(_dataContentDrMpD, SetTextOnSelectedStyleMusic));
            
            //_textOnButtonPad.text = _isExistFile ? StringHelperDrMpD.SelectDrMpD : StringHelperDrMpD.DownloadDrMpD;
        }

        private void NoButtonDrMpDClick()
        {
            _alertPanelDrMpD.SetActive(false);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void YesButtonDrMpDClick()
        {
            _alertPanelDrMpD.SetActive(false);
            
            _isFileDownloading = true;
            loadBarObject.SetActive(true);
            buttonActionDrMpD.gameObject.SetActive(false);
            exitButton.gameObject.SetActive(false);
            _textOnButtonPad.text = $"{StringHelperDrMpD.DownloadDrMpD}ing";
            DownloadMasterPad.LoadMusicPack(_dataContentDrMpD, LoadBarCorrect, EndLoadFileDrMpD);
        }

        private void SetTextOnSelectedStyleMusic(double sizeFileDrMpD)
        {
            if (_isExistFile)
                _textOnButtonPad.text = StringHelperDrMpD.SelectDrMpD;
            else
            {
                float sizeResultDrMpD = (float)(sizeFileDrMpD / 1024 / 1000);
                _sizeFilesDrMpD = sizeResultDrMpD;
                _textOnButtonPad.text = StringHelperDrMpD.DownloadDrMpD + $" ({Math.Round(sizeResultDrMpD, 2)}mb)";
            }
        }
        
        private void ClickActionButton()
        {
            if(_isFileDownloading) return;
            
            if (_isExistFile)
            {
                GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsMusicList.SelectMusicStyle(_dataContentDrMpD);
                GlobalEventSystem.SelectNewStyle(_dataContentDrMpD);

                MusicPackElement musicPackElement = new MusicPackElement();
                musicPackElement.InitElementBlock(_dataContentDrMpD);
                
                GlobalEventSystem.ClickButtonWindow(WindowEnumPad.MYMUSIC);
                GameManagerDrum.GaM.DownPanelButtonManager.OpenCloseButton(false);
                GameManagerDrum.GaM.MyMusicManager.RecordManager.gameObject.SetActive(true);
                GameManagerDrum.GaM.MyMusicManager.RecordManager.SelectMusicStyle(musicPackElement);
                OnActionActive?.Invoke();
                gameObject.SetActive(false);
            }
            else
            {
                _alertPanelDrMpD.SetActive(true);
            }
        }

        private void EndLoadFileDrMpD()
        {
            _isFileDownloading = false;
            loadBarPad.enabled = false;
            loadBarObject.SetActive(false);
            buttonActionDrMpD.gameObject.SetActive(true);
            exitButton.gameObject.SetActive(true);
            _isExistFile = CheckFileExistDrMpD();
            _textOnButtonPad.text = _isExistFile ? StringHelperDrMpD.SelectDrMpD : StringHelperDrMpD.DownloadDrMpD;
        }

        private bool CheckFileExistDrMpD()
        {
            string pathToFileStyleA = DownloadMasterPad.PathToFolder(_dataContentDrMpD) + "/Side A";
            string pathToFileStyleB = DownloadMasterPad.PathToFolder(_dataContentDrMpD) + "/Side B";
            bool isFileExistDrMpD = Directory.Exists(pathToFileStyleA) && Directory.Exists(pathToFileStyleB);
            return isFileExistDrMpD;
        }

        private void LoadBarCorrect(float progressDrMpD)
        {
            loadBarPad.enabled = true;
            loadBarPad.fillAmount = progressDrMpD;

            var downloadPercentDrMpD = _sizeFilesDrMpD * progressDrMpD;
           
            Debug.Log($"{StringHelperDrMpD.DownloadDrMpD}ing... {Math.Round( downloadPercentDrMpD, 2)}/{Math.Round(_sizeFilesDrMpD, 2)}mb");
            _textDowloadingDrMpD.text = $"{Math.Round(downloadPercentDrMpD, 2)}/{Math.Round(_sizeFilesDrMpD, 2)}mb";
            
        }
    }
}
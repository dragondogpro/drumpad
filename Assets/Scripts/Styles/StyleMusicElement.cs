using DrumPad.Utilits;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.Styles
{
    public class StyleMusicElement : MonoBehaviour
    {
        [SerializeField] private Text nameStyleName;
        [SerializeField] private Text nameStyle;

        private Button _buttonElementDrMpD;
        private Image _imageOnButton;
        
        private MusicPackElement _musicPackElement;
        private DataContent _dataContentDrMpD;
        
        public void Initialization(MusicPackElement musicPackElement)
        {
            _dataContentDrMpD = musicPackElement.DataContent;
            _musicPackElement = musicPackElement;
            _buttonElementDrMpD = GetComponent<Button>();
            _imageOnButton = _buttonElementDrMpD.image;
            _buttonElementDrMpD.onClick.AddListener(SelectMusicStyle);

            _imageOnButton.sprite = _dataContentDrMpD.SpriteElement;
            nameStyleName.text = _dataContentDrMpD.StyleName;
            nameStyle.text = _dataContentDrMpD.KindName;
        }

        private void SelectMusicStyle()
        {
            GlobalEventSystem.SelectNewStyle(_dataContentDrMpD);
            GameManagerDrum.GaM.DownPanelButtonManager.OpenCloseButton(false);
            GameManagerDrum.GaM.MyMusicManager.RecordManager.gameObject.SetActive(true);
            GameManagerDrum.GaM.MyMusicManager.RecordManager.SelectMusicStyle(_musicPackElement);
        }
    }
}
using System;
using System.Collections.Generic;
using SettingsDrMpD;
using UnityEngine.Serialization;

namespace DrumPad.Styles
{
    [Serializable]
    public class JsonStyleList
    {
        public JsonStyleListElement[] AllStyles;
    }
    
    [Serializable]
    public class JsonStyleListElement
    {
        public string KindOfStyle;
        public string[] Styles;
    }

    [Serializable]
    public class JsonLessonElement
    {
        public Dictionary<int, Dictionary<int, PadIndexName[]>> LessonsDictionary;
    }
}
using DrumPad.Library_Block;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.Styles
{
    public class StyleMusicManager : MonoBehaviour, IInitialization, IManager
    {
        [SerializeField] private MusicStyleHorizontalBlock styleMusicPrefab;
        private RectTransform _rotHolderMusicStyle;
        
        public void InitializationDrMpD()
        {
            _rotHolderMusicStyle = GetComponent<ScrollRect>().content;
            
            foreach (var styleListDrMpD in GameManagerDrum.GaM.JsonManagerDrMpD.JsonStyleList.AllStyles)
            {
                MusicStyleHorizontalBlock styleHorizontalBlock = Instantiate(styleMusicPrefab, _rotHolderMusicStyle);
                
                styleHorizontalBlock.InitBlockElements(styleListDrMpD);
            }
        }
    }
}
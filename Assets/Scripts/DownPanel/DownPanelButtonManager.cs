using DG.Tweening;
using UnityEngine;

namespace DrumPad.DownPanel
{
    public class DownPanelButtonManager : MonoBehaviour, IInitialization, IManager
    {
        public void InitializationDrMpD()
        {
            foreach (var downPanelButton in GetComponentsInChildren<DownPanelButton>())
            {
                downPanelButton.InitializationDrMpD();
            }
            
            GlobalEventSystem.OnClickButtonWindow += GlobalEventSystemOnOnClickButtonWindow;
        }

        private void GlobalEventSystemOnOnClickButtonWindow(WindowEnumPad obj)
        {
            OpenCloseButton(true);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void OpenCloseButton(bool isActivated)
        {
            float endLocalScale = isActivated ? 1.0f : 0.0f;
            transform.DOScaleY(endLocalScale, 0.3f);
            GameManagerDrum.GaM.TimeBlockPanel(0.3f);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}
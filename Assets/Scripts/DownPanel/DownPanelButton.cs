using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.DownPanel
{
    internal class DownPanelButton : MonoBehaviour, IInitialization
    {
        [SerializeField] private WindowEnumPad windowEnumPad;

        private Button _buttonActivePad;
        private Image _buttonImagePad;
        private Text _buttonTextPad;

        private readonly Color _colorInActivePad = new Color(0.3f, 0.15f, 0.4f);
        private readonly Color _colorActivePad = new Color(0.98f, 0.25f, 1.0f);

        public void InitializationDrMpD()
        {
            _buttonActivePad = GetComponent<Button>();
            _buttonTextPad = GetComponentInChildren<Text>();
            _buttonActivePad.onClick.AddListener(SelectButtonPad);
            _buttonImagePad = _buttonActivePad.image;

            GlobalEventSystem.OnClickButtonWindow += OnSelectButtonWinPad;
        }

        private void SelectButtonPad()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            GlobalEventSystem.ClickButtonWindow(windowEnumPad);
        }

        private void OnSelectButtonWinPad(WindowEnumPad winEnumPad)
        {
            if (winEnumPad == windowEnumPad)
            {
                _buttonImagePad.color = Color.white;
                _buttonTextPad.color = _colorActivePad;
            }
            else
            {
                _buttonImagePad.color = Color.gray;
                _buttonTextPad.color = _colorInActivePad;
            }
        }
    }
}
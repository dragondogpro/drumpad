using System.Collections.Generic;

namespace DrumPad
{
    public class DataContainer
    {
        public DataContainer()
        {
            DataContentsMap = new Dictionary<string, DataElementsDrMpD>();
        }
        
        public Dictionary<string, DataElementsDrMpD> DataContentsMap { get; }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using DrumPad.DownPanel;
using DrumPad.JsonBlock;
using DrumPad.Library_Block;
using DrumPad.MoreBlock;
using DrumPad.MyMusic;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace DrumPad
{
    public class GameManagerDrum : MonoBehaviour
    {
        private static GameManagerDrum _gaM;
        public static GameManagerDrum GaM => _gaM;
        
        //Other
        [SerializeField] private GameObject blockPanelDrMpD;
        [SerializeField] private AudioPlayerRecord audioPlayerRecord;
        private bool _isActiveBlock;
        
        //Init managers
        [SerializeField] private DownPanelButtonManager downPanelButtonManager;
        [SerializeField] private WindowPadManager windowPadManager;
        [SerializeField] private MyMusicManager myMusicManager;
        [SerializeField] private LibraryManager libraryManager;
        [SerializeField] private MoreManager moreManager;
        [SerializeField] private LoadScreenPad loadScreenPad;
        private JsonManagerDrMpD jsonManagerDrMpD;
        
        //Public reference
        public DownPanelButtonManager DownPanelButtonManager => downPanelButtonManager;
        public MyMusicManager MyMusicManager => myMusicManager;
        public LibraryManager LibraryManager => libraryManager;
        public JsonManagerDrMpD JsonManagerDrMpD => jsonManagerDrMpD;
        public AudioPlayerRecord AudioPlayerRecord => audioPlayerRecord;

        public DataContent CurrentData { get; private set; }
        public Dictionary<string, DataElementsDrMpD> DataContentsMap { get; private set; }
        
        [Inject]
        public void Construct(JsonManagerDrMpD jsonManagerDrMpD)
        {
            _gaM = this;
            _isActiveBlock = false;
            blockPanelDrMpD.SetActive(_isActiveBlock);
            
            this.jsonManagerDrMpD = jsonManagerDrMpD;
            this.jsonManagerDrMpD.OnLoadAllJsons += InitAllManagersDrMpD;
            this.jsonManagerDrMpD.InitDrMpD();
 
            DataContentsMap = new Dictionary<string, DataElementsDrMpD>();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void InitAllManagersDrMpD()
        {
            IManager[] initializations =
            {
                downPanelButtonManager,
                libraryManager,
                myMusicManager,
                windowPadManager,
                moreManager,
                loadScreenPad
            };

            foreach (var initialization in initializations)
            {
                initialization.InitializationDrMpD();
            }
            
            jsonManagerDrMpD.OnLoadAllJsons -= InitAllManagersDrMpD;
            
            GlobalEventSystem.ClickButtonWindow(WindowEnumPad.LIBRARY);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public async void TimeBlockPanel(float blockDurationDrMpD)
        {
            if (_isActiveBlock) return;

            _isActiveBlock = true;
            blockPanelDrMpD.SetActive(_isActiveBlock);
            await Task.Delay(ConstantHolderPad.ConvertToMilliseconds(blockDurationDrMpD));
            _isActiveBlock = false;
            blockPanelDrMpD.SetActive(_isActiveBlock);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void SetKindStyle(DataContent dataContent)
        {
            if (dataContent != null)
              CurrentData = dataContent;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}
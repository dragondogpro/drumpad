using DrumPad.Utilits;
using UnityEngine;

namespace DrumPad
{
    public static class ConstantHolderPad
    {
        public const string ROOT_FOLDER_STYLES = "MusicStyles";
        public const string ROOT_FOLDER_RECORDS = "Capture";

        public const string ROOT_FOLDER_ON_SERVER = //"https://drumpadmachineee.s3.amazonaws.com/";
                                                    "https://drampad.s3.amazonaws.com/";//"https://drampad.s3.eu-central-1.amazonaws.com/";

        
        public static string GetRootPath()
        {
#if UNITY_EDITOR
            string pathFileDrMpD = Application.dataPath;
#else
            string pathFileDrMpD = Application.persistentDataPath;
#endif
            return pathFileDrMpD;
        }

        public static bool IsCorrectString(string checkString)
        {
            string validSpaces = checkString;
            while (validSpaces.Contains(StringHelperDrMpD.SpaceDrMpD))
            {
                validSpaces = validSpaces.Replace(StringHelperDrMpD.SpaceDrMpD, "");
            }

            if (validSpaces.Length == 0) return false;
            
            string symbolsDrMpD = "?<>:*|\"~`!@#$%^&'[]{}+=;/\\№";

            foreach (var charSymbol in symbolsDrMpD)
            {
                if (validSpaces.Contains(charSymbol.ToString()))
                {
                    Debug.Log($"The symbol [{charSymbol}] include to the string");
                    return false;
                }
            }

            return true;
        }

        public static int ConvertToMilliseconds(float seconds)
        {
            int secondInt = (int) (seconds * 1000);
            return secondInt;
        }
    }
}
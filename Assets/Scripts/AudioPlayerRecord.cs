using SettingsDrMpD;
using UnityEngine;

namespace DrumPad
{
    public class AudioPlayerRecord : MonoBehaviour
    {
        [Header("[AUDIO SOURCES]")] 
        [SerializeField] private AudioSource drum1Source;
        [SerializeField] private AudioSource drum2Source;
        [SerializeField] private AudioSource drum3Source;
        [SerializeField] private AudioSource mainMelodySource;
        [SerializeField] private AudioSource fxMelodySource;
        [SerializeField] private AudioSource rythmMelodySource;

        public void PlayAudioClipMusic(AudioClip clipToPlay, PadIndexName padIndexName)
        {
            AudioSource selectSource = null;

            switch (padIndexName)
            {
                case PadIndexName.Drum1Pad1:
                case PadIndexName.Drum1Pad2:
                    selectSource = drum1Source;
                    break;

                case PadIndexName.Drum2Pad1:
                case PadIndexName.Drum2Pad2:
                    selectSource = drum2Source;
                    break;

                case PadIndexName.Drum3Pad1:
                case PadIndexName.Drum3Pad2:
                    selectSource = drum3Source;
                    break;

                case PadIndexName.FxMelody1Pad1:
                case PadIndexName.FxMelody2Pad1:
                case PadIndexName.FxMelody1Pad2:
                case PadIndexName.FxMelody2Pad2:
                    selectSource = fxMelodySource;
                    break;

                case PadIndexName.MainMelody1Pad1:
                case PadIndexName.MainMelody2Pad1:
                case PadIndexName.MainMelody3Pad1:
                case PadIndexName.MainMelody4Pad1:
                case PadIndexName.MainMelody1Pad2:
                case PadIndexName.MainMelody2Pad2:
                case PadIndexName.MainMelody3Pad2:
                case PadIndexName.MainMelody4Pad2:
                    selectSource = mainMelodySource;
                    break;

                case PadIndexName.RythmMelody1Pad1:
                case PadIndexName.RythmMelody2Pad1:
                case PadIndexName.RythmMelody3Pad1:
                case PadIndexName.RythmMelody1Pad2:
                case PadIndexName.RythmMelody2Pad2:
                case PadIndexName.RythmMelody3Pad2:
                    selectSource = rythmMelodySource;
                    break;
            }

            if (selectSource != null)
            {
                selectSource.clip = clipToPlay;
                selectSource.Play();
            }
        }

        public void StopAll()
        {
            drum1Source.Stop();
            drum2Source.Stop();
            drum3Source.Stop();
            mainMelodySource.Stop();
            fxMelodySource.Stop();
            rythmMelodySource.Stop();
        }
    }
}

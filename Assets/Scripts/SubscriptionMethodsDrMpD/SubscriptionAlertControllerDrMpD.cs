using I2.Loc;
using UnityEngine;
using UnityEngine.UI;

namespace SubscriptionMethodsDrMpD
{
    public class SubscriptionAlertControllerDrMpD : MonoBehaviour
    {
        [SerializeField] private Text[] _textAlertDrMpD;
        [SerializeField] private Button _yesButtonDrMpD;
        [SerializeField] private Button _noButtonDrMpD;
        [SerializeField] private GameObject _priceTextDrMpD;
        [SerializeField] private GameObject _standartSubPanelDrMpD;

        private void Start()
        {
            _yesButtonDrMpD.onClick.AddListener(AlertButtonOnClickDrMpD);
            _noButtonDrMpD.onClick.AddListener(AlertButtonOnClickDrMpD);
            
            
            //if (Application.systemLanguage != SystemLanguage.English)
              
            string currentLanguageLoc = LocalizationManager.CurrentLanguage;
            Debug.Log("current language is " + currentLanguageLoc);
            if (currentLanguageLoc == "English")
            {
                if (_standartSubPanelDrMpD.activeSelf)
                    _priceTextDrMpD.SetActive(true);
            }

            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void AlertButtonOnClickDrMpD()
        {
            if (_yesButtonDrMpD != null && _noButtonDrMpD != null) 
                gameObject.SetActive(false);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}
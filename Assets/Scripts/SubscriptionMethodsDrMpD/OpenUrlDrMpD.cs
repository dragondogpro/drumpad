using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SubscriptionMethodsDrMpD
{
    public class OpenUrlDrMpD : MonoBehaviour
    {
        [SerializeField] private Transform _priceTextDrMpD;
        [SerializeField] private Transform _termsTransformDrMpD;
        [SerializeField] private Transform _privacyTransDrMpD;
        [SerializeField] private Transform _restoreDrMpD;
        [SerializeField] private Transform _exitDrMpD;

        [SerializeField] private string TermUrlDrMpD;
        [SerializeField] private string PrivacyUrl;

        public void Awake()
        {
            Application.targetFrameRate = 60;

            if (Application.systemLanguage != SystemLanguage.English)
            {
                _priceTextDrMpD.gameObject.GetComponent<TextMeshProUGUI>().enabled = false;
                _priceTextDrMpD.gameObject.GetComponent<TextMeshProUGUI>().text = 
                "Start 1 month for <b><color=#D235D9>FREE</color></b>\nThen $1.99/week";
            }

            if (Application.systemLanguage == SystemLanguage.Arabic ||
                Application.systemLanguage == SystemLanguage.Hebrew)
            {
                
                _termsTransformDrMpD.GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Left;
                _privacyTransDrMpD.GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Right;
                
                _restoreDrMpD.GetComponent<RectTransform>().anchorMin = new(0f, 1f);
                _restoreDrMpD.GetComponent<RectTransform>().anchorMax = new(0f, 1f);
                _restoreDrMpD.GetComponent<RectTransform>().anchoredPosition = new(
                    -_restoreDrMpD.GetComponent<RectTransform>().anchoredPosition.x,
                    _restoreDrMpD.GetComponent<RectTransform>().anchoredPosition.y);

                if (_exitDrMpD != null)
                {
                    _exitDrMpD.GetComponent<RectTransform>().anchorMin = new(1f, 1f);
                    _exitDrMpD.GetComponent<RectTransform>().anchorMax = new(1f, 1f);
                    _exitDrMpD.GetComponent<RectTransform>().anchoredPosition = new(
                        -_exitDrMpD.GetComponent<RectTransform>().anchoredPosition.x,
                        _exitDrMpD.GetComponent<RectTransform>().anchoredPosition.y);
                }
                
            }

            _termsTransformDrMpD.gameObject.GetComponent<Button>().onClick.AddListener(TermOpenDrMpD);
            _privacyTransDrMpD.gameObject.GetComponent<Button>().onClick.AddListener(PrivacyOpenDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void TermOpenDrMpD()
        {
            bool testDrMpD = true;
            if (_termsTransformDrMpD != null && TermUrlDrMpD != null) 
                Application.OpenURL(TermUrlDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void PrivacyOpenDrMpD()
        {
            bool testDrMpD = true;
            if (_privacyTransDrMpD != null && PrivacyUrl != null) 
                Application.OpenURL(PrivacyUrl);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

using UnityEngine;
using UnityEngine.UI;

namespace SubscriptionMethodsDrMpD
{
    public class SubscriptionStyleSelectedDrMpD : MonoBehaviour
    {
        [SerializeField] private Sprite _selectedSpriteDrMpD;
        [SerializeField] private Sprite _defaultSpriteDrMpD;

        void Start()
        {
            var testDrMpD = true;
            var test1DrMpD = !testDrMpD;
            
            gameObject.GetComponent<Button>().onClick.AddListener(ChangeElementSizeDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void ChangeElementSizeDrMpD()
        {
            var testDrMpD = true;
            var test1DrMpD = !testDrMpD;
            
            if (gameObject.GetComponent<Image>().sprite == _defaultSpriteDrMpD)
            {
                gameObject.GetComponent<Image>().sprite = _selectedSpriteDrMpD;
                gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(310, 310);
            }
            else
            {
                gameObject.GetComponent<Image>().sprite = _defaultSpriteDrMpD;
                gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(230, 230);
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace SubscriptionMethodsDrMpD
{
    public class DoTweenAnimationDrMpD : MonoBehaviour
    {
        [SerializeField] private ScrollRect _scrollViewDrMpD;

        private void OnEnable()
        {
            float normPosDrMpD = 0f;
        
            DOTween.To(() => normPosDrMpD, xDrMpD => normPosDrMpD = xDrMpD, 1f, 5f)
                .SetLoops(-1, LoopType.Restart)
                .SetEase(Ease.Linear)
                .OnUpdate(() => _scrollViewDrMpD.horizontalNormalizedPosition = normPosDrMpD);

            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

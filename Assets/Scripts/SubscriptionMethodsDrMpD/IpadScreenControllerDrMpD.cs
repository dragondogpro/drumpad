using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace SubscriptionMethodsDrMpD
{
    public class IpadScreenControllerDrMpD : MonoBehaviour
    {
        [SerializeField] private ScrollRect _firstScrollDrMpD;
        [SerializeField] private HorizontalLayoutGroup _firstHLGDrMpD;
        [SerializeField] private ScrollRect _secondScrollDrMpD;
        [SerializeField] private HorizontalLayoutGroup _secondHLGDrMpD;
        [SerializeField] private Canvas _canvasDrMpD;

        private void Start()
        {
            if ((float)Screen.height / Screen.width < 1.5f)
            {
                _canvasDrMpD.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.3f;//GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
                _secondHLGDrMpD.GetComponent<HorizontalLayoutGroup>().padding.left = 100;
            }
            if (Screen.height == 1334 && Screen.width == 750)
            {
                _canvasDrMpD.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.4f;
            }

            if (Application.systemLanguage == SystemLanguage.Arabic ||
                Application.systemLanguage == SystemLanguage.Hebrew)
            {
                _firstScrollDrMpD.horizontalNormalizedPosition = 1;
                _firstHLGDrMpD.reverseArrangement = true;
                _secondScrollDrMpD.horizontalNormalizedPosition = 1;
                _secondHLGDrMpD.reverseArrangement = true;
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

using System.Collections.Generic;
using UnityEngine;

namespace DrumPad
{
    public class DataElementsDrMpD
    {
        public DataElementsDrMpD()
        {
            DataMap = new Dictionary<string, DataContent>();
        }

        public Dictionary<string, DataContent> DataMap { get; }

        public DataContent GetElement(string style)
        {
            DataContent returnData = null;

            if (DataMap.ContainsKey(style))
                returnData = DataMap[style];
            else
                Debug.LogError($"Element with key [{style}] did not found");
            
            return returnData;
        }
    }
}
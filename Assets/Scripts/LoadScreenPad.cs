using System.Threading.Tasks;
using UnityEngine;

namespace DrumPad
{
    public class LoadScreenPad : MonoBehaviour, IManager
    {
        [SerializeField] private FirstPanel firstPanel;
        public void InitializationDrMpD()
        {
            if(firstPanel!= null)
                DelayDisableObject();
        }

        private async void DelayDisableObject()
        {
            await Task.Delay(3500);
            gameObject.SetActive(false);
            firstPanel.InitializationDrMpD();
        }
    }
}
using System;

namespace DrumPad
{
    public static class GlobalEventSystem
    {
        public static event Action<DataContent> OnSelectNewStyle;
        public static void SelectNewStyle(DataContent data) => OnSelectNewStyle?.Invoke(data);
        
        public static event Action<WindowEnumPad> OnClickButtonWindow;
        public static void ClickButtonWindow(WindowEnumPad wind) => OnClickButtonWindow?.Invoke(wind);
    }
}
using System;
using System.Collections.Generic;
using DrumPad.Utilits;
using UnityEngine;
using UnityEngine.Serialization;

namespace DrumPad.JsonBlock
{
    [Serializable]
    public class SaveJsonMusicList : IInitialization
    {
        public List<DataContent> recentMusicStyles;

        public void InitializationDrMpD()
        {
            int kkDrMpD = 431;
            if (kkDrMpD == int.Parse("321890"))
                kkDrMpD--;
            
            GlobalEventSystem.OnSelectNewStyle += SelectMusicStyle;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        public void SelectMusicStyle(DataContent dataContentDrMpD)
        {
            foreach (var dataDrMpD in recentMusicStyles)
            {
                if (dataDrMpD.StyleName == dataContentDrMpD.StyleName)
                {
                    int indexOfStyle = recentMusicStyles.IndexOf(dataContentDrMpD);
                    NewCollect(indexOfStyle, dataContentDrMpD);
                    return;
                }
            }
            
            recentMusicStyles.Add(dataContentDrMpD);
            NewCollect(recentMusicStyles.Count - 1, dataContentDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void NewCollect(int indexForStart, DataContent dataContentDrMpD)
        {
            for (int iDrMpD = indexForStart; iDrMpD > 0; iDrMpD--)
            {
                DataContent tmpData = recentMusicStyles[iDrMpD - 1];
                recentMusicStyles[iDrMpD] = tmpData;
            }
                
            recentMusicStyles[0] = dataContentDrMpD;
            
            GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsonFileDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }

    [Serializable]
    public class SaveMusicMapJson
    {
        public Dictionary<string, DataContent> SaveFileMusic;

        public void AddElement(string fileNameDrMpD, DataContent dataContent)
        {
            fileNameDrMpD += StringHelperDrMpD.WavDrMpD;
            if(!SaveFileMusic.ContainsKey(fileNameDrMpD))
                SaveFileMusic.Add(fileNameDrMpD, dataContent);
            
            GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsonFileDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void RenameFileDrMpD(string oldFileNameDrMpD, string newFileNameDrMpD)
        {
            newFileNameDrMpD += StringHelperDrMpD.WavDrMpD;
            
            Debug.Log(oldFileNameDrMpD);
            Debug.Log(newFileNameDrMpD);
            
            if (SaveFileMusic.ContainsKey(oldFileNameDrMpD))
            {
                DataContent dataContent = SaveFileMusic[oldFileNameDrMpD];
                SaveFileMusic.Add(newFileNameDrMpD, dataContent);
                SaveFileMusic.Remove(oldFileNameDrMpD);
                
                GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsonFileDrMpD();
            }
        }

        public void RemoveElementDrMpD(string fileNameDrMpD)
        {
            if (SaveFileMusic.ContainsKey(fileNameDrMpD))
            {
                SaveFileMusic.Remove(fileNameDrMpD);
                
                GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsonFileDrMpD();
            }
        }
    }

    [Serializable]
    public class MusicLessonsListJson
    {
        public Dictionary<string, MusicLessonsJson> LessonJsons;
    }

    [Serializable]
    public class MusicLessonsJson
    {
        public MusicLessonsJson(int countDrMpD)
        {
            lessons = new MusicLessonJson[countDrMpD];
            for (int i = 0; i < lessons.Length; i++)
            {
                lessons[i] = new MusicLessonJson(i + 1);
            }
        }

        public MusicLessonJson[] lessons;

        public event Action OnUpdateElement;

        public void UpdateElement()
        {
            OnUpdateElement?.Invoke();
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

    }

    [Serializable]
    public class MusicLessonJson
    {
        public MusicLessonJson(int countDrMpD)
        {
            lessonIndex = countDrMpD;
            doneLesson = false;
            lastScore = 0;
            bestScoreDrMpD = 0;
        }

        public int lessonIndex;
        public bool doneLesson;
        public int lastScore;
        [FormerlySerializedAs("bestScore")] public int bestScoreDrMpD;

        public void SetScore(float scoreDrMpD)
        {
            int newScoreDrMpD = (int)(scoreDrMpD * 100);
            lastScore = Mathf.Clamp(newScoreDrMpD, 0, 100);

            if (bestScoreDrMpD < newScoreDrMpD)
                bestScoreDrMpD = newScoreDrMpD;

            if (!doneLesson)
                doneLesson = newScoreDrMpD > 50;
            
            GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsonFileDrMpD();
        }
    }
}
using System;
using System.Collections.Generic;
using System.IO;
using DrumPad.Styles;
using DrumPad.Utilits;
using Newtonsoft.Json;
using UnityEngine;
using Zenject;

namespace DrumPad.JsonBlock
{
    public class JsonManagerDrMpD
    {
        public event Action OnLoadAllJsons;
        
        public SaveJsonMusicList SaveJsMusicList { get; private set; }
        public SaveMusicMapJson SaveMusicMapJson { get; private set; }
        public MusicLessonsListJson MusicLessonsListJson { get; private set; }
        public JsonStyleList JsonStyleList { get; private set; }

        private readonly SpriteAtlasHolder _atlasHolder;

        [Inject]
        public JsonManagerDrMpD(SpriteAtlasHolder atlasHolder)
        {
            _atlasHolder = atlasHolder;
        }

        public void InitDrMpD()
        {
            string nameFileDrMpD = "MusicStyles.json";
            string pathToJsOnServerDrMpD = ConstantHolderPad.ROOT_FOLDER_ON_SERVER + nameFileDrMpD;

            DownloadMasterPad.NotSafeLoadTextFile(pathToJsOnServerDrMpD, CallBackLoadJson);
        }

        private void CallBackLoadJson(string jsonFileDrMpD)
        {
            JsonStyleList = JsonUtility.FromJson<JsonStyleList>(jsonFileDrMpD);

            foreach (var styleListDrMpD in JsonStyleList.AllStyles)
            {
                DataElementsDrMpD dataContentsDrMpD = new DataElementsDrMpD();

                int indexCountDrMpD = 1;
                
                foreach (var styleDrMpD in styleListDrMpD.Styles)
                {
                    string[] dataSplitDrMpD = styleDrMpD.Split('/');
                    int normalBpmDrMpD = 0;
                    if (int.TryParse(dataSplitDrMpD[^1], out int result))
                        normalBpmDrMpD = result;
                
                    DataContent dataContentDrMpD = new DataContent
                    {
                        KindName = styleListDrMpD.KindOfStyle,
                        StyleName = dataSplitDrMpD[0],
                        NormalBpm = normalBpmDrMpD
                    };

                    dataContentsDrMpD.DataMap.Add(dataSplitDrMpD[0], dataContentDrMpD);

                    dataContentDrMpD.SpriteElement = _atlasHolder.GetSpriteFromAtlas(dataContentDrMpD.KindName, indexCountDrMpD);
                    
                    indexCountDrMpD++;
                }

                if(!GameManagerDrum.GaM.DataContentsMap.ContainsKey(styleListDrMpD.KindOfStyle))
                    GameManagerDrum.GaM.DataContentsMap.Add(styleListDrMpD.KindOfStyle, dataContentsDrMpD);
            }

            LoadLessons();
        }

        private void LoadLessons()
        {
            string nameFileDrMpD = "LessonsAll.json";
            string pathToJsonFileDrMpD = PathToJson(nameFileDrMpD);
                
            if(!File.Exists(pathToJsonFileDrMpD))
            {
                string pathToJsOnServerDrMpD = ConstantHolderPad.ROOT_FOLDER_ON_SERVER + nameFileDrMpD;
                DownloadMasterPad.LoadTextFile(pathToJsOnServerDrMpD, nameFileDrMpD, s =>
                {
                    ContinueLoadJsons();
                });
            }
            else
            {
                ContinueLoadJsons();
            }
        }
        
        private void ContinueLoadJsons()
        {
            string pathFolderDrMpD = Path.Combine(ConstantHolderPad.GetRootPath(), ConstantHolderPad.ROOT_FOLDER_STYLES);
            
            if (!Directory.Exists(pathFolderDrMpD))
                Directory.CreateDirectory(pathFolderDrMpD);
            
            //Styles
            if (!File.Exists(PathToJson("Styles.json")))
            {
                SaveJsMusicList = new SaveJsonMusicList
                {
                    recentMusicStyles = new List<DataContent>()
                };
                
                
                File.WriteAllText(PathToJson("Styles.json"), JsonConvert.SerializeObject(SaveJsMusicList));
            }
            else
            {
                SaveJsMusicList = JsonConvert.DeserializeObject<SaveJsonMusicList>(File.ReadAllText(PathToJson("Styles.json")));
            }
            
            //Save musics
            if (!File.Exists(PathToJson("SaveMusics.json")))
            {
                SaveMusicMapJson = new SaveMusicMapJson
                {
                     SaveFileMusic = new Dictionary<string, DataContent>()
                };
                
                File.WriteAllText(PathToJson("SaveMusics.json"), JsonConvert.SerializeObject(SaveMusicMapJson));
            }
            else
            {
                SaveMusicMapJson = JsonConvert.DeserializeObject<SaveMusicMapJson>(File.ReadAllText(PathToJson("SaveMusics.json")));
            }
            
            //Lessons
            if (!File.Exists(PathToJson("Lessons.json")))
            {
                MusicLessonsListJson = new MusicLessonsListJson
                {
                    LessonJsons = new Dictionary<string, MusicLessonsJson>()
                };
                
                File.WriteAllText(PathToJson("Lessons.json"), JsonConvert.SerializeObject(MusicLessonsListJson));
            }
            else
            {
                MusicLessonsListJson = JsonConvert.DeserializeObject<MusicLessonsListJson>(File.ReadAllText(PathToJson("Lessons.json")));
            }
            
            OnLoadAllJsons?.Invoke();
        }
        

        private string PathToJson(string fileNameDrMpD)
        {
            string pathToJsonSaveDrMpD = 
                Path.Combine(ConstantHolderPad.GetRootPath(), ConstantHolderPad.ROOT_FOLDER_STYLES, fileNameDrMpD);
            return pathToJsonSaveDrMpD;
        }

        public void SaveJsonFileDrMpD()
        {
            string pathFolderDrMpD = Path.Combine(ConstantHolderPad.GetRootPath(), ConstantHolderPad.ROOT_FOLDER_STYLES);
            if (!Directory.Exists(pathFolderDrMpD))
                Directory.CreateDirectory(pathFolderDrMpD);
            
            if (File.Exists(PathToJson("Styles.json")))
            {
                File.WriteAllText(PathToJson("Styles.json"), JsonConvert.SerializeObject(SaveJsMusicList));
            }
            
            if (File.Exists(PathToJson("SaveMusics.json")))
            {
                File.WriteAllText(PathToJson("SaveMusics.json"), JsonConvert.SerializeObject(SaveMusicMapJson));
            }
            
            if (File.Exists(PathToJson("Lessons.json")))
            {
                File.WriteAllText(PathToJson("Lessons.json"), JsonConvert.SerializeObject(MusicLessonsListJson));
            }
        }
    }
}
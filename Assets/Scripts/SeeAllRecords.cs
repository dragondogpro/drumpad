using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeeAllRecords : MonoBehaviour
{

    [SerializeField] private GameObject _recordsScroll;
    [SerializeField] private GameObject _libraryMainScroll;
    [SerializeField] private GameObject _mainHeader;
    [SerializeField] private GameObject _recordsAllHeader;

    public void SeeAllRecordsButtonOnClick()
    {
        _mainHeader.SetActive(false);
        _recordsAllHeader.SetActive(true);
        
        _libraryMainScroll.gameObject.SetActive(false);
        
        _recordsScroll.GetComponent<RectTransform>().anchorMin += new Vector2(0, -0.50f);
    }

    public void BackToLibraryScreen()
    {
        _mainHeader.SetActive(true);
        _recordsAllHeader.SetActive(false);
        
        _libraryMainScroll.gameObject.SetActive(true);
        
        _recordsScroll.GetComponent<RectTransform>().anchorMin -= new Vector2(0, -0.50f);
    }
}

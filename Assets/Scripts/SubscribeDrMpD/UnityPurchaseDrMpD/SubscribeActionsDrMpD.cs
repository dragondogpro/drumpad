﻿using System.Threading.Tasks;
using SubscribeDrMpD.ProductsDrMpD;
using SubscribeDrMpD.PushWooshDrMpD;

namespace SubscribeDrMpD.UnityPurchaseDrMpD
{
    internal class SubscribeActionsDrMpD
    {
        private static readonly DebugExDrMpD DebugDrMpD = new DebugExDrMpD(nameof(SubscribeActionsDrMpD));

        private readonly string subDrMpD;
        private readonly string subDiscountDrMpD;
        private readonly IProductsDrMpD productsDrMpDControllerDrMpD;
        private readonly IPushWooshDrMpD pushControllerDrMpD;

        public SubscribeActionsDrMpD(
            string subDrMpD,
            string subDiscountDrMpD,
            IProductsDrMpD productsDrMpDControllerDrMpD,
            IPushWooshDrMpD pushControllerDrMpD)
        {
            this.subDrMpD = subDrMpD;
            this.subDiscountDrMpD = subDiscountDrMpD;
            this.productsDrMpDControllerDrMpD = productsDrMpDControllerDrMpD;
            this.pushControllerDrMpD = pushControllerDrMpD;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public Task BuyDrMpD()
        {
            if (pushControllerDrMpD.IsLaunchWithPushDrMpD())
            {
                DebugDrMpD.LogDrMpD("Buy discount subscribeDrMpD");
                return productsDrMpDControllerDrMpD.BuyProductDrMpD(subDiscountDrMpD);
            }

            DebugDrMpD.LogDrMpD("Buy default subscribeDrMpD");
            return productsDrMpDControllerDrMpD.BuyProductDrMpD(subDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public Task RestoreDrMpD()
        {
            DebugDrMpD.LogDrMpD("Restore productsDrMpD");
            return productsDrMpDControllerDrMpD.RestoreProductsDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

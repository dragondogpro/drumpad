﻿using System;
using SubscribeDrMpD.ScriptsDrMpD;
using SubscribeDrMpD.SubscribeDrMpD;
using UnityEngine;
using UnityEngine.Purchasing;

namespace SubscribeDrMpD.UnityPurchaseDrMpD
{
    internal delegate void OnProductProcessed(Product productDrMpD, PurchaseFailureReason? failureReasonDrMpD = null);

    internal class StoreListenerDrMpD : MonoBehaviour, IStoreListener
    {
        private static readonly DebugExDrMpD DebugDrMpD = new DebugExDrMpD(nameof(StoreListenerDrMpD));

        public event OnProductProcessed OnProductProcessed;

        private IStoreController storeController;
        private IExtensionProvider extensionProvider;

        private ISubscribeIdsDrMpD subscribe;
        private SubscribeBehaviourDrMpD behaviour;

        public void InitDrMpD(ISubscribeIdsDrMpD subscribeIdsDrMpD, SubscribeBehaviourDrMpD behaviourDrMpD)
        {
            this.subscribe = subscribeIdsDrMpD;
            this.behaviour = behaviourDrMpD;

            UnityPurchasing.Initialize(this, subscribe.ProductsDrMpDDrMpD.ConfigurationBuilderDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void OnInitialized(IStoreController controllerDrMpD, IExtensionProvider extensionsDrMpD)
        {
            DebugDrMpD.LogDrMpD("OnInitializedDrMpD");

            storeController = controllerDrMpD;
            extensionProvider = extensionsDrMpD;

            subscribe.ProductsDrMpDDrMpD.InitDrMpD(this, storeController, extensionProvider);
            subscribe.PushDrMpD.InitDrMpD(subscribe.ProductsDrMpDDrMpD);

            IStoreLoadDrMpD loadControllerDrMpD;

            if (subscribe.PushDrMpD.IsLaunchWithPushDrMpD())
            {
                loadControllerDrMpD = behaviour.PushDrMpD;
            }
            else
            {
                loadControllerDrMpD = behaviour.DefaultDrMpD;
            }

            if (subscribe.ProductsDrMpDDrMpD.HasProductsDrMpD())
            {
                loadControllerDrMpD.OnLoadApplicationDrMpD();
            }
            else
            {
                loadControllerDrMpD.OnLoadSubscribeDrMpD();
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
            
        }

        public void OnInitializeFailed(InitializationFailureReason errorDrMpD)
        {
            DebugDrMpD.LogDrMpD($"OnInitializeFailedDrMpD => {errorDrMpD}");
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEventDrMpD)
        {
            DebugDrMpD.LogDrMpD($"ProcessPurchaseDrMpD => {purchaseEventDrMpD.purchasedProduct.definition.id}");

            if (subscribe.ProductsDrMpDDrMpD.ContainsProductDrMpD(purchaseEventDrMpD.purchasedProduct))
            {
                var purchaseDateDrMpD = subscribe.ValidatorDrMpD.ValidateDrMpD(purchaseEventDrMpD.purchasedProduct) ?? DateTime.Now;

                subscribe.AdjustDrMpDDrMpD.ProcessPurchaseDrMpD(purchaseEventDrMpD.purchasedProduct, $"{purchaseDateDrMpD}");
                subscribe.PushDrMpD.ProcessPurchaseDrMpD();

                try
                {
                    behaviour.PurchaseDrMpD.OnPurchaseDrMpD();
                }
                catch (Exception exDrMpD)
                {
                    UnityEngine.Debug.Log(exDrMpD);
                }
            }
            else
            {
                DebugDrMpD.LogDrMpD($"Unrecognized productDrMpD => {purchaseEventDrMpD.purchasedProduct.definition.id}");
            }

            OnProductProcessed?.Invoke(purchaseEventDrMpD.purchasedProduct);

            return PurchaseProcessingResult.Complete;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void OnPurchaseFailed(Product productDrMpD, PurchaseFailureReason failureReasonDrMpD)
        {
            DebugDrMpD.LogDrMpD($"OnPurchaseFailedDrMpD => {productDrMpD.definition.id} {failureReasonDrMpD}");

            // if (failureReasonDrMpD == PurchaseFailureReason.UserCancelled || failureReasonDrMpD == PurchaseFailureReason.Unknown)
            // {
            //     try
            //     {
            //         behaviour.PurchaseDrMpD.OnFaliedDrMpD();
            //     }
            //     catch (Exception exDrMpD)
            //     {
            //         UnityEngine.Debug.Log(exDrMpD);
            //     }
            // }

            OnProductProcessed?.Invoke(productDrMpD, failureReasonDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        void OnApplicationPause(bool pauseStatusDrMpD)
        {
            subscribe.AdjustDrMpDDrMpD.PauseDrMpD(pauseStatusDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

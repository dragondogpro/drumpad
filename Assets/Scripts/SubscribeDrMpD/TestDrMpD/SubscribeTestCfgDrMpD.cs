﻿using System;
using UnityEngine;

namespace SubscribeDrMpD.TestDrMpD
{
    [Serializable]
    internal class SubscribeTestCfgDrMpD
    {
        [Header("Startup statesDrMpD")]
        [Tooltip("User already has any subscribe / User has't subscribeDrMpD ")]
        [SerializeField] private bool userHasSubscribeDrMpD;

        [Tooltip("User start application from push / Usually startDrMpD")]
        [SerializeField] private bool loadFromPushDrMpD;

        [Header("User actionsDrMpD")]
        [Tooltip("User cancel purchase, when buy productDrMpD")]
        [SerializeField] private bool userCancelPurchaseDrMpD;


        public bool UserHasSubscribeDrMpD => userHasSubscribeDrMpD;
        public bool LoadFromPushDrMpD => loadFromPushDrMpD;
        public bool UserCancelPurchaseDrMpD => userCancelPurchaseDrMpD;
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

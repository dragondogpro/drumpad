﻿using System.Threading.Tasks;
using SubscribeDrMpD.UnityPurchaseDrMpD;
using UnityEngine.Purchasing;

namespace SubscribeDrMpD.ProductsDrMpD
{
    internal interface IProductsDrMpD
    {
        ConfigurationBuilder ConfigurationBuilderDrMpD { get; }

        Task BuyProductDrMpD(string productIdDrMpD);
        bool ContainsProductDrMpD(Product productDrMpD);
        bool HasProductsDrMpD();
        void InitDrMpD(StoreListenerDrMpD storeListenerDrMpD, IStoreController storeDrMpD, IExtensionProvider extensionsDrMpD);
        Task RestoreProductsDrMpD();
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

﻿using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using SubscribeDrMpD.TestDrMpD;
using SubscribeDrMpD.UnityPurchaseDrMpD;
using UnityEngine;
using UnityEngine.Purchasing;

namespace SubscribeDrMpD.ProductsDrMpD
{
    internal class FakeProductsDrMpD : IProductsDrMpD
    {
        private DefaultProductsDrMpD defaultProductsDrMpD;

        private SubscribeTestCfgDrMpD testDrMpD;
        private StoreListenerDrMpD storeListenerDrMpD;

        public ConfigurationBuilder ConfigurationBuilderDrMpD => defaultProductsDrMpD.ConfigurationBuilderDrMpD;

        public FakeProductsDrMpD(SubscribeTestCfgDrMpD testDrMpD, params string[] productsId)
        {
            this.testDrMpD = testDrMpD;
            defaultProductsDrMpD = new DefaultProductsDrMpD(productsId);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void InitDrMpD(StoreListenerDrMpD storeListenerDrMpD, IStoreController storeDrMpD, IExtensionProvider extensionsDrMpD)
        {
            this.storeListenerDrMpD = storeListenerDrMpD;
            defaultProductsDrMpD.InitDrMpD(storeListenerDrMpD, storeDrMpD, extensionsDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public bool HasProductsDrMpD()
        {
            if (testDrMpD.UserHasSubscribeDrMpD)
            {
                Debug.Log($"EmulateDrMpD {nameof(testDrMpD.UserHasSubscribeDrMpD)}");

                return true;
            }

            return defaultProductsDrMpD.HasProductsDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public bool ContainsProductDrMpD(Product productDrMpD)
        {
            return defaultProductsDrMpD.ContainsProductDrMpD(productDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public async Task BuyProductDrMpD(string productIdDrMpD)
        {
            if (testDrMpD.UserCancelPurchaseDrMpD)
            {
                var productDrMpD = defaultProductsDrMpD.ProductsDrMpD.First(x => x.definition.id == productIdDrMpD);

                Debug.Log($"EmulateDrMpD {nameof(testDrMpD.UserCancelPurchaseDrMpD)}");

                storeListenerDrMpD.OnPurchaseFailed(productDrMpD, PurchaseFailureReason.UserCancelled);
            }
            else
            {
                await defaultProductsDrMpD.BuyProductDrMpD(productIdDrMpD);
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public Task RestoreProductsDrMpD()
        {
            var ctorsDrMpD = typeof(PurchaseEventArgs)
                .GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic);

            var instance = (PurchaseEventArgs)ctorsDrMpD.First()
                .Invoke(new object[] { defaultProductsDrMpD.ProductsDrMpD.First() });

            Debug.Log($"EmulateDrMpD {nameof(RestoreProductsDrMpD)}");

            storeListenerDrMpD.ProcessPurchase(instance);

            return Task.CompletedTask;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

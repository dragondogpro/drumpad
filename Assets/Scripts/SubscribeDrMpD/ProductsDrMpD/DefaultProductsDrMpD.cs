﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SubscribeDrMpD.UnityPurchaseDrMpD;
using UnityEngine;
using UnityEngine.Purchasing;

namespace SubscribeDrMpD.ProductsDrMpD
{
    internal class DefaultProductsDrMpD : IProductsDrMpD
    {
        private static readonly DebugExDrMpD DebugDrMpD = new DebugExDrMpD(nameof(DefaultProductsDrMpD));

        private const int maxResponseTime = 2500;

        private static bool processingDrMpD;

        private string[] productsIdDrMpD;
        public Product[] ProductsDrMpD { get; private set; }

        public ConfigurationBuilder ConfigurationBuilderDrMpD { get; private set; }

        protected StoreListenerDrMpD StoreListenerDrMpD;
        private IStoreController storeDrMpD;
        private IExtensionProvider extensionsDrMpD;

        public DefaultProductsDrMpD(params string[] productsIdDrMpD)
        {
            this.productsIdDrMpD = productsIdDrMpD;

            ConfigurationBuilderDrMpD = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            foreach (var productIdDrMpD in productsIdDrMpD)
            {
                ConfigurationBuilderDrMpD.AddProduct(productIdDrMpD, ProductType.Subscription, new IDs {
                    { productIdDrMpD, AppleAppStore.Name }
                });
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void InitDrMpD(StoreListenerDrMpD storeListenerDrMpD, IStoreController storeDrMpD, IExtensionProvider extensionsDrMpD)
        {
            this.StoreListenerDrMpD = storeListenerDrMpD;
            this.extensionsDrMpD = extensionsDrMpD;

            storeListenerDrMpD.OnProductProcessed += OnProductProcessedDrMpD;

            this.storeDrMpD = storeDrMpD;

            ProductsDrMpD = productsIdDrMpD.Select(x => storeDrMpD.products.WithID(x)).ToArray();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public bool HasProductsDrMpD()
        {
            return ProductsDrMpD.Any(x => x.hasReceipt);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public bool ContainsProductDrMpD(Product productDrMpD)
        {
            return ProductsDrMpD.Any(x => string.Equals(x.definition.id, productDrMpD.definition.id, StringComparison.Ordinal));
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public async Task BuyProductDrMpD(string productIdDrMpD)
        {
            await WaitQueleDrMpD();

            var taskDrMpD = LockDrMpD();

            var productDrMpD = storeDrMpD.products.WithID(productIdDrMpD);

            if (productDrMpD.availableToPurchase)
            {
                DebugDrMpD.LogDrMpD($"Purchasing productDrMpD => {productDrMpD.definition.id}");

                storeDrMpD.InitiatePurchase(productDrMpD);
            }

            await taskDrMpD;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public async Task RestoreProductsDrMpD()
        {
            await WaitQueleDrMpD();

            var taskDrMpD = LockDrMpD();

            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
            {
                DebugDrMpD.LogDrMpD("RestorePurchases startedDrMpD ...");

                var appleGirlsSkinsDrMpD = extensionsDrMpD.GetExtension<IAppleExtensions>();
                appleGirlsSkinsDrMpD.RestoreTransactions((resultGirlsSkinsDrMpD) =>
                {
                    DebugDrMpD.LogDrMpD($"RestorePurchases DrMpD=> {resultGirlsSkinsDrMpD}. DrMpDIf no further messages, no purchases available to restore.");
                });
            }
            else
            {
                DebugDrMpD.LogDrMpD($"RestorePurchases FAIL. Not supported on platformDrMpD => {Application.platform}");
            }

            await taskDrMpD;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void OnProductProcessedDrMpD(Product productDrMpD, PurchaseFailureReason? failureReasonDrMpD = null)
        {
            processingDrMpD = false;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private async Task LockDrMpD()
        {
            if (!processingDrMpD)
            {
                _ = Task.Run(async () =>
                {
                    await Task.Delay(maxResponseTime);
                    processingDrMpD = false;
                });
            }

            processingDrMpD = true;

            while (processingDrMpD)
            {
                await Task.Yield();
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private async Task WaitQueleDrMpD()
        {
            do
            {
                await Task.Yield();
            } while (processingDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

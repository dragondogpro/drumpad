﻿using System;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

namespace SubscribeDrMpD.ValidationDrMpD
{
    internal class DefaultValidatorDrMpD : IValidatorDrMpD
    {
        private static readonly DebugExDrMpD DebugDrMpD = new DebugExDrMpD(nameof(DefaultValidatorDrMpD));

        private CrossPlatformValidator validatorDrMpD;

        public DefaultValidatorDrMpD()
        {
            /*
                if isn't not compile
                Services -> In-App Purchasing -> Receipt Validation Obfuscator
                press: Obfuscate
            */

            validatorDrMpD = new CrossPlatformValidator(GooglePlayTangle.Data(),
                    AppleTangle.Data(), Application.identifier);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public DateTime? ValidateDrMpD(Product productDrMpD)
        {
            try
            {
                // On Google Play, result has a single product ID.
                // On Apple stores, receipts contain multiple products.
                var resultDrMpD = validatorDrMpD.Validate(productDrMpD.receipt);

                var logDrMpD = LogDrMpD(resultDrMpD);
                DebugDrMpD.LogDrMpD(logDrMpD);

                return resultDrMpD.Last().purchaseDate;

            }
            catch (IAPSecurityException)
            {
                DebugDrMpD.LogDrMpD("Invalid receipt, not unlocking contentDrMpD");
                return null;
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public static string LogDrMpD(IPurchaseReceipt[] receiptsDrMpD)
        {
            var logDrMpD = new StringBuilder();

            foreach (var receiptDrMpD in receiptsDrMpD)
            {
                logDrMpD.AppendLine($@"[Receipt]DrMpD
productIDDrMpD => {receiptDrMpD.productID}
purchaseDateDrMpD => {receiptDrMpD.purchaseDate}
transactionIDDrMpD => {receiptDrMpD.transactionID}");
            }

            return $"Success receipt\n{logDrMpD}DrMpD";
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

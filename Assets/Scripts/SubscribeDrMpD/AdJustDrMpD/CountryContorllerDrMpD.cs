﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine.Networking;

namespace SubscribeDrMpD.AdJustDrMpD
{
    internal class CountryContorllerDrMpD
    {
        private static readonly DebugExDrMpD DebugDrMpD = new DebugExDrMpD(nameof(CountryContorllerDrMpD));

        private string IpLinkDrMpD => "https://api.ipify.org";
        private string CountryLinkDrMpD => $"https://ipapi.co/{ipDrMpD}/json/";

        private string ipDrMpD;
        private CountryDrMpD countryDrMpDDrMpD;

        public string CountryCodeDrMpD => countryDrMpDDrMpD?.CountryCode;

        private CountryContorllerDrMpD() { }

        public static Task<CountryContorllerDrMpD> NewDrMpD()
        {
            return new CountryContorllerDrMpD().InitDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private async Task<CountryContorllerDrMpD> InitDrMpD()
        {
            ipDrMpD = await Send(IpLinkDrMpD);

            DebugDrMpD.LogDrMpD($"IpDrMpD => {ipDrMpD}");

            var countryDataDrMpD = await Send(CountryLinkDrMpD);

            countryDrMpDDrMpD = JsonConvert.DeserializeObject<CountryDrMpD>(countryDataDrMpD);

            DebugDrMpD.LogDrMpD($"CountryCodeDrMpD => {countryDrMpDDrMpD?.CountryCode}");

            return this;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private async Task<string> Send(string urlDrMpD)
        {
            using (UnityWebRequest webRequestDrMpD = UnityWebRequest.Get(urlDrMpD))
            {
                _ = webRequestDrMpD.SendWebRequest();

                while (!webRequestDrMpD.isDone)
                {
                    await Task.Yield();
                }

                return webRequestDrMpD.downloadHandler.text;
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        [Serializable]
        private class CountryDrMpD
        {
            [JsonProperty("country_code")]
            public string CountryCode { get; set; }
            
            private void RefMethodDrMpD()
            {
                bool refBoolDrMpD = false;
                if (refBoolDrMpD != false || refBoolDrMpD == true)
                    refBoolDrMpD = true;
            }
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

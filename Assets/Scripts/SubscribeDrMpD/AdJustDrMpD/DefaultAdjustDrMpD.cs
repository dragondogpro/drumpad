﻿using System.Threading.Tasks;
using com.adjust.sdk;
using UnityEngine.Purchasing;

namespace SubscribeDrMpD.AdJustDrMpD
{
    internal class DefaultAdjustDrMpD : IAdjustDrMpD
    {
        private static readonly DebugExDrMpD DebugDrMpD = new DebugExDrMpD(nameof(DefaultAdjustDrMpD));

        private string pauseCodeDrMpD;

        private CountryContorllerDrMpD countryContorllerDrMpDDrMpD;

        public static async Task<DefaultAdjustDrMpD> NewDrMpD(string pauseCodeDrMpD)
        {
            return new DefaultAdjustDrMpD
            {
                pauseCodeDrMpD = pauseCodeDrMpD,
                countryContorllerDrMpDDrMpD = await CountryContorllerDrMpD.NewDrMpD()
            };
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private DefaultAdjustDrMpD() { }

        public void PauseDrMpD(bool pauseStatusDrMpD)
        {
            if (pauseStatusDrMpD)
            {
                Adjust.trackEvent(adjustEvent: new AdjustEvent(pauseCodeDrMpD));
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void ProcessPurchaseDrMpD(Product productDrMpD, string purchaseDateDrMpD)
        {
            var subscriptionDrMpD = new AdjustAppStoreSubscription(
                productDrMpD.metadata.localizedPriceString,
                productDrMpD.metadata.isoCurrencyCode,
                productDrMpD.transactionID,
                productDrMpD.receipt);

            DebugDrMpD.LogDrMpD($"PRICE ISDrMpD {productDrMpD.metadata.localizedPriceString}");
            subscriptionDrMpD.setTransactionDate($"{purchaseDateDrMpD}DrMpD");
            subscriptionDrMpD.setSalesRegion(countryContorllerDrMpDDrMpD.CountryCodeDrMpD);

            Adjust.trackAppStoreSubscription(subscriptionDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

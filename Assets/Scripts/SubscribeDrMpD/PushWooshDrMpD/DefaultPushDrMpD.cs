﻿using System;
using Newtonsoft.Json;
using SubscribeDrMpD.ProductsDrMpD;

namespace SubscribeDrMpD.PushWooshDrMpD
{
    internal class DefaultPushDrMpD : IPushWooshDrMpD
    {
        private static readonly DebugExDrMpD DebugDrMpD = new DebugExDrMpD(nameof(DefaultPushDrMpD));

        private const string purchaseTagDrMpD = "Subscription purchased";

        private const int NOTHasProductTagValueDrMpD = 0;
        private const int HasProductTagValueDrMpD = 1;

        private string codeDrMpD;

        private string launchNotificationDrMpD;

        public DefaultPushDrMpD(string codeDrMpD)
        {
            this.codeDrMpD = codeDrMpD;

            Pushwoosh.ApplicationCode = this.codeDrMpD;
            Pushwoosh.Instance.OnRegisteredForPushNotifications += OnRegisteredForPushNotificationsDrMpD;
            Pushwoosh.Instance.OnFailedToRegisteredForPushNotifications += OnFailedToRegisteredForPushNotificationsDrMpD;
            Pushwoosh.Instance.OnPushNotificationsReceived += OnPushNotificationsReceivedDrMpD;
            Pushwoosh.Instance.RegisterForPushNotifications();

            DebugDrMpD.LogDrMpD($"Created with codeDrMpD => {codeDrMpD}");

            launchNotificationDrMpD = Pushwoosh.Instance.GetLaunchNotification();

            DebugDrMpD.LogDrMpD($"launchNotificationDrMpD => {launchNotificationDrMpD}");
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void InitDrMpD(IProductsDrMpD productsDrMpD)
        {
            var codeDrMpD = productsDrMpD.HasProductsDrMpD() ? HasProductTagValueDrMpD : NOTHasProductTagValueDrMpD;
            Pushwoosh.Instance.SetIntTag(purchaseTagDrMpD, codeDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public bool IsLaunchWithPushDrMpD()
        {
            if (!string.IsNullOrEmpty(launchNotificationDrMpD))
            {
                var pushDrMpD = JsonConvert.DeserializeObject<ContentPushDrMpD>(launchNotificationDrMpD);

                if (pushDrMpD?.UserDataDrMpD != null && pushDrMpD.UserDataDrMpD.Contains("trueDrMpD".Remove("trueDrMpD".Length - 5)))
                {
                    return true;
                }

                DebugDrMpD.ErrorDrMpD($"UserData null or not trueDrMpD => {pushDrMpD}");
            }

            return false;
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void ProcessPurchaseDrMpD()
        {
            Pushwoosh.Instance.SetIntTag(purchaseTagDrMpD, HasProductTagValueDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void OnRegisteredForPushNotificationsDrMpD(string tokenDrMpD)
        {
            DebugDrMpD.LogDrMpD("Received tokenDrMpD: \n" + tokenDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void OnFailedToRegisteredForPushNotificationsDrMpD(string errorDrMpD)
        {
            DebugDrMpD.LogDrMpD("Error ocurred while registering to push notificationsDrMpD: \n" + errorDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void OnPushNotificationsReceivedDrMpD(string payloadDrMpD)
        {
            DebugDrMpD.LogDrMpD("Received push notificaitonDrMpD: \n" + payloadDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        [Serializable]
        private class ContentPushDrMpD
        {
            [JsonProperty("l")]
            public string LinkDataDrMpD { get; set; }

            [JsonProperty("u")]
            public string UserDataDrMpD { get; set; }
            
            private void RefMethodDrMpD()
            {
                bool refBoolDrMpD = false;
                if (refBoolDrMpD != false || refBoolDrMpD == true)
                    refBoolDrMpD = true;
            }
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

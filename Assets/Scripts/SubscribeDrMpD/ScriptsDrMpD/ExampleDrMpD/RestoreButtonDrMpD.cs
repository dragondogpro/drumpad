﻿using UnityEngine;
using UnityEngine.UI;

namespace SubscribeDrMpD.ScriptsDrMpD.ExampleDrMpD
{
    [RequireComponent(typeof(Button))]
    internal class RestoreButtonDrMpD : MonoBehaviour
    {
        private Button buttonDrMpD;

        [SerializeField] private SubscribeControllerDrMpD subscribeDrMpD;

        void Awake()
        {
            buttonDrMpD = this.GetComponent<Button>();
            buttonDrMpD.onClick.AddListener(RestoreDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private async void RestoreDrMpD()
        {
            buttonDrMpD.interactable = false;

            try
            {
                await subscribeDrMpD.RestoreDrMpD();
            }
            finally
            {
                if(buttonDrMpD != null)
                {
                    buttonDrMpD.interactable = true;
                }
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

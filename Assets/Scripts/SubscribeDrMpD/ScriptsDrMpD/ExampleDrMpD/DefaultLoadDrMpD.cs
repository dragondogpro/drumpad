﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace SubscribeDrMpD.ScriptsDrMpD.ExampleDrMpD
{
    internal class DefaultLoadDrMpD : MonoBehaviour, IStoreLoadDrMpD
    {
        private const string mainSceneNameDrMpD = "MainScene";

        [SerializeField] private GameObject subscribePageDrMpD;
        [SerializeField] private OnSubscribeLoadDrMpD subscribeLoadDrMpD;

        public void OnLoadApplicationDrMpD()
        {
            LoadApplicationDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void OnLoadSubscribeDrMpD()
        {
            subscribePageDrMpD.SetActive(true);
            subscribeLoadDrMpD.LoadDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

#if !UNITY_EDITOR
        private static bool loadDrMpD = false;
#endif
        public static void LoadApplicationDrMpD()
        {
#if !UNITY_EDITOR
            if (loadDrMpD)
                return;

            loadDrMpD = true;
#endif

            SceneManager.LoadScene(mainSceneNameDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

namespace SubscribeDrMpD.ScriptsDrMpD.ExampleDrMpD
{
    internal class PushLoadDrMpD : MonoBehaviour, IStoreLoadDrMpD
    {
        [SerializeField] private GameObject subscribePageDrMpD;
        [SerializeField] private Button exitButtonDrMpD;
        [SerializeField] private OnSubscribeLoadDrMpD subscribeLoadDrMpDDrMpD;

        void Awake()
        {
            exitButtonDrMpD.onClick.AddListener(DefaultLoadDrMpD.LoadApplicationDrMpD);
            exitButtonDrMpD.gameObject.SetActive(false);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void OnLoadApplicationDrMpD()
        {
            subscribePageDrMpD.SetActive(true);
            exitButtonDrMpD.gameObject.SetActive(true);
            subscribeLoadDrMpDDrMpD.LoadDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        public void OnLoadSubscribeDrMpD()
        {
            subscribePageDrMpD.SetActive(true);
            exitButtonDrMpD.gameObject.SetActive(false);
            subscribeLoadDrMpDDrMpD.LoadDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

namespace SubscribeDrMpD.ScriptsDrMpD.ExampleDrMpD
{
    internal class CancelDialogDrMpD : MonoBehaviour
    {
        [SerializeField] private SubscribeControllerDrMpD subscribeDrMpD;
        [SerializeField] private Button yesButtonDrMpD;
        [SerializeField] private Button noButtonDrMpD;

        void Awake()
        {
            yesButtonDrMpD.onClick.AddListener(YesDrMpD);
            noButtonDrMpD.onClick.AddListener(NoDrMpD);
            this.gameObject.SetActive(false);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private async void YesDrMpD()
        {
            yesButtonDrMpD.interactable = false;
            try
            {
                await subscribeDrMpD.BuyDrMpD();
            }
            finally
            {
                if(yesButtonDrMpD != null)
                {
                    yesButtonDrMpD.interactable = true;
                }
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void NoDrMpD()
        {
            this.gameObject.SetActive(false);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

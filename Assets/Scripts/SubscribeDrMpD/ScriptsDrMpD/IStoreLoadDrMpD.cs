﻿namespace SubscribeDrMpD.ScriptsDrMpD
{
    /// <summary>
    /// This interface should implement an action that is called when store is loaded on application startup
    /// </summary>
    interface IStoreLoadDrMpD
    {
        /// <summary>
        /// Сalled if user does NOT have a subscription
        /// </summary>
        void OnLoadSubscribeDrMpD();
        /// <summary>
        /// Сalled if user does have a subscription
        /// </summary>
        void OnLoadApplicationDrMpD();
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

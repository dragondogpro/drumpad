﻿namespace SubscribeDrMpD.ScriptsDrMpD
{
    /// <summary>
    /// This interface should implement an action when the user interacts with the native AppStore window, initialized method group "Buy"
    /// </summary>
    internal interface IPurchaseActionsDrMpD
    {
        /// <summary>
        /// Сalled when user success buy a subscription
        /// </summary>
        void OnPurchaseDrMpD();

        /// <summary>
        /// Сalled when user decline buy a subscription
        /// </summary>
        void OnFaliedDrMpD();
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

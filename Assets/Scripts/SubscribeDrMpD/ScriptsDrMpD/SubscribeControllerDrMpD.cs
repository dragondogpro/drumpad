﻿using System;
using System.Threading.Tasks;
using com.adjust.sdk;
using SubscribeDrMpD.SubscribeDrMpD;
using SubscribeDrMpD.TestDrMpD;
using SubscribeDrMpD.UnityPurchaseDrMpD;
using UnityEngine;

namespace SubscribeDrMpD.ScriptsDrMpD
{
    internal class SubscribeControllerDrMpD : MonoBehaviour
    {
        //private const string defTestCodeDrMpD = "test";

        [Header("SubscribeDrMpD")]
        [Tooltip("Product id for buy standart subscribe\nExample: test.test.testDrMpD")]
        [SerializeField] private string defaultProductIdDrMpD;
        [Tooltip("Product id for buy subscribe with discount\nExample: test.test.testDrMpD")]
        [SerializeField] private string discountProductIdDrMpD;

        [Header("PushWooshDrMpD")]
        [Tooltip("Code for PushWoosh notification\nExample: XXXXX-XXXXXDrMpD")]
        [SerializeField] private string codeDrMpD;

        [Header("AdjustDrMpD")]
        [Tooltip("Adjust environment\nfor test: Sandbox, for release: Production, for editor: (Any)DrMpD")]
        [SerializeField] private AdjustEnvironment adjustEnvironmentDrMpD;
        [Tooltip("Application code for Adjust events\nExample: xxxxxxxxxxDrMpD")]
        [SerializeField] private string appTokenDrMpD;
        [Tooltip("Adjust code for event when user pause application\nExample: xxxxxxDrMpD")]
        [SerializeField] private string exitDrMpD;

        [Header("BehavoiurDrMpD")]
        [Tooltip("You default IStoreLoad object (see interface summary)DrMpD")]
        [SerializeField] private MonoBehaviour defaultLoadDrMpD;
        [Tooltip("You push IStoreLoad object (see interface summary)DrMpD")]
        [SerializeField] private MonoBehaviour pushLoadDrMpD;
        [Tooltip("You IPurchaseActions object (see interface summary)DrMpD")]
        [SerializeField] private MonoBehaviour purchaseDrMpD;

        [Tooltip("Emulate configuration. Only used in EditorDrMpD")]
        [SerializeField] private SubscribeTestCfgDrMpD testDrMpD;

        private SubscribeActionsDrMpD actionsDrMpD;

        async void Awake()
        {
            CheckCodeDrMpD(ref defaultProductIdDrMpD, nameof(defaultProductIdDrMpD));
            CheckCodeDrMpD(ref discountProductIdDrMpD, nameof(discountProductIdDrMpD));
            CheckCodeDrMpD(ref codeDrMpD, nameof(codeDrMpD));
            CheckCodeDrMpD(ref appTokenDrMpD, nameof(appTokenDrMpD));
            CheckCodeDrMpD(ref exitDrMpD, nameof(exitDrMpD));

#if UNITY_EDITOR
            await Task.Delay(1000);
            var subscribeIdsDrMpD = new FakeIdsDrMpD(defaultProductIdDrMpD, discountProductIdDrMpD, testDrMpD);
#else
            var subscribeIdsDrMpD = await DefaultIdsDrMpD.NewDrMpD(defaultProductIdDrMpD, discountProductIdDrMpD, codeDrMpD, exitDrMpD);
#endif

            var defaultLoadDrMpD = this.defaultLoadDrMpD as IStoreLoadDrMpD ?? throw new ArgumentException(nameof(this.defaultLoadDrMpD));
            var pushLoadDrMpD = this.pushLoadDrMpD as IStoreLoadDrMpD ?? throw new ArgumentException(nameof(this.pushLoadDrMpD));
            var purchaseDrMpD = this.purchaseDrMpD as IPurchaseActionsDrMpD ?? throw new ArgumentException(nameof(this.purchaseDrMpD));

            var behavoiurDrMpD = new SubscribeBehaviourDrMpD(defaultLoadDrMpD, pushLoadDrMpD, purchaseDrMpD);

            InitAdjustDrMpD();

            var storeDrMpD = this.gameObject.AddComponent<StoreListenerDrMpD>();
            storeDrMpD.InitDrMpD(subscribeIdsDrMpD, behavoiurDrMpD);

            actionsDrMpD = new SubscribeActionsDrMpD(defaultProductIdDrMpD, discountProductIdDrMpD,
                subscribeIdsDrMpD.ProductsDrMpDDrMpD, subscribeIdsDrMpD.PushDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        /// <summary>
        /// Buy defautl or discont subscribe(depends on application start), use in onClick with you button
        /// </summary>
        /// <returns>May be awaited, for use to button.interactable</returns>
        public Task BuyDrMpD()
        {
            return actionsDrMpD.BuyDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        } 
        
        /// <summary> For button event in Editor </summary>
        public async void BuyVoidDrMpD()
        {
            await actionsDrMpD.BuyDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        /// <summary>
        /// Restore all Products
        /// </summary>
        /// <returns>Return when first product been resotred</returns>
        public Task RestoreDrMpD()
        {
            return actionsDrMpD.RestoreDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        /// <summary> For button event in Editor </summary>
        public async void RestoreVoidDrMpD()
        {
            await actionsDrMpD.RestoreDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void InitAdjustDrMpD()
        {
            
            var adjustObjDrMpD = new GameObject("DO NOT DELETE - AdjustDrMpD");
            adjustObjDrMpD.SetActive(false);
            var ajustDrMpD = adjustObjDrMpD.AddComponent<Adjust>();
            ajustDrMpD.startManually = false;
            ajustDrMpD.appToken = appTokenDrMpD;
            ajustDrMpD.environment = adjustEnvironmentDrMpD;
            ajustDrMpD.logLevel = AdjustLogLevel.Verbose;
            adjustObjDrMpD.SetActive(true);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private void CheckCodeDrMpD(ref string codeDrMpD, string nameDrMpD)
        {
            if (string.IsNullOrWhiteSpace(codeDrMpD))
            {
#if UNITY_EDITOR
                codeDrMpD = "testDrMpD".Remove("testDrMpD".Length - 5);
                Debug.LogWarning($"{nameDrMpD} empty, setDrMpD \"{"testDrMpD".Remove("testDrMpD".Length - 5)}\"");
#else
                Debug.LogException(new NullReferenceException(name));
#endif
            }
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

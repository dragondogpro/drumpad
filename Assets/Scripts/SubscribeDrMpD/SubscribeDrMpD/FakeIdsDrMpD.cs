﻿using SubscribeDrMpD.AdJustDrMpD;
using SubscribeDrMpD.ProductsDrMpD;
using SubscribeDrMpD.PushWooshDrMpD;
using SubscribeDrMpD.TestDrMpD;
using SubscribeDrMpD.ValidationDrMpD;

namespace SubscribeDrMpD.SubscribeDrMpD
{
    internal class FakeIdsDrMpD : ISubscribeIdsDrMpD
    {
        public IProductsDrMpD ProductsDrMpDDrMpD { get; private set; }
        public IAdjustDrMpD AdjustDrMpDDrMpD { get; private set; }
        public IPushWooshDrMpD PushDrMpD { get; private set; }
        public IValidatorDrMpD ValidatorDrMpD { get; private set; }

        public FakeIdsDrMpD(string subDrMpD, string subDiscountDrMpD, SubscribeTestCfgDrMpD testDrMpD)
        {
            ProductsDrMpDDrMpD = new FakeProductsDrMpD(testDrMpD, subDrMpD, subDiscountDrMpD);
            PushDrMpD = new FakePushDrMpD(testDrMpD);
            AdjustDrMpDDrMpD = new FakeAdjustDrMpDDrMpD();
            ValidatorDrMpD = new FakeValidatorDrMpD();
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

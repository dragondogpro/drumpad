﻿using System.Threading.Tasks;
using SubscribeDrMpD.AdJustDrMpD;
using SubscribeDrMpD.ProductsDrMpD;
using SubscribeDrMpD.PushWooshDrMpD;
using SubscribeDrMpD.ValidationDrMpD;

namespace SubscribeDrMpD.SubscribeDrMpD
{
    internal class DefaultIdsDrMpD : ISubscribeIdsDrMpD
    {
        public IProductsDrMpD ProductsDrMpDDrMpD { get; private set; }
        public IAdjustDrMpD AdjustDrMpDDrMpD { get; private set; }
        public IPushWooshDrMpD PushDrMpD { get; private set; }
        public IValidatorDrMpD ValidatorDrMpD { get; private set; }

        /// <summary> Create subscripe from Ids </summary>
        /// <param name="subDrMpD">example: name.name.name</param>
        /// <param name="subDiscountDrMpD">example: name.name.name</param>
        /// <param name="pushWooshDrMpD">example: XXXXX-XXXXX</param>
        /// <param name="adjustExitDrMpD">example: xxxxxx</param>
        public async static Task<DefaultIdsDrMpD> NewDrMpD(
            string subDrMpD,
            string subDiscountDrMpD,
            string pushWooshDrMpD,
            string adjustExitDrMpD)
        {
            return new DefaultIdsDrMpD
            {
                ProductsDrMpDDrMpD = new DefaultProductsDrMpD(subDrMpD, subDiscountDrMpD),
                PushDrMpD = new DefaultPushDrMpD(pushWooshDrMpD),
                AdjustDrMpDDrMpD = await DefaultAdjustDrMpD.NewDrMpD(adjustExitDrMpD),
                ValidatorDrMpD = new DefaultValidatorDrMpD()
            };
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }

        private DefaultIdsDrMpD() { }
        
        private void RefMethodDrMpD()
        {
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
    }
}

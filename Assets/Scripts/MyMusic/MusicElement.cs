using System.Collections;
using System.IO;
using DG.Tweening;
using DrumPad.Utilits;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.MyMusic
{
    public class MusicElement : MonoBehaviour
    {
        internal AudioClip GetMyAudioClip => _audioClipPad;
        
        [SerializeField] private Image titleImagePadMusic;
        [SerializeField] private Slider fillBarMusic;
        
        [SerializeField] private Text nameMusicSong;
        [SerializeField] private Text dataMusicSong;
        [SerializeField] private Text timeMusicSong;

        [SerializeField] private Button playMusicButton;
        [SerializeField] private Button moreSettings;

        private GameObject _playObjectPad;
        private GameObject _pauseObjectPad;

        private AudioClip _audioClipPad;
        private FileInfo _fileInfoPad;

        private SliderPointFill _sliderPointFill;
        private MusicHolderManager _musicHolderManager;
        
        private int _secondsPad;
        private int _minutesPad;

        private bool _isPlayingMusic;

        private bool IsPlayingMusic
        {
            get => _isPlayingMusic;

            set
            {
                _isPlayingMusic = value;
                
                _playObjectPad.SetActive(!_isPlayingMusic);
                _pauseObjectPad.SetActive(_isPlayingMusic);
                
                print($"Select: {_isPlayingMusic}");
            }
        }
        
        public void Initialization(AudioClip audioClip, FileInfo fileInfo, MusicHolderManager holderManager)
        {
            _fileInfoPad = fileInfo;
            _musicHolderManager = holderManager;
            _audioClipPad = audioClip;
            
            nameMusicSong.text = _fileInfoPad.Name.Replace(StringHelperDrMpD.WavDrMpD, "");
            dataMusicSong.text = $"{_fileInfoPad.CreationTime.Day}.{_fileInfoPad.CreationTime.Month:00}.{_fileInfoPad.CreationTime.Year}";
            
            _secondsPad = (int)(_audioClipPad.length * 0.5f);
            _minutesPad = _secondsPad / 60;
            _secondsPad = _secondsPad - 60 * _minutesPad;
            VisualizationTime();

            _secondsPad = 0;
            _minutesPad = 0;
            
            _playObjectPad = playMusicButton.gameObject.transform.GetChild(0).gameObject;
            _pauseObjectPad = playMusicButton.gameObject.transform.GetChild(1).gameObject;

            _sliderPointFill = GetComponentInChildren<SliderPointFill>();
            
            playMusicButton.onClick.AddListener(OnClickPlayButtonDrMpD);
            moreSettings.onClick.AddListener(ClickMoreButtonDrMpD);
            
            fillBarMusic.onValueChanged.AddListener(OnChangeValue);
   
            _sliderPointFill.OnEndDragHandler += OnEndDragSlider;

            DataContent dataContent = GameManagerDrum.GaM.JsonManagerDrMpD.SaveMusicMapJson.SaveFileMusic[fileInfo.Name];
            dataContent = GameManagerDrum.GaM.DataContentsMap[dataContent.KindName].GetElement(dataContent.StyleName);
            
            titleImagePadMusic.sprite = dataContent.SpriteElement;
            _pauseObjectPad.SetActive(false);
        }

        private void ClickMoreButtonDrMpD()
        {
            PausePlayMusicDrMpD();
/*#if UNITY_EDITOR
#endif*/
            GameManagerDrum.GaM.DownPanelButtonManager.OpenCloseButton(false);
            _musicHolderManager.ClickMoreSetting(_fileInfoPad);
        }

        private void OnClickPlayButtonDrMpD()
        {
            if (IsPlayingMusic)
            {
                PausePlayMusicDrMpD();
            }
            else
            {
                PlayMusicDrMpD();
            }
        }
        
        private void PlayMusicDrMpD()
        {
            IsPlayingMusic = true;
            
            int startIndex = _secondsPad + _minutesPad * 60;
            float durationSong = _audioClipPad.length / 2 - startIndex;
            print($"Play: Dur({durationSong}) StartIndex({startIndex})");
            _musicHolderManager.StartPlayMusic(startIndex, this);
            
            StartCoroutine(TextCountRoutine(startIndex));

            fillBarMusic.DOValue(1.0f, durationSong).SetEase(Ease.Linear);
        }

        internal void PausePlayMusicDrMpD()
        {
            print("Pause");
            StopAllCoroutines();
            IsPlayingMusic = false;
            DOTween.KillAll();
            
            _musicHolderManager.PausePlayMusic();
        }

        IEnumerator TextCountRoutine(int startIndex)
        {
            _secondsPad = startIndex;
            _minutesPad = 0;
            
            _minutesPad = _secondsPad / 60;
            _secondsPad = _secondsPad - 60 * _minutesPad;

            // VisualizationTime();
            
            for (int i = startIndex; i < _audioClipPad.length / 2; i++)
            {
                yield return new WaitForSeconds(1.0f);
                _secondsPad++;
                if (_secondsPad == 60)
                {
                    _minutesPad++;
                    _secondsPad = 0;
                }

                // VisualizationTime();
            }
            
            _secondsPad = 0;
            _minutesPad = 0;
            fillBarMusic.value = 0.0f;
            // VisualizationTime();
            IsPlayingMusic = false;
        }

        private void OnChangeValue(float newValue)
        {
            if(!_sliderPointFill.IsDragNow) return;
            
            if (IsPlayingMusic)
            {
                StopAllCoroutines();
                DOTween.KillAll();
                _musicHolderManager.PausePlayMusic();
            }

            int currentSecond = (int) (newValue * _audioClipPad.length / 2);
            _minutesPad = currentSecond / 60;
            _secondsPad = currentSecond - 60 * _minutesPad;
            
            // VisualizationTime();
        }
        private void OnEndDragSlider()
        {
            if (IsPlayingMusic)
            {
                PlayMusicDrMpD();
            }
        }

        private void VisualizationTime() => timeMusicSong.text = $"{_minutesPad:00}:{_secondsPad:00}";
    }
}
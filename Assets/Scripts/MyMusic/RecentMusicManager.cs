using System.Collections.Generic;
using DrumPad.Styles;
using DrumPad.Utilits;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.MyMusic
{
    public class RecentMusicManager : MonoBehaviour, IManager
    {
        [SerializeField] private StyleMusicElement stylePrefab;
        private RectTransform _listContent;
        private GameObject _emptyBlock;

        private readonly Dictionary<DataContent, GameObject> _listRecentMusic = new ();
        
        public void InitializationDrMpD()
        {
            _listContent = GetComponent<ScrollRect>().content;

            foreach (var styleNameDrMpD in GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsMusicList.recentMusicStyles)
            {
                DataContent dataContentDrMpD = GameManagerDrum.GaM.DataContentsMap[styleNameDrMpD.KindName].GetElement(styleNameDrMpD.StyleName);
                AddNewElement(dataContentDrMpD);
            }
            
            _emptyBlock = Instantiate(
                new GameObject("[Empty object]", typeof(RectTransform)), 
                _listContent);
            _emptyBlock.SetActive(_listRecentMusic.Count < 2);
            
            GlobalEventSystem.OnSelectNewStyle += SelectMusicElement;
        }

        private void SelectMusicElement(DataContent dataContentDrMpD)
        {
            print(dataContentDrMpD.StyleName);

            GameObject selectElementDrMpD = null;
            foreach (var dataDrMpD in _listRecentMusic.Keys)
            {
                if (dataDrMpD.StyleName == dataContentDrMpD.StyleName)
                {
                    selectElementDrMpD = _listRecentMusic[dataDrMpD];
                    break;
                }
            }

            if (selectElementDrMpD == null)
                selectElementDrMpD = AddNewElement(dataContentDrMpD);

            _emptyBlock.SetActive(_listRecentMusic.Count < 2);
            
            MoveObjectToUpperHierarchyHelper.MoveObjectDrMpD(selectElementDrMpD.transform);
            GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsMusicList.SelectMusicStyle(dataContentDrMpD);
        }

        private GameObject AddNewElement(DataContent dataContentDrMpD)
        {
            MusicPackElement musicPackElementDrMpD = new MusicPackElement();
            musicPackElementDrMpD.InitElementBlock(dataContentDrMpD);

            StyleMusicElement styleMusicElement = Instantiate(stylePrefab, _listContent);
            styleMusicElement.Initialization(musicPackElementDrMpD);
            
            if(!_listRecentMusic.ContainsKey(dataContentDrMpD))
                _listRecentMusic.Add(dataContentDrMpD, styleMusicElement.gameObject);

            return styleMusicElement.gameObject;
        }
    }
}
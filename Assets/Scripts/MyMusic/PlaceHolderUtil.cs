using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DrumPad.MyMusic
{
    internal class PlaceHolderUtil : MonoBehaviour, IInitialization, IManager
    {
        [FormerlySerializedAs("inputText")] [SerializeField] private InputField inputTextDrMpD;
        [SerializeField] private Button buttonOkPad;
        [SerializeField] private Button buttonCancelPad;

        private Action _onSelectCancelButton;
        private Action<string> _onSelectOkButton;
        
        public void InitializationDrMpD()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            buttonOkPad.onClick.AddListener(ClickButtonOk);
            buttonCancelPad.onClick.AddListener(ClickButtonCancel);
        }

        internal void SelectPlaceholder(string startTextDrMpD ,Action<string> selectOkButton, Action selectCancelButton)
        {
            inputTextDrMpD.text = startTextDrMpD;
            
            _onSelectOkButton = selectOkButton;
            _onSelectCancelButton = selectCancelButton;
        }

        private void ClickButtonOk()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            _onSelectOkButton?.Invoke(inputTextDrMpD.text);
            EndActionDrMpD();
        }
        
        private void ClickButtonCancel()
        {
            if (inputTextDrMpD != null)
            {
                int klmDrMpD = 1243;
            }
            
            _onSelectCancelButton?.Invoke();
            EndActionDrMpD();
        }

        private void EndActionDrMpD()
        {
            inputTextDrMpD.text = "";
            if(inputTextDrMpD.text == "")
                gameObject.SetActive(false);
        }
    }
}
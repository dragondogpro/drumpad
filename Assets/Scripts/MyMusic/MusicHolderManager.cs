using System.Collections.Generic;
using System.IO;
using System.Linq;
using DrumPad.Utilits;
using IOSBridge;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.MyMusic
{
    public class MusicHolderManager : MonoBehaviour, IInitialization, IManager
    {
        [SerializeField] private AudioSource audioSourcePlayMyMusic;
        
        [SerializeField] private RectTransform myMusicRectHolder;

        [SerializeField] private MusicElement musicPrefab;
        
        [SerializeField] private RecordMusicMoreSettings recordMusicMoreSettings;
        [SerializeField] private Button _createFirstSong;
        
        private MusicElement _currentPlayElement;
        
        private readonly Dictionary<string,GameObject> _allMusicFiles = new Dictionary<string, GameObject>();

        public void InitializationDrMpD()
        {
            string pathToHolderMusic = Path.Combine(ConstantHolderPad.GetRootPath(), "Capture");
            
            print("Init MusicHolderManager");
            
            recordMusicMoreSettings.InitializationDrMpD();
//#if UNITY_EDITOR
            recordMusicMoreSettings.AnimationCloseOpenPanel(false);
//#endif
            _createFirstSong.onClick.AddListener(CreateFirstSong);
            
            _createFirstSong.gameObject.SetActive(GameManagerDrum.GaM.JsonManagerDrMpD.SaveMusicMapJson.SaveFileMusic.Count == 0);
            
            if(!Directory.Exists(pathToHolderMusic)) return;
            
            foreach (var musicFileDrMpD in Directory.GetFiles(pathToHolderMusic))
            {
                FileInfo musicFilePad = new FileInfo(musicFileDrMpD);
                
                if (musicFilePad.Name.Contains(".meta")) continue;
                
                MusicElement musicElementDrMpD = Instantiate(musicPrefab, myMusicRectHolder);
                AudioClip recordClip = WavUtility.ToAudioClip(musicFileDrMpD);

                musicElementDrMpD.Initialization(recordClip, musicFilePad, this);
                
                _allMusicFiles.Add(musicFilePad.FullName, musicElementDrMpD.gameObject);
            }
        }

        internal void ClickMoreSetting(FileInfo fileInfoDrMpD)
        {
            recordMusicMoreSettings.gameObject.SetActive(true);
            recordMusicMoreSettings.AnimationCloseOpenPanel(true);
            recordMusicMoreSettings.SelectRecordSong(fileInfoDrMpD);
        }

        internal void UpdateMusicList()
        {
            string pathToHolderMusicDrMpD = Path.Combine(ConstantHolderPad.GetRootPath(), "Capture");
            
            if (!Directory.Exists(pathToHolderMusicDrMpD))
                Directory.CreateDirectory(pathToHolderMusicDrMpD);
            
            foreach (var fileInfoDrMpD in _allMusicFiles.Keys.ToList())
            {
                if(!File.Exists(fileInfoDrMpD))
                {
                    Destroy(_allMusicFiles[fileInfoDrMpD]);
                    _allMusicFiles.Remove(fileInfoDrMpD);
                }
            }
            
            foreach (var musicFileDrMpD in Directory.GetFiles(pathToHolderMusicDrMpD))
            {
                FileInfo musicFilePadDrMpD = new FileInfo(musicFileDrMpD);
                
                if (musicFilePadDrMpD.Name.Contains(".meta")) continue;

                if(!_allMusicFiles.ContainsKey(musicFilePadDrMpD.FullName))
                {
                    MusicElement musicElementDrMpD = Instantiate(musicPrefab, myMusicRectHolder);
                    AudioClip recordClipDrMpD = WavUtility.ToAudioClip(musicFileDrMpD);

                    musicElementDrMpD.Initialization(recordClipDrMpD, musicFilePadDrMpD, this);
                    _allMusicFiles.Add(musicFilePadDrMpD.FullName, musicElementDrMpD.gameObject);
                }
            }
            
            _createFirstSong.gameObject.SetActive(GameManagerDrum.GaM.JsonManagerDrMpD.SaveMusicMapJson.SaveFileMusic.Count == 0);
        }

        internal void StartPlayMusic(int startIndex, MusicElement musicElementDrMpD)
        {
            if(_currentPlayElement != null)
                _currentPlayElement.PausePlayMusicDrMpD();
            
            _currentPlayElement = musicElementDrMpD;

            audioSourcePlayMyMusic.clip = _currentPlayElement.GetMyAudioClip;
            audioSourcePlayMyMusic.time = startIndex;
            audioSourcePlayMyMusic.Play();
        }

        internal void PausePlayMusic()
        {
            print("Pause Play Music");
            _currentPlayElement = null;
            audioSourcePlayMyMusic.Stop();
        }

        private void CreateFirstSong()
        {
            if (GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsMusicList.recentMusicStyles.Count > 0)
            {
                DataContent dataContentDrMpD = GameManagerDrum.GaM.JsonManagerDrMpD.SaveJsMusicList.recentMusicStyles[0];
                MusicPackElement musicPackElement = new MusicPackElement();
                musicPackElement.InitElementBlock(dataContentDrMpD);
                GlobalEventSystem.SelectNewStyle(dataContentDrMpD);
                GameManagerDrum.GaM.DownPanelButtonManager.OpenCloseButton(false);
                GameManagerDrum.GaM.MyMusicManager.RecordManager.gameObject.SetActive(true);
                GameManagerDrum.GaM.MyMusicManager.RecordManager.SelectMusicStyle(musicPackElement);
            }
            else
            {
                GlobalEventSystem.ClickButtonWindow(WindowEnumPad.LIBRARY);
                IOStoUnityBridge.ShowAlert(StringHelperDrMpD.MusicDrMpD, "Choose the style of music you want to play and download!");
            }
        }
    }
}

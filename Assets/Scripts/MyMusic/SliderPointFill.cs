using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DrumPad.MyMusic
{
    internal class SliderPointFill : Slider, IEndDragHandler
    {
        public bool IsDragNow { get; private set; }
        
        internal event Action OnEndDragHandler;

        public void OnEndDrag(PointerEventData eventData)
        {
            if (eventData != null)
            {
                IsDragNow = false;
                OnEndDragHandler?.Invoke();
            }
        }

        public override void OnDrag(PointerEventData eventData)
        {
            if (eventData != null)
            {
                base.OnDrag(eventData);
                IsDragNow = true;
            }
        }
    }
}
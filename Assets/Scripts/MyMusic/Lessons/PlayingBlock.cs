using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    public class PlayingBlock : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private Text _listenText;
        [SerializeField] private Text _playText;
        [SerializeField] private Color _selectedColorDrMpD;
        [SerializeField] private Color _defaultColorDrMpD;

        public void OnPointerDown(PointerEventData eventData)
        {
            _listenText.color = _defaultColorDrMpD;
            _playText.color = _selectedColorDrMpD;
            gameObject.SetActive(false);
        }
    }
}
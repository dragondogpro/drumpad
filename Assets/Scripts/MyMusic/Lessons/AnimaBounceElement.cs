using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

namespace DrumPad.MyMusic.Lessons
{
    public class AnimaBounceElement : MonoBehaviour, IInitialization
    {
        [SerializeField] private AnimationCurve bouncedCurve;

        private RectTransform _rectTransformDrMpD;

        private Vector2 _startVector;
        private float _maxDuration;
        private float _endPointPos;

        public void InitializationDrMpD()
        {
            _rectTransformDrMpD = GetComponent<RectTransform>();
            _rectTransformDrMpD.localPosition = new Vector3(-225, -600);
        }
        
        public void PlayAnimationDrMpD(float duration, int endPoint)
        {
            // print("Current duration: " + duration);
            if(duration == 0) return;
            
            switch (endPoint)
            {
                case 1:
                    _endPointPos = -225;
                    break;
                case 2:
                    _endPointPos = 90;
                    break;
                case 3:
                    _endPointPos = 420;
                    break;
            }

            var localPositionDrMpD = _rectTransformDrMpD.localPosition;
            localPositionDrMpD = new Vector3(localPositionDrMpD.x, -600f);
            _rectTransformDrMpD.localPosition = localPositionDrMpD;

            _startVector = localPositionDrMpD;

            _endPointPos = _endPointPos - _startVector.x;
            float currentDurationDrMpD = duration;
            _maxDuration = currentDurationDrMpD;

            CorrectPosition(currentDurationDrMpD);
            
            StopAllCoroutines();
            StartCoroutine(AnimaRoutine());
            
            IEnumerator AnimaRoutine()
            {
                while (currentDurationDrMpD > 0)
                {
                    yield return new WaitForSeconds(0.01f);
                    currentDurationDrMpD -= 0.01f;
                    CorrectPosition(currentDurationDrMpD);
                }
            }
        }

        private void CorrectPosition(float curDurDrMpD)
        {
            float timeCurveDrMpD = 1.0f - curDurDrMpD / _maxDuration;
            Vector2 currentVector;
            currentVector.y = _startVector.y * bouncedCurve.Evaluate(timeCurveDrMpD);
            currentVector.x = _endPointPos * timeCurveDrMpD + _startVector.x;

            _rectTransformDrMpD.localPosition = currentVector;
        }
    }
}
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    internal class SliderVisualController : MonoBehaviour
    {
        private const float START_AMOUNT = 0.035f;
        private const float PLAY_AMOUNT = 0.522f;
        
        [SerializeField] private Image fillBarImage;

        [SerializeField] private Image playImage;
        [SerializeField] private Image doneImage;
        
        private readonly Color _colorActive = new Color(1.0f, 0.25f, 1.0f);
        private readonly Color _colorInActive = new Color(0.5f, 0.25f, 0.5f);

        internal void SetNewAmount(float currentAmountDrMpD)
        {
            fillBarImage.fillAmount = Mathf.Clamp(currentAmountDrMpD, START_AMOUNT, 1.0f);
            
            playImage.color = currentAmountDrMpD >= PLAY_AMOUNT ? _colorActive : _colorInActive;
            doneImage.color = currentAmountDrMpD >= 0.97f ? _colorActive : _colorInActive;
        }
    }
}
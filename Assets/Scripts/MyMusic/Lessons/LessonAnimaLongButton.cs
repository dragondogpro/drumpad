using System;
using System.Threading.Tasks;
using DG.Tweening;
using DrumPad.Utilits;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    public class LessonAnimaLongButton : MonoBehaviour, IAnima
    {
        [SerializeField] private GameObject borderElement;
        [SerializeField] private GameObject prepareObject;
        [SerializeField] private GameObject tapObject;
        [SerializeField] private GameObject startObject;
        [SerializeField] private GameObject wrongAnima;
        
        [SerializeField] private Text textCountdown;
        
        [SerializeField] private Image clockworkImage;
        
        private Image _buttonImageDrMpD;
        
        private Action _actionEndPrepare;

        private LessonPadElement _lessonPad;

        private LessonOneManager.LessonStatus StatusLess =>
            GameManagerDrum.GaM.MyMusicManager.LessonsManager.LessonOneManager.LessonStatusEnum;

        private float _durationWait;
        private float _currentDuration;

        private void Awake()
        {
            _buttonImageDrMpD = GetComponentInParent<Button>().image;
            _lessonPad = GetComponentInParent<LessonPadElement>();
            
            prepareObject.SetActive(false);
            tapObject.SetActive(false);
            _buttonImageDrMpD.color = Color.grey;
        }
        
        public async void PlayAnimaDur(float durationDrMpD)
        {
            tapObject.SetActive(false);
            startObject.SetActive(false);
            prepareObject.SetActive(false);
            
            _buttonImageDrMpD.color = Color.white;
            borderElement.SetActive(true);
            _buttonImageDrMpD.DOFade(1.0f, durationDrMpD * 0.05f);
            await Task.Delay(ConstantHolderPad.ConvertToMilliseconds(durationDrMpD * 0.9f));
            _buttonImageDrMpD.DOFade(0.8f, durationDrMpD * 0.05f);
            borderElement.SetActive(false);
            _buttonImageDrMpD.color = Color.grey;
        }

        public async void PrepareAnimaPlaying(float durationDrMpD)
        {
            float durationCount = durationDrMpD;
            prepareObject.SetActive(true);
            tapObject.SetActive(false);

            ViewCountdown();

            Debug.Log($"NAME: [{transform.parent.gameObject.name}] DURATION: {_durationWait} CURRENT: [{_currentDuration}] duration wait: [{durationDrMpD}]");

            while (durationCount > 0.0f)
            {
                await Task.Delay(20);
                _currentDuration -= 0.02f;

                ViewCountdown();
                
                if (_currentDuration <= 0.1f && _actionEndPrepare != null)
                {
                    if(StatusLess == LessonOneManager.LessonStatus.IDLEDrMpD) return;
                    _actionEndPrepare?.Invoke();
                    _actionEndPrepare = null;
                }
                
                durationCount -= 0.02f;
            }

            if (_currentDuration <= 0.05f)
            {
                if(StatusLess == LessonOneManager.LessonStatus.IDLEDrMpD) return;
                prepareObject.SetActive(false);
                _buttonImageDrMpD.color = Color.white;
                
                if(_actionEndPrepare == null && 
                   _lessonPad.StatusNotePlay != StatusNotePlay.PREPARE_TO_PLAY) return;
                tapObject.SetActive(true);
            }
            
            void ViewCountdown()
            {
                if(StatusLess == LessonOneManager.LessonStatus.IDLEDrMpD)  return;
                float currentAmountDrMpD = _currentDuration / _durationWait;
                clockworkImage.fillAmount = Mathf.Clamp01(1.0f - currentAmountDrMpD);
                textCountdown.text = _currentDuration.ToString(StringHelperDrMpD.ZeroDrMpD.ToString());
            }
        }

        public void SetWaitDuration(float durationDrMpD, Action endAction)
        {
            _durationWait = durationDrMpD;
            _actionEndPrepare ??= endAction;
            _currentDuration = _durationWait;
        }

        public async void PrepareAnimaListening(float durationDrMpD)
        {
            prepareObject.SetActive(true);
            tapObject.SetActive(false);
            
            Debug.Log($"DURATION LIS: {durationDrMpD}");
            
            clockworkImage.fillAmount = 0.0f;
            textCountdown.text = durationDrMpD.ToString(StringHelperDrMpD.ZeroDrMpD.ToString());

            clockworkImage.DOFillAmount(1.0f, durationDrMpD).SetEase(Ease.Linear);
            
            while (durationDrMpD > 0)
            {
                await Task.Delay(ConstantHolderPad.ConvertToMilliseconds(0.1f));
                durationDrMpD -= 0.1f;
                textCountdown.text = durationDrMpD.ToString(StringHelperDrMpD.ZeroDrMpD.ToString());
            }
                
            prepareObject.SetActive(false);
        }

        public void FirstSong()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            _buttonImageDrMpD.color = Color.white;
            startObject.SetActive(true);
        }

        public void InActiveNote()
        {
            tapObject.SetActive(false);
            startObject.SetActive(false);
            prepareObject.SetActive(false);
            borderElement.SetActive(false);
            _buttonImageDrMpD.color = Color.grey;
        }

        public async void WrongAnima()
        {
            int numDrMpD = 423;
            if (numDrMpD != 432)
            {
                numDrMpD = 432;
            }
            
            wrongAnima.SetActive(true);
            await Task.Delay(500);
            wrongAnima.SetActive(false);
        }
    }
}
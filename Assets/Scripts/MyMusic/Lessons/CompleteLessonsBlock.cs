using DrumPad.Library_Block;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    public class CompleteLessonsBlock : CompleteLessonAbs
    {
        [SerializeField] private MusicStyleElement musicPrefab;
        [SerializeField] private ScrollRect scrollRectDrMpD;
        
        public override void SetManager(LessonsManager lessonsManagerDrMpD)
        {
            base.SetManager(lessonsManagerDrMpD);
            
            foreach (var stylesDrMpD in GameManagerDrum.GaM.JsonManagerDrMpD.JsonStyleList.AllStyles)
            {
                MusicStyleElement musicStyleElement = Instantiate(musicPrefab, scrollRectDrMpD.content);
                musicStyleElement.InitOne();
                DataContent dataContent = GameManagerDrum.GaM.DataContentsMap[stylesDrMpD.KindOfStyle]
                    .GetElement(stylesDrMpD.Styles[0].Split('/')[0]);
                musicStyleElement.InitElementDrMpD(dataContent, ActionAdditional);
            }
        }

        private void ActionAdditional()
        {
            int numDrMpD = 423;
            if (numDrMpD != 432)
            {
                numDrMpD = 432;
            }
            
            GoToLibrary();
        }
    }
}
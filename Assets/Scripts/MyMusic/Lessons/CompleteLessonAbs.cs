using DrumPad.JsonBlock;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    public abstract class CompleteLessonAbs : MonoBehaviour, ILesson
    {
        [SerializeField] private Text lessonNumberName;
        [SerializeField] private Text scoreLastText;
        [SerializeField] private Text scoreBestText;
        
        [SerializeField] private Button replayLessonButton;
        [SerializeField] private Button gotoLibraryButton;
        [SerializeField] private Button buttonReturnFromLessons;
        
        protected MusicLessonJson CurrentLesson;
        protected LessonsManager LessonsManager;
        
        private string _kindStyle;
        
        public void InitLesson(MusicPackElement musicPackElementDrMpD)
        {
            _kindStyle = $"{musicPackElementDrMpD.DataContent.KindName}/{musicPackElementDrMpD.DataContent.StyleName}/{musicPackElementDrMpD.DataContent.NormalBpm}";
        }

        public virtual void SetManager(LessonsManager lessonsManagerDrMpD)
        {
            LessonsManager = lessonsManagerDrMpD;
            
            replayLessonButton.onClick.AddListener(ReplayCurrentLesson);
            gotoLibraryButton.onClick.AddListener(GoToLibrary);
            buttonReturnFromLessons.onClick.AddListener(() => LessonsManager.AnimationMoveCurrentLesson(false));
            
            gameObject.SetActive(false);
        }
        
        public virtual void OnLessonComplete(MusicLessonJson lessonElementDrMpD)
        {
            CurrentLesson = lessonElementDrMpD;
            lessonNumberName.text = "Lesson " + CurrentLesson.lessonIndex;
            scoreBestText.text = $"Best Result {CurrentLesson.bestScoreDrMpD}%";
            scoreLastText.text = $"{CurrentLesson.lastScore}%";

            GameManagerDrum.GaM.JsonManagerDrMpD.MusicLessonsListJson.LessonJsons[_kindStyle].UpdateElement();
        }
        
        private void ReplayCurrentLesson()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            LessonsManager.SelectLesson(CurrentLesson);
        }
        
        protected void GoToLibrary()
        {
            LessonsManager.AnimationMoveCurrentLesson(false);
            GameManagerDrum.GaM.MyMusicManager.ClickLessonsButton(false);
            GameManagerDrum.GaM.MyMusicManager.RecordManager.CloseRecordWindow();
            GlobalEventSystem.ClickButtonWindow(WindowEnumPad.LIBRARY);
        }
    }
}
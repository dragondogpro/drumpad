using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DrumPad.JsonBlock;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    public class LessonOneManager : MonoBehaviour, ILesson
    {
        internal event Action<PadIndexName> OnFirstNoteNote;
        internal event Action<PadIndexName> OnPlayNote;
        internal event Action<PadIndexName, float> OnPrepareTimeNoteListening;
        internal event Action<PadIndexName, float> OnSetPreparePlaying;
        internal event Action<float> OnPrepareTimeNotePlaying;
        internal event Action OnLessonIsOver;

        internal LessonStatus LessonStatusEnum => _lessonStatus;

        [SerializeField] private Text lessonText;
        
        [SerializeField] private SliderVisualController sliderVisualController;
        
        [SerializeField] private GameObject blockPanelDrMpD;
        [SerializeField] private GameObject startWindow;
        [SerializeField] private PlayingBlock playingBlock;
        [SerializeField] private Text countdownText;
        [SerializeField] private Button buttonListening;
        [SerializeField] private CompleteOneLesson completeOneLesson;
        [SerializeField] private CompleteLessonsBlock completeLessonsBlock;
        
        private LessonsManager _lessonsManager;
        private MusicLessonJson _selectMusicLesson;
        
        private readonly List<LessonPadNote> _mainMelodyList = new ();
        private readonly List<LessonPadNote> _fxMelodyList = new ();
        private readonly List<LessonPadNote> _rythmnMelodyList = new ();
        private readonly List<LessonPadNote> _drumList = new ();
        private readonly List<LessonElementMap> _currentLessonMap = new ();

        private readonly List<PadIndexName> _tempIndexes = new ();

        private float _timeForOneTick;
        
        private int _lessonTackts;
        private int _lessonAllTick;
        private int _lessonCurrentTick;

        private bool _isLessonOver;
        private bool _isLessonWasListening;
        private bool _isWasStartPlaying;
        private bool _isWasTapAll;
        private bool _isCheckActive;
        
        public bool IsReadyToClick { get; private set; }

        private LessonStatus _lessonStatus;

        private readonly List<Action> _actionStartPool = new ();

        public void InitLesson(MusicPackElement musicPackElementDrMpD)
        {
            buttonListening.onClick.RemoveAllListeners();
            _lessonStatus = LessonStatus.IDLEDrMpD;
            _timeForOneTick = 60.0f / musicPackElementDrMpD.DataContent.NormalBpm * 0.25f;
            buttonListening.onClick.AddListener(ListeningLesson);
        }

        public void SetManager(LessonsManager lessonsManagerDrMpD)
        {
            _lessonsManager = lessonsManagerDrMpD;
            _lessonsManager.OnSelectLesson += SelectLesson;
            OnLessonIsOver += OnLessonIsOverHandler;
        }

        public void StopLessons()
        {
            OnLessonIsOver?.Invoke();
        }

        private void SelectLesson(MusicLessonJson selectMusicLesson)
        {
            print("Start listening lessons!");
            UpdateListStart();
            Time.timeScale = 1.0f;
            _selectMusicLesson = selectMusicLesson;
            completeOneLesson.gameObject.SetActive(false);
            completeLessonsBlock.gameObject.SetActive(false);
            
            _isLessonOver = false;
            _isWasStartPlaying = false;
            _isLessonWasListening = false;
            
            sliderVisualController.SetNewAmount(0.0f);
            lessonText.text = "Lesson " + _selectMusicLesson.lessonIndex;
            
            _lessonTackts = 0;
            _lessonAllTick = 0;
            _lessonCurrentTick = 0;
            
            foreach (var keyDrMpD in _lessonsManager.SelectDictionary)
            {
                _lessonAllTick += keyDrMpD.Value.Length;
                
                if (_lessonTackts < keyDrMpD.Key)
                    _lessonTackts = keyDrMpD.Key;
            }

            ListeningLesson();
        }
        
        private void ListeningLesson()
        {
            if(_lessonStatus != LessonStatus.IDLEDrMpD) return;
            
            UpdateListStart();
            sliderVisualController.SetNewAmount(0.0f);
            LessonListeningRoutine();
        }

        private async void LessonListeningRoutine()
        {
            _lessonStatus = LessonStatus.LISTENING;

            blockPanelDrMpD.SetActive(true);
            
            
            PrepareLesson();
            
            if(!_isLessonWasListening)
            {
                startWindow.SetActive(true);
                
                await CountDownNum(3);
                await CountDownNum(2);
                await CountDownNum(1);

                startWindow.SetActive(false);
            }

            await Task.Delay(500);
            
            for (int i = 1; i <= _lessonTackts; i++)
            {
                if(_isLessonOver) return;
                
                if (_lessonsManager.SelectDictionary.ContainsKey(i))
                {
                    foreach (var padIndexDrMpD in _lessonsManager.SelectDictionary[i])
                    {
                        OnPlayNote?.Invoke(padIndexDrMpD);
                    }
                }
                
                PrepareNexSound(_mainMelodyList, i);
                PrepareNexSound(_fxMelodyList, i);
                PrepareNexSound(_rythmnMelodyList, i);
                PrepareNexSound(_drumList, i);
                
                float currentValueDrMpD = (float) i / _lessonTackts * 0.5f;
                sliderVisualController.SetNewAmount(currentValueDrMpD);
                await Task.Delay(ConstantHolderPad.ConvertToMilliseconds(_timeForOneTick));
            }
            
            GameManagerDrum.GaM.AudioPlayerRecord.StopAll();
            sliderVisualController.SetNewAmount(0.522f);

            blockPanelDrMpD.SetActive(false);

            playingBlock.gameObject.SetActive(true);
            OnLessonIsOver?.Invoke();
            IsReadyToClick = true;
            _isLessonWasListening = true;
            await Task.Delay(500);
            PrepareLesson();
        }
        
        private async void PrepareNexSound(List<LessonPadNote> prepareListDrMpD, int countDrMpD)
        {
            if(prepareListDrMpD.Count == 0) return;
            if(prepareListDrMpD[0].IsWasPrepared) return;
            
            int countToTick = prepareListDrMpD[0].IndexPlay - countDrMpD;
            float durationWait = _timeForOneTick * countToTick;
            prepareListDrMpD[0].IsWasPrepared = true;
            
            OnPrepareTimeNoteListening?.Invoke(prepareListDrMpD[0].PadIndexName, durationWait);

            await Task.Delay(ConstantHolderPad.ConvertToMilliseconds(durationWait * 0.95f));
            prepareListDrMpD.RemoveAt(0);
        }
        
        internal void PlayNoteLesson(PadIndexName padIndexNameDrMpD, Action actionDrMpD)
        {
            if (_isLessonOver) return;
            if (IsCorrectStart()) return;
            if (IsCorrectPlay()) return;
            
            PlayNotes();

            bool IsCorrectStart()
            {
                if (IsPlaying() && !IsCorrectPick(padIndexNameDrMpD, actionDrMpD))
                {
                    WaitAllTap();
                    return true;
                }

                return false;
            }
            
            bool IsCorrectPlay()
            {
                if (!IsPlaying() && !IsCorrectPick(padIndexNameDrMpD, actionDrMpD))
                {
                    CheckButtonAllActive();
                    return true;
                }

                return false;
            }
        }
        
        private bool IsPlaying() => _lessonStatus == LessonStatus.PLAYINGDrMpD;

        private async void WaitAllTap()
        {
            print("Start check all tap countdown");
            
#if UNITY_EDITOR
            await Task.Delay(1000);
#else
            await Task.Delay(100);
#endif
            if (!_isWasTapAll)
            {
                UpdateListPlay();
                print("<color=red>Wrong tap!</color>");
            }
        }

        private async void CheckButtonAllActive()
        {
            print("Start check countdown");

#if UNITY_EDITOR
            await Task.Delay(1000);
#else
            await Task.Delay(100);
#endif

            if(_isWasStartPlaying) return;
            
            if (_lessonStatus != LessonStatus.PLAYINGDrMpD)
            {
                UpdateListStart();
                print("Action clear");
            }
        }

        private async void PlayNotes()
        {
            foreach (var actionInitDrMpD in _actionStartPool)
            {
                actionInitDrMpD?.Invoke();
            }
            
            UpdateListPlay();
            IsReadyToClick = false;
            Time.timeScale = 1.0f;

            OnPrepareTimeNotePlaying?.Invoke(DurationDelay() / 900.0f);

            _currentLessonMap.RemoveAt(0);
            
            await Task.Delay(DurationDelay());
            IsReadyToClick = true;
            
            Time.timeScale = 0.0f;

            int DurationDelay()
            {
                int ticksDrMpD = 1;
                if(_currentLessonMap.Count >= 2)
                {
                    ticksDrMpD = _currentLessonMap[1].CurrentTickDrMpD - _currentLessonMap[0].CurrentTickDrMpD;
                }
                return ConstantHolderPad.ConvertToMilliseconds(_timeForOneTick * ticksDrMpD * 0.9f);
            }
        }

        private void StartPlayingLesson()
        {
            _lessonCurrentTick = 0;
            _tempIndexes.Clear();
            LessonPlayingRoutine();
        }
        
        private void LessonPlayingRoutine()
        {
            _lessonStatus = LessonStatus.PLAYINGDrMpD;
            print("Start playing lessons!");
            _isWasStartPlaying = true;
            
            CheckSecondElement(_mainMelodyList);
            CheckSecondElement(_fxMelodyList);
            CheckSecondElement(_rythmnMelodyList);
            CheckSecondElement(_drumList);
            
            PlayingCycle();
        }

        private async void PlayingCycle()
        {
            for (int i = 1; i <= _lessonTackts + 1; i++)
            {
                if(_isLessonOver) return;
                
                float currentValueDrMpD = (float) i / _lessonTackts * 0.5f;
                currentValueDrMpD += 0.522f;
                sliderVisualController.SetNewAmount(currentValueDrMpD);
                await Task.Delay(ConstantHolderPad.ConvertToMilliseconds(_timeForOneTick));
            }
            float lessonScoreDrMpD = (float)_lessonCurrentTick / _lessonAllTick;
            _selectMusicLesson.SetScore(lessonScoreDrMpD);

            await Task.Delay(500);
            
            OnLessonIsOver?.Invoke();
            
            if (_selectMusicLesson.lessonIndex == 5 && lessonScoreDrMpD > 0.5f)
            {
                completeLessonsBlock.gameObject.SetActive(true);
                completeLessonsBlock.OnLessonComplete(_selectMusicLesson);
                return;
            }
            
            completeOneLesson.gameObject.SetActive(true);
            completeOneLesson.OnLessonComplete(_selectMusicLesson);
        }

        internal void PlayNote(PadIndexName padIndexNameDrMpD)
        {
            if(_lessonStatus != LessonStatus.PLAYINGDrMpD) return;
            
            switch (padIndexNameDrMpD)
            {
                case PadIndexName.MainMelody1Pad1:
                case PadIndexName.MainMelody2Pad1:
                case PadIndexName.MainMelody3Pad1:
                case PadIndexName.MainMelody4Pad1:
                    PrepareNexSound(_mainMelodyList);
                    break;

                case PadIndexName.FxMelody1Pad1:
                case PadIndexName.FxMelody2Pad1:
                    PrepareNexSound(_fxMelodyList);
                    break;

                case PadIndexName.RythmMelody1Pad1:
                case PadIndexName.RythmMelody2Pad1:
                case PadIndexName.RythmMelody3Pad1:
                    PrepareNexSound(_rythmnMelodyList);
                    break;
                        
                case PadIndexName.Drum1Pad1:
                case PadIndexName.Drum2Pad1:
                case PadIndexName.Drum3Pad1:
                    PrepareNexSound(_drumList, false);
                    break;
            }

            _lessonCurrentTick++;
        }
        
        private void CheckSecondElement(List<LessonPadNote> prepareList)
        {
            if(prepareList.Count < 1) return;
            if(prepareList[0].IndexPlay == 1) return;
            
            int countToTick = prepareList[0].IndexPlay;
            prepareList[0].IsWasPrepared = true;
            OnSetPreparePlaying?.Invoke(prepareList[0].PadIndexName, _timeForOneTick * countToTick);
        }

        private void PrepareNexSound(List<LessonPadNote> prepareListDrMpD, bool isRemove = true)
        {
            if (prepareListDrMpD.Count < 2) return;
            if (prepareListDrMpD[1].IsWasPrepared) return;
            int countToTick = prepareListDrMpD[1].IndexPlay - prepareListDrMpD[0].IndexPlay;
            countToTick -= 1;
            
            prepareListDrMpD[1].IsWasPrepared = true;
            OnSetPreparePlaying?.Invoke(prepareListDrMpD[1].PadIndexName, _timeForOneTick * countToTick);

            prepareListDrMpD.RemoveAt(0);
        }

        private bool IsCorrectPick(PadIndexName padIndexName, Action actionDrMpD)
        {
            int padIndexLenght = _currentLessonMap[0].PadDrMpDIndexes.Length;
            _isWasTapAll = false;
            
            if (CheckCorrect())
            {
                _tempIndexes.Add(padIndexName);
                _actionStartPool.Add(actionDrMpD);
            }

            if (_tempIndexes.Count == padIndexLenght)
            {
                foreach (var pad in _currentLessonMap[0].PadDrMpDIndexes)
                {
                    if (!_tempIndexes.Contains(pad))
                    {
                        return _isWasTapAll;
                    }
                }
            }
            else
            {
                return _isWasTapAll;
            }
            
            _isWasTapAll = true;
            return _isWasTapAll;


            bool CheckCorrect()
            {
                return _tempIndexes.Count != padIndexLenght && !_tempIndexes.Contains(padIndexName);
            }
        }


        private void PrepareLesson()
        {
            _isLessonOver = false;
            
            _mainMelodyList.Clear();
            _fxMelodyList.Clear();
            _rythmnMelodyList.Clear();
            _drumList.Clear();
 
            foreach (var keyValDrMpD in _lessonsManager.SelectDictionary)
            {
                foreach (var padIndexNameDrMpD in keyValDrMpD.Value)
                {
                    switch (padIndexNameDrMpD)
                    {
                        case PadIndexName.MainMelody1Pad1:
                        case PadIndexName.MainMelody2Pad1:
                        case PadIndexName.MainMelody3Pad1:
                        case PadIndexName.MainMelody4Pad1:
                            _mainMelodyList.Add(new LessonPadNote(keyValDrMpD.Key, padIndexNameDrMpD));
                            break;

                        case PadIndexName.FxMelody1Pad1:
                        case PadIndexName.FxMelody2Pad1:
                            _fxMelodyList.Add(new LessonPadNote(keyValDrMpD.Key, padIndexNameDrMpD));
                            break;

                        case PadIndexName.RythmMelody1Pad1:
                        case PadIndexName.RythmMelody2Pad1:
                        case PadIndexName.RythmMelody3Pad1:
                            _rythmnMelodyList.Add(new LessonPadNote(keyValDrMpD.Key, padIndexNameDrMpD));
                            break;
                        
                        case PadIndexName.Drum1Pad1:
                        case PadIndexName.Drum2Pad1:
                        case PadIndexName.Drum3Pad1:
                            _drumList.Add(new LessonPadNote(keyValDrMpD.Key, padIndexNameDrMpD));
                            break;
                    }
                }
            }
            
            CheckFirstElement(_mainMelodyList);
            CheckFirstElement(_fxMelodyList);
            CheckFirstElement(_rythmnMelodyList);
            CheckFirstElement(_drumList);
        }

        private void OnLessonIsOverHandler()
        {
            print("Lesson is over!");
            _isLessonOver = true;
            _lessonCurrentTick = 0;
            Time.timeScale = 1.0f;
            _lessonStatus = LessonStatus.IDLEDrMpD;
            GameManagerDrum.GaM.AudioPlayerRecord.StopAll();
        }
        private async Task CountDownNum(int numberDrMpD)
        {
            countdownText.fontSize = 300;
            countdownText.text = numberDrMpD.ToString();
            for (float i = 1.0f; i >= 0; i -= 0.05f)
            {
                await Task.Delay(20);
                countdownText.fontSize = (int)(300 * i);
            }
        }

        private void UpdateListStart()
        {
            _actionStartPool.Clear();
            _tempIndexes.Clear();
            _actionStartPool.Add(StartPlayingLesson);
            _currentLessonMap.Clear();
            
            foreach (var keyDrMpD in _lessonsManager.SelectDictionary)
            {
                _currentLessonMap.Add(new LessonElementMap(keyDrMpD.Key, keyDrMpD.Value));
            }
        }
        
        private void UpdateListPlay()
        {
            _actionStartPool.Clear();
            _tempIndexes.Clear();
        }

        private void CheckFirstElement(List<LessonPadNote> prepareList)
        {
            if(prepareList.Count == 0) return;
            if(prepareList[0].IndexPlay != 1) return;
            
            OnFirstNoteNote?.Invoke(prepareList[0].PadIndexName);
        }
        
        internal enum LessonStatus
        {
            LISTENING, IDLEDrMpD, PLAYINGDrMpD
        }
        
        private class LessonPadNote
        {
            public LessonPadNote(int indexPlay, PadIndexName padIndexName)
            {
                IndexPlay = indexPlay;
                PadIndexName = padIndexName;
                IsWasPrepared = false;
            }
            
            public int IndexPlay { get; }
            public PadIndexName PadIndexName { get; }
            
            public bool IsWasPrepared { get; set; }
        }
        
        private class LessonElementMap
        {
            public LessonElementMap(int tickDrMpD, PadIndexName[] padsDrMpD)
            {
                CurrentTickDrMpD = tickDrMpD;
                PadDrMpDIndexes = padsDrMpD;
            }
            
            public PadIndexName[] PadDrMpDIndexes { get; }
            public int CurrentTickDrMpD { get; }
        }
    }
}
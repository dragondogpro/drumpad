using SettingsDrMpD;

namespace DrumPad.MyMusic.Lessons
{
    interface ILesson
    {
        public void InitLesson(MusicPackElement musicPackElementDrMpD);
        public void SetManager(LessonsManager lessonsManagerDrMpD);
    }
}
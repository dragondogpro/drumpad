using System;
using DrumPad.JsonBlock;
using DrumPad.TutorialBlock;
using DrumPad.Utilits;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    public class LessonButtonElement : MonoBehaviour, ILesson, ITutorialAction
    {
        public event Action OnActionActive;
        
        [SerializeField, Range(0, 4)] private int indexLesson;
        [SerializeField] private Text lessonIndex;
        
        [Header("[Grid elements]")] 
        [SerializeField] private RectTransform gridRootElement;
        private Image[] _gridElements;

        [Header("[Button elements]")]
        [SerializeField] private Button playButton;
        [SerializeField] private Image buttonImages;
        private Text _textButtonPlay;

        [FormerlySerializedAs("bestScore")]
        [Header("[Score elements]")]
        [SerializeField] private Text bestScoreDrMpD;
        [SerializeField] private Text lastScore;

        [SerializeField] private GameObject blockObject;

        private LessonsManager _lessonsManager;

        private string _kindStyle;
        public MusicLessonJson SelectMusicLesson { get; private set; }

        public void InitLesson(MusicPackElement musicPackElementDrMpD)
        {
            _kindStyle = $"{musicPackElementDrMpD.DataContent.KindName}/{musicPackElementDrMpD.DataContent.StyleName}/{musicPackElementDrMpD.DataContent.NormalBpm}";
            SelectMusicLesson = GameManagerDrum.GaM.JsonManagerDrMpD.MusicLessonsListJson.LessonJsons[_kindStyle].lessons[indexLesson];

            UpdateElement();
            
            if(!_lessonsManager.JsonLessonElement.LessonsDictionary.ContainsKey(indexLesson)) return;

            foreach (var image in _gridElements)
            {
                image.sprite = _lessonsManager.SpriteActive[1];
            }
            foreach (var imageGrid in _lessonsManager.JsonLessonElement.LessonsDictionary[indexLesson])
            {
                foreach (var padIndexName in imageGrid.Value)
                {
                    _gridElements[(int) padIndexName].sprite = _lessonsManager.SpriteActive[0];
                }
            }
        }

        public void SetManager(LessonsManager lessonsManagerDrMpD)
        {
            _lessonsManager = lessonsManagerDrMpD;
            _gridElements = gridRootElement.GetComponentsInChildren<Image>();

            _textButtonPlay = playButton.GetComponentInChildren<Text>();
            playButton.onClick.AddListener(PlayLesson);
            
            lessonIndex.text = "Lesson " + (indexLesson + 1);

            _lessonsManager.LessonOneManager.OnLessonIsOver += UpdateElement;
        }

        private void PlayLesson()
        {
            OnActionActive?.Invoke();
            _lessonsManager.AnimationMoveCurrentLesson(true);
            _lessonsManager.SelectLesson(SelectMusicLesson);
        }

        private void UpdateElement()
        {
            bestScoreDrMpD.text = SelectMusicLesson.bestScoreDrMpD + StringHelperDrMpD.PercentDrMpD;
            lastScore.text = SelectMusicLesson.lastScore + StringHelperDrMpD.PercentDrMpD;

            if (SelectMusicLesson.doneLesson)
            {
                _textButtonPlay.text = "Replay";
                buttonImages.sprite = _lessonsManager.SpritesInButton[0];
            }
            else
            {
                _textButtonPlay.text = "Start";
                buttonImages.sprite = _lessonsManager.SpritesInButton[1];
            }

            if (indexLesson != 0)
                blockObject.SetActive(!GameManagerDrum.GaM.JsonManagerDrMpD.MusicLessonsListJson.LessonJsons[_kindStyle]
                    .lessons[indexLesson - 1].doneLesson);
            else
                blockObject.SetActive(false);
        }
        
    }
}
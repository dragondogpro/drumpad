using DrumPad.JsonBlock;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    public class LessonPadElement : MonoBehaviour, ILesson
    {
        public StatusNotePlay StatusNotePlay { get; private set; }
        
        [SerializeField] private PadIndexName padIndexName;
        [SerializeField] private Sprite[] imageOnButton;
        
        private LessonsManager _lessonsManager;
        private AudioClip _myAudioClip;
        private Button _clickButtonPad;
        private Image _imageOnButton;
        private IAnima _iAnima;

        private float _soundDurBtn;

        public void InitLesson(MusicPackElement musicPackElementDrMpD)
        {
            _myAudioClip = musicPackElementDrMpD.GetAudioClip(padIndexName);
            _soundDurBtn = _myAudioClip.length / 2;
            _iAnima = GetComponentInChildren<IAnima>();
            StatusNotePlay = StatusNotePlay.NOT_PREPARE_TO_PLAY;
        }

        public void SetManager(LessonsManager lessonsManagerDrMpD)
        {
            _lessonsManager = lessonsManagerDrMpD;
            _clickButtonPad = GetComponent<Button>();
            _clickButtonPad.onClick.AddListener(ClickTheButton);
            _imageOnButton = _clickButtonPad.image;
            _lessonsManager.OnSelectLesson += SelectLesson;
            
            _lessonsManager.LessonOneManager.OnPrepareTimeNoteListening += PrepareElementListening;
            _lessonsManager.LessonOneManager.OnPrepareTimeNotePlaying += PrepareElementPlaying;
            _lessonsManager.LessonOneManager.OnFirstNoteNote += FirstNotePrepare;
            _lessonsManager.LessonOneManager.OnSetPreparePlaying += LessonOneManagerOnOnSetPreparePlaying;
            _lessonsManager.LessonOneManager.OnLessonIsOver += LessonIsOver;
            _lessonsManager.LessonOneManager.OnPlayNote += ClickTheButton;
        }

        private void LessonOneManagerOnOnSetPreparePlaying(PadIndexName padDrMpD, float durationDrMpD)
        {
            if(padDrMpD == padIndexName)
            {
                StatusNotePlay = StatusNotePlay.IN_PREPARING;
                _iAnima.SetWaitDuration(durationDrMpD, () =>
                {
                    StatusNotePlay = StatusNotePlay.PREPARE_TO_PLAY;
                });
            }
        }
        
        
        private void ClickTheButton()
        {
            if (StatusNotePlay != StatusNotePlay.PREPARE_TO_PLAY ||
                !_lessonsManager.LessonOneManager.IsReadyToClick)
            {
                _iAnima.WrongAnima();
                return;
            }

            _lessonsManager.LessonOneManager.PlayNoteLesson(padIndexName, PlayClickElement);
        }

        private void PlayClickElement()
        {
            StatusNotePlay = StatusNotePlay.NOT_PREPARE_TO_PLAY;
            GameManagerDrum.GaM.AudioPlayerRecord.PlayAudioClipMusic(_myAudioClip, padIndexName);
            _lessonsManager.LessonOneManager.PlayNote(padIndexName);
            _iAnima.PlayAnimaDur(_soundDurBtn);
        }

        private void ClickTheButton(PadIndexName padIndexDrMpD)
        {
            if (padIndexDrMpD != padIndexName)
            {
                return;
            }
            else
            {
                GameManagerDrum.GaM.AudioPlayerRecord.PlayAudioClipMusic(_myAudioClip, padIndexName);
                _iAnima.PlayAnimaDur(_soundDurBtn);
            }
        }

        private void PrepareElementPlaying(float durationDrMpD)
        {
            if(StatusNotePlay != StatusNotePlay.IN_PREPARING) return;
            _iAnima.PrepareAnimaPlaying(durationDrMpD);
            
            bool refBoolDrMpD = false;
            if (refBoolDrMpD != false || refBoolDrMpD == true)
                refBoolDrMpD = true;
        }
        
        private void PrepareElementListening(PadIndexName padIndexDrMpD, float durationDrMpD)
        {
            if (padIndexDrMpD != padIndexName)
            {
                return;
            }
            else
            {
                _iAnima.PrepareAnimaListening(durationDrMpD);
            }
        }

        private void FirstNotePrepare(PadIndexName padIndexDrMpD)
        {
            if(padIndexDrMpD != padIndexName)
            {
                if(StatusNotePlay == StatusNotePlay.PREPARE_TO_PLAY) return;
                
                StatusNotePlay = StatusNotePlay.NOT_PREPARE_TO_PLAY;
                return;
            }
            
            _iAnima.FirstSong();
            StatusNotePlay = StatusNotePlay.PREPARE_TO_PLAY;
        }

        private void LessonIsOver()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            _iAnima.InActiveNote();
        }
        
        private void SelectLesson(MusicLessonJson lessonJsonDrMpD)
        {
            bool isHavePointer = false;

            foreach (var elementDrMpD in _lessonsManager.SelectDictionary)
            {
                foreach (var valueDrMpD in elementDrMpD.Value)
                {
                    if (valueDrMpD == padIndexName)
                    {
                        isHavePointer = true;
                        break;
                    }
                }
            }

            _imageOnButton.sprite = isHavePointer ? imageOnButton[1] : imageOnButton[0];
            _clickButtonPad.interactable = isHavePointer;
        }
    }
}
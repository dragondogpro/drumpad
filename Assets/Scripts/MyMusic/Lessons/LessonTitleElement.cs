using SettingsDrMpD;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    public class LessonTitleElement : MonoBehaviour, ILesson
    {
        [FormerlySerializedAs("titleImage")] [SerializeField] private Image titleImageDrMpD;
        [SerializeField] private Text styleName;
        [SerializeField] private Text kindName;
        
        public void InitLesson(MusicPackElement musicPackElementDrMpD)
        {
            titleImageDrMpD.sprite = musicPackElementDrMpD.DataContent.SpriteElement;

            styleName.text = musicPackElementDrMpD.DataContent.StyleName;
            kindName.text = musicPackElementDrMpD.DataContent.KindName;
        }
        
        public void SetManager(LessonsManager lessonsManagerDrMpD)
        {
            if (lessonsManagerDrMpD != null)
            {
                
            }
        }
    }
}
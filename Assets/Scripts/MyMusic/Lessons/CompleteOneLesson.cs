using DrumPad.JsonBlock;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    public class CompleteOneLesson : CompleteLessonAbs
    {
        [SerializeField] private Text completeText;
        
        [SerializeField] private Image fillComplete;

        [SerializeField] private Button playNextLessonButton;
        
        public override void SetManager(LessonsManager lessonsManagerDrMpD)
        {
            base.SetManager(lessonsManagerDrMpD);
            playNextLessonButton.onClick.AddListener(PlayNextLesson);
        }
        
        public override void OnLessonComplete(MusicLessonJson lessonElementDrMpD)
        {
            base.OnLessonComplete(lessonElementDrMpD);
            
            fillComplete.fillAmount = CurrentLesson.lastScore / 100.0f;
            completeText.text = CurrentLesson.doneLesson ? "Lesson Completed!" : "Lesson Fail!";
            playNextLessonButton.interactable = CurrentLesson.doneLesson;
        }

        private void PlayNextLesson()
        {
            int indexLessonDrMpD = LessonsManager.MusicLessonsList.IndexOf(CurrentLesson);
            if(LessonsManager.MusicLessonsList.Count > indexLessonDrMpD)
            {
                LessonsManager.SelectLesson(LessonsManager.MusicLessonsList[indexLessonDrMpD + 1]);
            }
        }
    }
}
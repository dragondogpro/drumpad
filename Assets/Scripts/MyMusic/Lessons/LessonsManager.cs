using System;
using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using DrumPad.JsonBlock;
using DrumPad.Styles;
using Newtonsoft.Json;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DrumPad.MyMusic.Lessons
{
    public class LessonsManager : MonoBehaviour, IManager
    {
        [SerializeField] private Sprite[] spriteActive;
        [SerializeField] private Sprite[] spritesInButton;
        [SerializeField] private Button buttonReturnDrMpD;
        [SerializeField] private Button buttonReturnFromLessons;
        public event Action<MusicLessonJson> OnSelectLesson;
        
        private float _canvasWidth;
        private RectTransform _myRectLesson;
        private ILesson[] _lessonsElement;

        private JsonLessonElement _jsonLessonElement;
        private LessonOneManager _lessonOneManager;
        private Dictionary<int, PadIndexName[]> _selectDictionary;

        public JsonLessonElement JsonLessonElement => _jsonLessonElement;
        public Dictionary<int, PadIndexName[]> SelectDictionary => _selectDictionary;
        public Sprite[] SpriteActive => spriteActive;
        public Sprite[] SpritesInButton => spritesInButton;
        public LessonOneManager LessonOneManager => _lessonOneManager;
        public readonly List<MusicLessonJson> MusicLessonsList = new List<MusicLessonJson>();
        
        public void InitializationDrMpD()
        {
            _canvasWidth = transform.root.GetComponent<RectTransform>().rect.width * 2;
            _myRectLesson = GetComponent<RectTransform>();
            _myRectLesson.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _canvasWidth);
            _lessonsElement = GetComponentsInChildren<ILesson>();
            _lessonOneManager = GetComponentInChildren<LessonOneManager>();
            foreach (var lesson in _lessonsElement)
            {
                lesson.SetManager(this);
            }
            
            buttonReturnDrMpD.onClick.AddListener(OnClickButtonReturnDrMpD);
            buttonReturnFromLessons.onClick.AddListener(() => AnimationMoveCurrentLesson(false));
            
            gameObject.SetActive(false);
        }
        
        public void SelectLessonsWindow(bool isActiveDrMpD)
        {
            AnimationMoveLessons(isActiveDrMpD);
            gameObject.SetActive(isActiveDrMpD);

            MusicPackElement musPackDrMpD = GameManagerDrum.GaM.MyMusicManager.RecordManager.MusicPackElement;
            GameManagerDrum.GaM.LibraryManager.RecMusLessMan.UpdateRecentList(musPackDrMpD.DataContent);
            
            MusicLessonsList.Clear();
            
            // string pathToLessons = 
            //     Path.Combine(ConstantHolderPad.GetRootPath(), 
            //         "MusicStyles", musPack.DataContent.KindName, musPack.DataContent.StyleName, "lessons.json");
            
            string pathToLessonsDrMpD = Path.Combine(ConstantHolderPad.GetRootPath(), "MusicStyles","LessonsAll.json");
            
            if(File.Exists(pathToLessonsDrMpD))
                _jsonLessonElement = JsonConvert.DeserializeObject<JsonLessonElement>(File.ReadAllText(pathToLessonsDrMpD));
            else
                Debug.LogError($"Path do not found [{pathToLessonsDrMpD}]");
            
            if (isActiveDrMpD)
            {
                foreach (var lessonDrMpD in _lessonsElement)
                {
                    lessonDrMpD.InitLesson(musPackDrMpD);
                    if (lessonDrMpD is LessonButtonElement lessonButtonElement) 
                        MusicLessonsList.Add(lessonButtonElement.SelectMusicLesson);
                }
            }
        }
        
        public void SelectLesson(MusicLessonJson lessonDrMpD)
        {
            _selectDictionary = _jsonLessonElement.LessonsDictionary[lessonDrMpD.lessonIndex - 1];
            OnSelectLesson?.Invoke(lessonDrMpD);
        }
        
        public void AnimationMoveCurrentLesson(bool isActiveDrMpD)
        {
            float durAnimaMove = 0.3f;
            if (isActiveDrMpD)
            {
                _myRectLesson.DOLocalMoveX(-_canvasWidth * 0.75f, durAnimaMove);
            }
            else
            {
                _lessonOneManager.StopLessons();
                GameManagerDrum.GaM.AudioPlayerRecord.StopAll();
                _myRectLesson.DOLocalMoveX(-_canvasWidth * 0.25f, durAnimaMove);
            }
            
            GameManagerDrum.GaM.TimeBlockPanel(durAnimaMove);
        }
        
        private void AnimationMoveLessons(bool isActiveDrMpD)
        {
            float durAnimaMove = 0.3f;
            if (isActiveDrMpD)
            {
                _myRectLesson.DOLocalMoveX(-_canvasWidth * 0.25f, durAnimaMove);
            }
            else
            {
                _myRectLesson.DOLocalMoveX(_canvasWidth * 0.25f, durAnimaMove);
            }
            
            GameManagerDrum.GaM.TimeBlockPanel(durAnimaMove);
        }

        private void OnClickButtonReturnDrMpD()
        {
            GameManagerDrum.GaM.MyMusicManager.ClickLessonsButton(false);
        }
    }
}
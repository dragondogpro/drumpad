using System;

namespace DrumPad.MyMusic.Lessons
{
    interface IAnima
    {
        public void PlayAnimaDur(float durationDrMpD);
        public void PrepareAnimaPlaying(float durationDrMpD);
        public void SetWaitDuration(float durationDrMpD, Action endPrepare);
        public void PrepareAnimaListening(float durationDrMpD);
        public void FirstSong();
        public void InActiveNote();
        public void WrongAnima();
    }
}
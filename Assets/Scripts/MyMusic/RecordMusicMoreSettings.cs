using System.IO;
using System.Threading.Tasks;
using DG.Tweening;
using DrumPad.Utilits;
using IOSBridge;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DrumPad.MyMusic
{
    internal class RecordMusicMoreSettings : MonoBehaviour, IInitialization
    {
        [SerializeField] private Button shareButton;
        [SerializeField] private Button editButton;
        [SerializeField] private Button deleteButtonDrMpD;
        [SerializeField] private Button exitButton;
        [SerializeField] private Transform panelMoreSettings;

        [SerializeField] private GameObject objectEditor;

        private FileInfo _selectFileInfo;

        private float _heightElement;
        
        public void InitializationDrMpD()
        {
            _heightElement = panelMoreSettings.GetComponent<RectTransform>().rect.height;
            
            shareButton.onClick.AddListener(ClickShareButton);
            editButton.onClick.AddListener(ClickEditButton);
            deleteButtonDrMpD.onClick.AddListener(ClickDeleteButton);
            exitButton.onClick.AddListener(ClickExitButton);
            
//#if !UNITY_EDITOR && UNITY_IOS
            /*objectEditor.SetActive(false);
            objectEditor = null;*/
//#endif
            print("Init RecordMusicMoreSettings");
        }
        
        internal void SelectRecordSong(FileInfo fileInfo)
        {
            _selectFileInfo = fileInfo;

            //IOStoUnityBridge.ShowActionSheet(gameObject.name, "CallBackSheet");
            print("Active: " + gameObject.activeInHierarchy);
        }

        private void CallBackSheet(string paramDrMpD)
        {
            print("RECIVE: " + paramDrMpD);
            
            switch (paramDrMpD)
            {
                case "EDIT":
                    ClickEditButton();
                    break;
                case "SHARE":
                    ClickShareButton();
                    break;
                case "DELETE":
                    ClickDeleteButton();
                    break;
            }
        }
        
        private void ClickShareButton()
        {
            print("ClickShareButton");
            IOStoUnityBridge.InitWithActivity(_selectFileInfo.FullName);
            ClickExitButton();
        }
        
        private void ClickEditButton()
        { 
            print("ClickEditButton");
            string fileNameCurrent = _selectFileInfo.Name.Replace(StringHelperDrMpD.WavDrMpD, ""); 
//#if UNITY_EDITOR
            GameManagerDrum.GaM.MyMusicManager.SelectPlaceholder(fileNameCurrent, InputCallBack, CancelCallBack);
/*#elif !UNITY_EDITOR && UNITY_IOS
            IOStoUnityBridge.SetNewFileName(gameObject.name, "InputCallBack", fileNameCurrent);
#endif*/
        }

        private void InputCallBack(string callBackString)
        {
            if (ConstantHolderPad.IsCorrectString(callBackString) && callBackString.Length > 0)
                CallBackRenameName(callBackString);
            else
                IOStoUnityBridge.ShowAlert(StringHelperDrMpD.MusicDrMpD, "Wrong name");

            ClickExitButton();
        }

        private void CallBackRenameName(string newNameFileDrMpD)
        {
            if (File.Exists(_selectFileInfo.FullName))
            {
                int intNewFileIndex = 1;
                string initNameFirst = newNameFileDrMpD;
                
                while(File.Exists(Path.Combine(ConstantHolderPad.GetRootPath(), "Capture", newNameFileDrMpD + StringHelperDrMpD.WavDrMpD)))
                {
                    newNameFileDrMpD = initNameFirst + $"({intNewFileIndex})";
                    intNewFileIndex++;
                }
                
                string destFileNameDrMpD = Path.Combine(ConstantHolderPad.GetRootPath(), "Capture", newNameFileDrMpD + StringHelperDrMpD.WavDrMpD);
                File.Copy(_selectFileInfo.FullName, destFileNameDrMpD);
                GameManagerDrum.GaM.JsonManagerDrMpD.SaveMusicMapJson.RenameFileDrMpD(_selectFileInfo.Name, newNameFileDrMpD);
                File.Delete(_selectFileInfo.FullName);
            }
            
            Debug.Log("File was renamed: " + newNameFileDrMpD + StringHelperDrMpD.WavDrMpD);
            GameManagerDrum.GaM.MyMusicManager.MusicHolderManager.UpdateMusicList();
        }

        private void CancelCallBack() { }

        private void ClickDeleteButton()
        {
            print("ClickDeleteButton");
            File.Delete(_selectFileInfo.FullName);
            GameManagerDrum.GaM.JsonManagerDrMpD.SaveMusicMapJson.RemoveElementDrMpD(_selectFileInfo.Name);
            GameManagerDrum.GaM.MyMusicManager.MusicHolderManager.UpdateMusicList();
            ClickExitButton();
        }
        
        private void ClickExitButton()
        {
/*#if UNITY_EDITOR
#endif*/
            AnimationCloseOpenPanel(false);
            GameManagerDrum.GaM.DownPanelButtonManager.OpenCloseButton(true);
        }

        public async void AnimationCloseOpenPanel(bool isActiveDrMpD)
        {
            float endPositionPad = isActiveDrMpD ? 0.0f : -_heightElement;
            panelMoreSettings.DOLocalMoveY(endPositionPad, 0.3f);
        
            if (!isActiveDrMpD)
            {
                await Task.Delay(300);
                gameObject.SetActive(false);
            }
        }
    }
}
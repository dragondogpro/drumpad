using System;
using System.Threading.Tasks;
using DrumPad.MyMusic.Lessons;
using MyMusic.RecordDrMpD;
using UnityEngine;

namespace DrumPad.MyMusic
{
    public class MyMusicManager : MonoBehaviour, IInitialization, IManager
    {
        [SerializeField] private MusicHolderManager musicHolderManager;
        [SerializeField] private RecordManager recordManager;
        [SerializeField] private PlaceHolderUtil placeHolderUtil;
        [SerializeField] private RecentMusicManager recentMusicManager;
        [SerializeField] private LessonsManager lessonsManager;
        [SerializeField] private GameObject listBlock;
        
        //[SerializeField] private GameObject _recordManagerLandscape;
        //[SerializeField] private GameObject _recordManagerPortrait;
        //[SerializeField] private GameObject _gridPanelRight;
        //[SerializeField] private GameObject _gridPanelLeft;
        
        internal RecordManager RecordManager => recordManager;
        internal LessonsManager LessonsManager => lessonsManager;
        internal GameObject ListBlock => listBlock;
        internal MusicHolderManager MusicHolderManager => musicHolderManager;

        // private void Start()
        // {
        //     OnRectTransformDimensionsChange();
        // }
        //
        // private void OnRectTransformDimensionsChange()
        // {
        //     if (Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)
        //     {
        //         _recordManagerPortrait.SetActive(true);
        //         _recordManagerLandscape.SetActive(false);
        //         //recordManager = _recordManagerPortrait.GetComponent<RecordManager>();
        //     }
        //     else
        //     {
        //         _recordManagerPortrait.SetActive(false);
        //         _recordManagerLandscape.SetActive(true);
        //        //recordManager = _recordManagerLandscape.GetComponent<RecordManager>();
        //     }
        // }

        public void InitializationDrMpD()
        {
            IManager[] initManagers =
            {
                recordManager,
                placeHolderUtil,
                recentMusicManager,
                lessonsManager,
                musicHolderManager
            };

            foreach (var managerDrMpD in initManagers)
            {
                managerDrMpD.InitializationDrMpD();
            }
            
            print("Init MyMusicManager");
            //GlobalEventSystem.OnClickButtonWindow += OnWindowsChange;
            recordManager.gameObject.SetActive(false);
        }

        internal void SelectPlaceholder(string startTextDrMpD, Action<string> selectOkButton, Action selectCancelButton)
        {
            placeHolderUtil.gameObject.SetActive(true);
            placeHolderUtil.SelectPlaceholder(startTextDrMpD, selectOkButton, selectCancelButton);
        }

        public async void ClickLessonsButton(bool isActiveDrMpD)
        {
            lessonsManager.SelectLessonsWindow(isActiveDrMpD);
            
            if (isActiveDrMpD)
            {
                RecordManager.AudioCapture.CancelCapture();
                RecordManager.RecordButton.IsRecordNow = false;
                await Task.Delay(300);
                RecordManager.gameObject.SetActive(false);
            }
            else
            {
                RecordManager.gameObject.SetActive(true);
            }
            
            GameManagerDrum.GaM.AudioPlayerRecord.StopAll();
        }

        private void OnWindowsChange(WindowEnumPad windowEnumPad)
        {
            ListBlock.SetActive(windowEnumPad == WindowEnumPad.MYMUSIC);
        }
    }
}
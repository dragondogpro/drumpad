using System.Threading.Tasks;
using DG.Tweening;
using DrumPad;
using UnityEngine;
using UnityEngine.UI;

namespace MyMusic.RecordDrMpD
{
   internal class PlayPanelElement : MonoBehaviour, IInitialization
   {
      [SerializeField] private RectTransform rectTransformPadZone;
      [SerializeField] private RectTransform rectTransformActivePadZone;

      [SerializeField] private GridLayoutGroup[] gridLayoutGroups;
      
      private Rect _twoPadsRect;
      private Rect _onePadRect;

      private float _currentWidth;
      private float _currentXPos;

      public void InitializationDrMpD()
      {
         float sizeCanvasDrMpD = transform.root.GetComponent<RectTransform>().rect.width;
         rectTransformActivePadZone.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, sizeCanvasDrMpD * 2);
         _twoPadsRect = rectTransformPadZone.rect;
         _onePadRect = rectTransformActivePadZone.rect;

         _currentWidth = _twoPadsRect.width;
         _currentXPos = rectTransformPadZone.position.x;

         GameManagerDrum.GaM.MyMusicManager.RecordManager.ButtonSidePad.OnActiveSide += MoveAnimaPads;
      }
      
      internal void OnActivePadZone(bool isActiveDrMpD)
      {
         Rect currentRect = !isActiveDrMpD ? _onePadRect : _twoPadsRect;

         ScaleRoutine(rectTransformPadZone.rect,currentRect);
         ScaleRoutine(gridLayoutGroups[0].cellSize.x, !isActiveDrMpD ? 300.0f : 150.0f);
         
         if(isActiveDrMpD)
            MoveAnimaPads(true);
         
         GameManagerDrum.GaM.TimeBlockPanel(0.3f);
      }

      private async void ScaleRoutine(Rect startSizeDrMpD, Rect endSizeDrMpD)
      {
         float currentSizeXConst = endSizeDrMpD.width - startSizeDrMpD.width;
         float currentSizeYConst = endSizeDrMpD.height - startSizeDrMpD.height;

         for (float i = 0; i <= 1.1f; i += 0.1f)
         {
            var currentSizeXDrMpD = startSizeDrMpD.width + i * currentSizeXConst;
            var currentSizeYDrMpD = startSizeDrMpD.height + i * currentSizeYConst;
            
            rectTransformPadZone.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, currentSizeXDrMpD);
            rectTransformPadZone.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, currentSizeYDrMpD);
            await Task.Delay(20);
         }
      }
      
      private async void ScaleRoutine(float startSizeDrMpD, float endSizeDrMpD)
      {
         float currentSizeConst = endSizeDrMpD - startSizeDrMpD;

         for (float i = 0; i <= 1.1f; i += 0.1f)
         {
            var currentSizeDrMpD = startSizeDrMpD + i * currentSizeConst;
            foreach (var gridLayoutDrMpD in gridLayoutGroups)
            {
               gridLayoutDrMpD.cellSize = currentSizeDrMpD * Vector2.one;
            }
            await Task.Delay(20);
         }
      }
      
      private void MoveAnimaPads(bool isSideAOrB)
      {
         if (isSideAOrB)
         {
            _currentXPos = -_currentWidth * 0.5f;
         }
         else
         {
            _currentXPos = -_currentWidth * 1.5f;
         }
         
         rectTransformPadZone.DOLocalMoveX(_currentXPos, 0.3f).SetUpdate(false);
         GameManagerDrum.GaM.TimeBlockPanel(0.3f);
      }
   }
}
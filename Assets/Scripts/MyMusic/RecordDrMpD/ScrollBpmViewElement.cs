using DG.Tweening;
using DrumPad;
using TMPro;
using UnityEngine;

namespace MyMusic.RecordDrMpD
{
    internal class ScrollBpmViewElement : MonoBehaviour
    {
        private TextMeshProUGUI _textIndex;
        
        private float _stepElementPosition;
        
        private int _indexValue;
        private int _valueBpm;

        private ScrollBpmViewManager _scrollBpmViewManager;
        
        public void Initialization(ScrollBpmViewManager rootHolderManager, int indexValue)
        {
            _textIndex = GetComponent<TextMeshProUGUI>();
            
            _scrollBpmViewManager = rootHolderManager;
            _indexValue = indexValue;
            int baseElementNumber = _scrollBpmViewManager.BaseElementNumber;
            _stepElementPosition = _scrollBpmViewManager.OneStepSize * _indexValue;
            _valueBpm = baseElementNumber + baseElementNumber * _indexValue;
            _textIndex.text = _valueBpm.ToString();
            _scrollBpmViewManager.ScrollRootHolder.OnValueChangeNow += RuntimeChangeValue;
            _scrollBpmViewManager.ScrollRootHolder.OnValueChangeEnd += EndChangeValue;
            GameManagerDrum.GaM.MyMusicManager.RecordManager.OnSelectNewMusicPack += SelectNewStyle;
        }

        private void SelectNewStyle()
        {
            _textIndex.fontStyle = 
                _valueBpm == GameManagerDrum.GaM.MyMusicManager.RecordManager.MusicPackElement.DataContent.NormalBpm
                ? FontStyles.Underline
                : FontStyles.Normal;
        }
        
        private void RuntimeChangeValue(float valueChange)
        {
            Color textColorDrMpD = _textIndex.color;
            float calculatedValue = _stepElementPosition - valueChange;
            calculatedValue = Mathf.Abs(calculatedValue);
            calculatedValue = Mathf.Clamp01(calculatedValue);
            calculatedValue = 1 - calculatedValue * 50;
            textColorDrMpD.a = calculatedValue;
            _textIndex.color = textColorDrMpD;
            _textIndex.enabled = textColorDrMpD.a > 0;
        }
        private void EndChangeValue(float valueChangeDrMpD)
        {
            float calculatedValue = _scrollBpmViewManager.OneStepSize / 2;
            if (valueChangeDrMpD > _stepElementPosition - calculatedValue &&
                valueChangeDrMpD <= _stepElementPosition + calculatedValue)
            {
                _scrollBpmViewManager.RecordManager.FieldMusicRecord.BpmTemp = _valueBpm;
                DOTween.To(()=> _scrollBpmViewManager.ScrollRootHolder.ScrollBarElement.value, x=> _scrollBpmViewManager.ScrollRootHolder.ScrollBarElement.value = x, _stepElementPosition, 0.27f);
            }
        }
    }
}
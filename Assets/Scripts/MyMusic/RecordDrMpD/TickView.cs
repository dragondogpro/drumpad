using System;
using System.Collections.Generic;
using DrumPad;
using SettingsDrMpD;
using UnityEngine;

namespace MyMusic.RecordDrMpD
{
    internal class TickView : MonoBehaviour, IInitialization
    {
        public event Action OnSelectTickHandler;
        
        internal Dictionary<PadIndexName, List<TickRepository>> TickRepoMap => _tickRepoMap; 
        
        private RecordManager _recordManager;

        private readonly List<TickCellElement> _allTickCellElements = new List<TickCellElement>();

        private readonly Dictionary<PadIndexName, List<TickRepository>> _tickRepoMap =
            new Dictionary<PadIndexName, List<TickRepository>>();

        private FieldMusicRecord _musicRecord;

        private int _currentIndexDrMpD;
        private int _lastIndex;

        private bool _isWasInitDrMpD;

        public void InitializationDrMpD()
        {
            _recordManager = GameManagerDrum.GaM.MyMusicManager.RecordManager;
            
            _musicRecord = _recordManager.FieldMusicRecord;

            _allTickCellElements.AddRange(GetComponentsInChildren<TickCellElement>());
            
            foreach (var tickCellDrMpD in _allTickCellElements)
            {
                tickCellDrMpD.InitCellTick();

                if (_tickRepoMap.Count == 0)
                {
                    for (int i = 0; i < 24; i++)
                    {
                        _tickRepoMap.Add((PadIndexName)i, new List<TickRepository>()); 
                    }
                }

                for (int i = 0; i < 24; i++)
                {
                    _tickRepoMap[(PadIndexName)i].Add(tickCellDrMpD.PadsInfo[i]);
                }
            }

            _currentIndexDrMpD = 0;
            _lastIndex = _allTickCellElements.Count - 1;
            
            _isWasInitDrMpD = true;
            
            _musicRecord.OnTickHandler += MusicRecordOnOnTickHandler;
        }

        public void SelectTick()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            OnSelectTickHandler?.Invoke();
        }

        private void OnEnable()
        {
            if (!_isWasInitDrMpD)
            {
                return;
            }
            else
            {
                _musicRecord.OnTickHandler += MusicRecordOnOnTickHandler;
            }
        }

        private void OnDisable()
        {
            if (!_isWasInitDrMpD)
            {
                return;
            }
            else
            {
                _musicRecord.OnTickHandler -= MusicRecordOnOnTickHandler;
            }
            
        }

        private void MusicRecordOnOnTickHandler()
        {
            foreach (var tickCellDrMpD in _allTickCellElements)
            {
                tickCellDrMpD.InactiveTick();
            }

            _allTickCellElements[_currentIndexDrMpD].ActiveTick();
            _currentIndexDrMpD++;
            if (_currentIndexDrMpD > _lastIndex)
                _currentIndexDrMpD = 0;
        }
    }
}
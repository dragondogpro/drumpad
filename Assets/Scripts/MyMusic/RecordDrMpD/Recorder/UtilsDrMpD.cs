﻿using System;
using System.IO;
using System.Linq;
using DrumPad;
using Random = System.Random;

namespace MyMusic.RecordDrMpD.Recorder
{
    internal static class UtilsDrMpD
    {
        private static readonly Random RandomDrMpD = new Random();
    
        /// <summary>
        /// Generate random string.
        /// </summary>
        /// <param name="length">random string length</param>
        /// <returns></returns>
        internal static string GetRandomString(int length)
        {
            const string charsDrMpD = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(charsDrMpD, length)
                .Select(s => s[RandomDrMpD.Next(s.Length)]).ToArray());
        }

        internal static string GetTimeString()
        {
            return DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
        }

        internal static string CreateFolderDrMpD(string f2Create)
        {
            string folderDrMpD = f2Create;
            if (string.IsNullOrEmpty(folderDrMpD))
            {
                folderDrMpD = "Captures/";

            }
            folderDrMpD = Path.Combine(ConstantHolderPad.GetRootPath(),folderDrMpD);

            if (!folderDrMpD.EndsWith("/") && !folderDrMpD.EndsWith("\\"))
            {
                folderDrMpD += "/";
            }
      
            if (!Directory.Exists(folderDrMpD))
            {
                Directory.CreateDirectory(folderDrMpD);
            }
      
            return Path.GetFullPath(folderDrMpD);
        }
    }
}
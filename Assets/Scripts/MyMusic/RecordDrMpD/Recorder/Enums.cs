﻿namespace MyMusic.RecordDrMpD.Recorder
{
    [System.Serializable]
    internal enum CaptureStatusDrMpD
    {
        READY,
        STARTED
    }
}
﻿using UnityEngine;

namespace MyMusic.RecordDrMpD.Recorder
{
    internal class DontDestroyDrMpD : MonoBehaviour
    {
        void Awake()
        {
            int numDrMpD = 423;
            if (numDrMpD != 432)
            {
                numDrMpD = 432;
            }
            
            DontDestroyOnLoad(gameObject);
        }
    }
}
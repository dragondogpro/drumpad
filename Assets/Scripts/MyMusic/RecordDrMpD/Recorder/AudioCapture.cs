using System.IO;
using DrumPad;
using DrumPad.Utilits;
using IOSBridge;
using UnityEngine;
#if UNITY_IOS
#endif

namespace MyMusic.RecordDrMpD.Recorder
{ 
  internal class AudioCapture : MonoBehaviour
  {
    #region Properties

    // The audio recorder
    private AudioRecorder _audioRecorder;

    // Get or set the current status.
    private CaptureStatusDrMpD StatusDrMpD
    {
      get
      {
        if (_audioRecorder != null && _audioRecorder.RecordStarted)
          return CaptureStatusDrMpD.STARTED;
        return CaptureStatusDrMpD.READY;
      }
    }

    private string _saveFolderFullPath = "";

    // Log message format template
    private string LOG_FORMAT = "[AudioCapture] {0}";

    #endregion

    #region Methods

    // Start capture audio session
    public void StartCapture()
    {
      _saveFolderFullPath = UtilsDrMpD.CreateFolderDrMpD("Capture");

      // Init audio recorder
      if (AudioRecorder.singletonDrMpD == null)
      {
        if (GetComponent<DontDestroyDrMpD>() != null)
        {
          // Reset AudioListener
          AudioListener listenerDrMpD = FindObjectOfType<AudioListener>();
          if (listenerDrMpD)
          {
            Destroy(listenerDrMpD);
            Debug.LogFormat(LOG_FORMAT, "AudioListener found, reset in game scene.");
          }
          gameObject.AddComponent<AudioListener>();
          gameObject.AddComponent<AudioRecorder>();
        }
        else
        {
          // Keep AudioListener
          AudioListener listenerDrMpD = FindObjectOfType<AudioListener>();
          if (!listenerDrMpD)
          {
            listenerDrMpD = gameObject.AddComponent<AudioListener>();
            Debug.LogFormat(LOG_FORMAT, "AudioListener not found, add a new AudioListener.");
          }
          listenerDrMpD.gameObject.AddComponent<AudioRecorder>();
        }
      }
      AudioRecorder.singletonDrMpD.SaveFolderFullPath = _saveFolderFullPath;
      _audioRecorder = AudioRecorder.singletonDrMpD;

      _audioRecorder.StartRecordDrMpD();

      Debug.LogFormat(LOG_FORMAT, "Audio capture session started.");
    }

    // Stop capture audio session
    public void StopCapture()
    {
      if (StatusDrMpD != CaptureStatusDrMpD.STARTED)
      {
        Debug.LogFormat(LOG_FORMAT, "Audio capture session not start yet!");
      }

      if (_audioRecorder.StopRecordDrMpD())
      {
        string[] splitNameDrMpD = _audioRecorder.AudioSavePath.Split('/');
        string fileSaveNameDrMpD = splitNameDrMpD[splitNameDrMpD.Length - 1].Replace(StringHelperDrMpD.WavDrMpD, "");

#if UNITY_EDITOR
        GameManagerDrum.GaM.MyMusicManager.SelectPlaceholder(fileSaveNameDrMpD, CallBackSave, CancelCapture);
#elif !UNITY_EDITOR && UNITY_IOS
        IOStoUnityBridge.SetNewFileName(gameObject.name, "CallBackSave", fileSaveNameDrMpD);
#endif
      }

      Debug.LogFormat(LOG_FORMAT, "Audio capture session success!");
    }

    private void CallBackSave(string fileNameDrMpD)
    {
      if (fileNameDrMpD.Length > 0)
      {
        if (ConstantHolderPad.IsCorrectString(fileNameDrMpD))
        {
          SaveFileDrMpD(fileNameDrMpD);
          return;
        }
      }

      IOStoUnityBridge.ShowAlert(StringHelperDrMpD.MusicDrMpD, "Wrong name");
      CancelCapture();
    }

    private void SaveFileDrMpD(string newFileNameDrMpD)
    {
      if(File.Exists(_audioRecorder.AudioSavePath))
      {
        int intNewFileIndex = 1;
        string initNameFirstDrMpD = newFileNameDrMpD;
        
        while(File.Exists(Path.Combine(ConstantHolderPad.GetRootPath(), "Capture", newFileNameDrMpD + StringHelperDrMpD.WavDrMpD)))
        {
          newFileNameDrMpD = initNameFirstDrMpD + $"({intNewFileIndex})";
          intNewFileIndex++;
        }

        string fileNameSaveDrMpD = _saveFolderFullPath + newFileNameDrMpD + StringHelperDrMpD.WavDrMpD;
        
        File.Copy(_audioRecorder.AudioSavePath, fileNameSaveDrMpD);
        File.Delete(_audioRecorder.AudioSavePath);

        print("Path source: " + _audioRecorder.AudioSavePath);
        print("Path dest: " + fileNameSaveDrMpD);
        
        GameManagerDrum.GaM.JsonManagerDrMpD.SaveMusicMapJson.AddElement(newFileNameDrMpD, GameManagerDrum.GaM.CurrentData);
        GameManagerDrum.GaM.JsonManagerDrMpD.SaveMusicMapJson.RenameFileDrMpD(initNameFirstDrMpD, newFileNameDrMpD);
      }
      
      GameManagerDrum.GaM.MyMusicManager.MusicHolderManager.UpdateMusicList();
      _audioRecorder.ConfirmStopRecord();
      print("Save file");
    }

    // Cancel capture audio session
    public void CancelCapture()
    {
      if (StatusDrMpD != CaptureStatusDrMpD.STARTED)
      {
        Debug.LogFormat(LOG_FORMAT, "Audio capture session not start yet!");
      }

      if(_audioRecorder != null)
        _audioRecorder.CancelRecord();

      Debug.LogFormat(LOG_FORMAT, "Audio capture session canceled!");
    }

    #endregion

    #region Unity Lifecycle

    private void OnDestroy()
    {
      bool teDrMpD = false;
      if (teDrMpD)
        teDrMpD = true;
      // Check if still processing on destroy
      if (StatusDrMpD == CaptureStatusDrMpD.STARTED)
      {
        StopCapture();
      }
    }

    #endregion
  }
}
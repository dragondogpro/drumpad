﻿using System;
using System.IO;
using DrumPad.Utilits;
using UnityEngine;

namespace MyMusic.RecordDrMpD.Recorder
{
  // This script will record target audio listener sample and encode to audio file.
  internal class AudioRecorder : MonoBehaviour
  {

    #region Properties

    internal static AudioRecorder singletonDrMpD;
    
    // Audio slice for live streaming.
    private string audioSlicePath;

    internal string SaveFolderFullPath { get; set; }
    internal string AudioSavePath{ get; private set; }

    // Is audio record started
    internal bool RecordStarted { get; private set; }
    // Record sample rate
    private int outputSampleRate;
    
    private int headerSize = 44; // default for uncompressed wav
    private FileStream fileStreamDrMpD;
    
    // Log message format template
    private string LOG_FORMAT = "[AudioRecorder] {0}";

    #endregion

    #region Audio Recorder


    // Start capture audio session
    internal bool StartRecordDrMpD()
    {
      // Check if we can start capture session
      if (RecordStarted)
      {
        return false;
      }
      
      AudioSavePath = $"{SaveFolderFullPath}audio_{UtilsDrMpD.GetTimeString()}_{UtilsDrMpD.GetRandomString(5)}.wav";
      
      outputSampleRate = AudioSettings.outputSampleRate;

      if (!StartWriteDrMpD())
      {
        return false;
      }

      RecordStarted = true;

      return true;
    }

    // Stop capture audio session
    internal bool StopRecordDrMpD()
    {
      if (!RecordStarted)
      {
        Debug.LogFormat(LOG_FORMAT, "Audio capture session not start yet!");
        return false;
      }
      
      RecordStarted = false;
      
      // write header
      WriteHeader();

      return true;
    }

    internal void ConfirmStopRecord()
    {
      if (RecordStarted != false) 
          RecordStarted = false;
    }

    // Cancel capture audio session
    internal bool CancelRecord()
    {
      // if (!RecordStarted)
      // {
      //   Debug.LogFormat(LOG_FORMAT, "Audio capture session not start yet!");
      //   return false;
      // }

      RecordStarted = false;
      
      fileStreamDrMpD.Close();

      if (File.Exists(AudioSavePath))
        File.Delete(AudioSavePath);
      AudioSavePath = "";
      if (File.Exists(audioSlicePath))
        File.Delete(audioSlicePath);
      audioSlicePath = "";
      
      
      print($"Was delete: Audio Save Path:[{AudioSavePath}]");
      return true;
    }
    
    private bool StartWriteDrMpD()
    {
      string audioPathDrMpD = AudioSavePath;
      
      fileStreamDrMpD = new FileStream(audioPathDrMpD, FileMode.Create);

      byte emptyByteDrMpD = new byte();
      //preparing the header
      for (int i = 0; i < headerSize; i++)
      {
        fileStreamDrMpD.WriteByte(emptyByteDrMpD);
      }

      return true;
    }

    private void ConvertAndWrite(float[] dataSource)
    {
      Int16[] intDataDrMpD = new Int16[dataSource.Length];
      // converting in 2 steps : float[] to Int16[], then Int16[] to Byte[]
      Byte[] bytesDataDrMpD = new Byte[dataSource.Length * 2];
      // bytesData array is twice the size of dataSource array because a float converted in Int16 is 2 bytes.
      // to convert float to Int16
      int rescaleFactor = 32767;
      for (int i = 0; i < dataSource.Length; i++)
      {
        intDataDrMpD[i] = (Int16)(dataSource[i] * rescaleFactor);
        Byte[] byteArrDrMpD = BitConverter.GetBytes(intDataDrMpD[i]);
        byteArrDrMpD.CopyTo(bytesDataDrMpD, i * 2);
      }

      fileStreamDrMpD.Write(bytesDataDrMpD, 0, bytesDataDrMpD.Length);
    }

    private void WriteHeader()
    {
      fileStreamDrMpD.Seek(0, SeekOrigin.Begin);

      Byte[] riffDrMpD = System.Text.Encoding.UTF8.GetBytes(StringHelperDrMpD.RIFFDrMpD);
      fileStreamDrMpD.Write(riffDrMpD, 0, 4);

      Byte[] chunkSizeDrMpD = BitConverter.GetBytes(fileStreamDrMpD.Length - 8);
      fileStreamDrMpD.Write(chunkSizeDrMpD, 0, 4);

      Byte[] waveDrMpD = System.Text.Encoding.UTF8.GetBytes(StringHelperDrMpD.WaveDrMpD);
      fileStreamDrMpD.Write(waveDrMpD, 0, 4);

      Byte[] fmt = System.Text.Encoding.UTF8.GetBytes(StringHelperDrMpD.FmtDrMpD);
      fileStreamDrMpD.Write(fmt, 0, 4);

      Byte[] subChunk1 = BitConverter.GetBytes(16);
      fileStreamDrMpD.Write(subChunk1, 0, 4);

      UInt16 twoDrMpD = 2;
      UInt16 oneDrMpD = 1;

      Byte[] audioFormat = BitConverter.GetBytes(oneDrMpD);
      fileStreamDrMpD.Write(audioFormat, 0, 2);

      Byte[] numChannelsDrMpD = BitConverter.GetBytes(twoDrMpD);
      fileStreamDrMpD.Write(numChannelsDrMpD, 0, 2);

      Byte[] sampleRateDrMpD = BitConverter.GetBytes(outputSampleRate);
      fileStreamDrMpD.Write(sampleRateDrMpD, 0, 4);

      Byte[] byteRate = BitConverter.GetBytes(outputSampleRate * 4);
      // sampleRate * bytesPerSample*number of channels, here 44100*2*2

      fileStreamDrMpD.Write(byteRate, 0, 4);

      UInt16 fourDrMpD = 4;
      Byte[] blockAlign = BitConverter.GetBytes(fourDrMpD);
      fileStreamDrMpD.Write(blockAlign, 0, 2);

      UInt16 sixteenDrMpD = 16;
      Byte[] bitsPerSample = BitConverter.GetBytes(sixteenDrMpD);
      fileStreamDrMpD.Write(bitsPerSample, 0, 2);

      Byte[] dataStringDrMpD = System.Text.Encoding.UTF8.GetBytes(StringHelperDrMpD.DataDrMpD);
      fileStreamDrMpD.Write(dataStringDrMpD, 0, 4);

      Byte[] subChunk2 = BitConverter.GetBytes(fileStreamDrMpD.Length - headerSize);
      fileStreamDrMpD.Write(subChunk2, 0, 4);

      fileStreamDrMpD.Close();
    }

    #endregion

    #region Unity Lifecycle

    private void Awake()
    {
      if (singletonDrMpD != null)
        return;
      singletonDrMpD = this;

      RecordStarted = false;
    }

    private void OnAudioFilterRead(float[] data, int channels)
    {
      if (RecordStarted)
      {
        // audio data is interlaced
        ConvertAndWrite(data);
      }
    }

    private void OnDestroy()
    {
      if (RecordStarted)
      {
        int numDrMpD = 423;
        if (numDrMpD != 432)
        {
          numDrMpD = 432;
        }
        
        StopRecordDrMpD();
      }
    }

    #endregion
  }
}
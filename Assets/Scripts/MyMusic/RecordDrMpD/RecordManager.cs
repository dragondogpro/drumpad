using System;
using System.Collections.Generic;
using DrumPad;
using MyMusic.RecordDrMpD.Recorder;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.UI;

namespace MyMusic.RecordDrMpD
{
   public class RecordManager : MonoBehaviour, IInitialization, IManager
   {
      [SerializeField] private Text titleTextStyle;
      [SerializeField] private CanvasGroup trashButton;
      [SerializeField] private Toggle bpmBlock;
      [SerializeField] private Button closeRecordBlock;
      [SerializeField] private Button buttonLesson;
      [SerializeField] private Button buttonDeleteTrack;
      [SerializeField] private AudioCapture audioCapture;
      
      private RecordButton _recordButton;
      internal event Action OnSelectNewMusicPack;
      internal event Action OnResetTrackHandler;
      internal event Action<bool> OnSelectActivePads;

      private MusicPackElement _musicPackElement;
      internal MusicPackElement MusicPackElement => _musicPackElement;

      internal RecordRepository RecordRepository { get; private set; }
      internal TickView TickView { get; private set; }
      internal FieldMusicRecord FieldMusicRecord { get; private set; }
      internal ButtonSidePad ButtonSidePad { get; private set; }
      internal AudioCapture AudioCapture => audioCapture;
      internal RecordButton RecordButton => _recordButton;
      private PlayPanelElement PlayPanelElement { get;  set; }
      
      private readonly List<IInitialization> _allInitializations = new List<IInitialization>();

      private PadButtonSelect[] _padButtonSelects;

      public void InitializationDrMpD()
      {
         AudioListener.volume = 0.4f;

         RecordRepository = new RecordRepository();
         
         _allInitializations.AddRange(_padButtonSelects = GetComponentsInChildren<PadButtonSelect>());
         _allInitializations.Add(TickView = GetComponentInChildren<TickView>());
         _allInitializations.Add(FieldMusicRecord = GetComponentInChildren<FieldMusicRecord>());
         _allInitializations.Add(GetComponentInChildren<ScrollBpmViewManager>());
         _allInitializations.Add(PlayPanelElement = GetComponentInChildren<PlayPanelElement>());
         _allInitializations.Add(ButtonSidePad = GetComponentInChildren<ButtonSidePad>());
         _allInitializations.Add(_recordButton = GetComponentInChildren<RecordButton>());
         _allInitializations.AddRange(GetComponentsInChildren<CanvasListenerElement>());
         
         closeRecordBlock.onClick.AddListener(ClickCloseRecordBlockButton);
         buttonLesson.onClick.AddListener(ClickLessonButton);
         buttonDeleteTrack.onClick.AddListener(DeleteTrack);
         
         ContinueInit();
      }

      internal void SelectMusicStyle(MusicPackElement musicPackElement)
      {
         if(_musicPackElement != null)
            if(musicPackElement.DataContent.StyleName != _musicPackElement.DataContent.StyleName)
               DeleteTrack();
         
         _musicPackElement = musicPackElement;
         bpmBlock.isOn = false;
         GameManagerDrum.GaM.SetKindStyle(_musicPackElement.DataContent);
         GameManagerDrum.GaM.MyMusicManager.ListBlock.SetActive(false);
         
         titleTextStyle.text = _musicPackElement.DataContent.StyleName;
         
         PlayPanelElement.OnActivePadZone(false);
         SelectActivePads(false);
         
         OnSelectNewMusicPack?.Invoke();
         
         PadButtonSelect firstElementDrMpD = _padButtonSelects[0];

         RecordRepository.SelectPadElement(firstElementDrMpD.PadIndexName, firstElementDrMpD.MyAudioClip,
            firstElementDrMpD.SpriteOnButton, firstElementDrMpD.ColorOnButtonPad);
      }

      internal void DeleteTrack()
      {
         bool teDrMpD = false;
         if (teDrMpD)
            teDrMpD = true;
         
         OnResetTrackHandler?.Invoke();
      }
      
      private void SelectActivePads(bool isActiveDrMpD)
      {
         trashButton.alpha = !isActiveDrMpD ? 0.0f : 1.0f;
         trashButton.interactable = isActiveDrMpD;
         
         OnSelectActivePads?.Invoke(isActiveDrMpD);
      }

      internal void CloseRecordWindow()
      {
         gameObject.SetActive(false);
         audioCapture.CancelCapture();
         _recordButton.IsRecordNow = false;
         
         GameManagerDrum.GaM.AudioPlayerRecord.StopAll();
         GameManagerDrum.GaM.MyMusicManager.MusicHolderManager.UpdateMusicList();
         GameManagerDrum.GaM.DownPanelButtonManager.OpenCloseButton(true);
      }

      private void ContinueInit()
      {
         foreach (var initDrMpD in _allInitializations)
         {
            initDrMpD.InitializationDrMpD();
         }
         
         bpmBlock.onValueChanged.AddListener(ClickBpmToggle);
      }

      private void ClickBpmToggle(bool isActiveDrMpD)
      {
         SelectActivePads(isActiveDrMpD);
         PlayPanelElement.OnActivePadZone(isActiveDrMpD);
      }

      private void ClickLessonButton()
      {
         GameManagerDrum.GaM.MyMusicManager.ClickLessonsButton(true);
      }
      
      private void ClickCloseRecordBlockButton()
      {
         CloseRecordWindow();
         GameManagerDrum.GaM.MyMusicManager.LessonsManager.gameObject.SetActive(false);
         GameManagerDrum.GaM.MyMusicManager.ListBlock.SetActive(true);
      }
   }
}
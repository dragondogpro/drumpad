using System.Collections.Generic;
using DrumPad;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MyMusic.RecordDrMpD
{
    internal class TickCellElement : MonoBehaviour, IPointerDownHandler
    {
        internal List<TickRepository> PadsInfo => _padsInfo;
        
        private GameObject _borderActive;
        private GameObject _selectActive;
        private GameObject _pauseObject;
        
        private List<TickRepository> _padsInfo;
        
        private Image _tickImage;
        private Image _selectImage;

        private RecordManager _recordManager;

        private bool _isInit;
        
        private TickRepository _currentActiveTick;

        internal void InactiveTick()
        {
            if (!_isInit) return;
            _borderActive.SetActive(false);
        }

        internal void ActiveTick()
        {
            if (!_isInit) return;

            _borderActive.SetActive(true);
            
            foreach (var padDrMpD in _padsInfo)
            {
                if (padDrMpD.IsHaveTrack)
                {
                    if(padDrMpD.IsPauseTrack) continue;
                    
                    GameManagerDrum.GaM.AudioPlayerRecord.PlayAudioClipMusic(padDrMpD.ClipToPlay, padDrMpD.PadIndexName);
                }
            }
        }
        
        internal void InitCellTick()
        {
            _recordManager = GameManagerDrum.GaM.MyMusicManager.RecordManager;
            
            _tickImage = transform.GetChild(0).GetComponent<Image>();
            _selectImage = transform.GetChild(1).GetComponent<Image>();
            
            _borderActive = _tickImage.gameObject;
            _selectActive = _selectImage.gameObject;
            _pauseObject = _selectActive.gameObject.transform.GetChild(0).gameObject;
            
            _selectActive.SetActive(false);

            _padsInfo = new List<TickRepository>();
            for (int i = 0; i < 24; i++)
            {
                _padsInfo.Add(new TickRepository());
            }
            _isInit = true;

            _recordManager.RecordRepository.OnButtonSelect += OnChangePadIndex;
            _recordManager.OnResetTrackHandler += ResetTick;
        }

        private void OnEnable()
        {
            if(!_isInit) return;
            
            _recordManager.RecordRepository.OnButtonSelect += OnChangePadIndex;
            _recordManager.OnResetTrackHandler += ResetTick;
        }

        private void OnDisable()
        {
            if(!_isInit) return;
            
            _recordManager.RecordRepository.OnButtonSelect -= OnChangePadIndex;
            _recordManager.OnResetTrackHandler -= ResetTick;
        }
        
        public void OnPointerDown(PointerEventData eventData)
        {
            if (_currentActiveTick == null) return;

            _currentActiveTick.IsHaveTrack = !_currentActiveTick.IsHaveTrack;
            bool isHaveClip = _currentActiveTick.IsHaveTrack;
            _currentActiveTick.ClipToPlay = isHaveClip ? _recordManager.RecordRepository.SelectAudioClip : null;
            _currentActiveTick.ColorActive = isHaveClip ? 
                _recordManager.RecordRepository.PadColor : 
                Color.white;
            _currentActiveTick.PadIndexName = _recordManager.RecordRepository.IndexPadSelect;
            _selectActive.SetActive(isHaveClip);
            _selectImage.color = _currentActiveTick.ColorActive;
            _pauseObject.SetActive(_currentActiveTick.IsPauseTrack);
            _recordManager.TickView.SelectTick();
        }

        private void OnChangePadIndex(PadIndexName padIndexName, AudioClip clipOnPad)
        {
            _currentActiveTick = _padsInfo[(int) _recordManager.RecordRepository.IndexPadSelect];
            bool isActiveTrack = _currentActiveTick.IsHaveTrack;
            _selectImage.color = isActiveTrack ? _currentActiveTick.ColorActive : Color.white;
            _tickImage.sprite = _recordManager.RecordRepository.PadSprite;
            
            _selectActive.SetActive(isActiveTrack);
            _pauseObject.SetActive(_currentActiveTick.IsPauseTrack);
        }

        private void ResetTick()
        {
            foreach (var tickRepositoryDrMpD in _padsInfo)
            {
                tickRepositoryDrMpD.ColorActive = Color.white;
                tickRepositoryDrMpD.ClipToPlay = null;
                tickRepositoryDrMpD.IsHaveTrack = false;
                tickRepositoryDrMpD.IsPauseTrack = false;
            }

            _pauseObject.SetActive(false);
            _selectActive.SetActive(false);
            _selectImage.color = Color.white;
        }
    }
    
    internal class TickRepository
    {
        public bool IsHaveTrack;
        public bool IsPauseTrack;
        public AudioClip ClipToPlay;
        public Color ColorActive;
        public PadIndexName PadIndexName;
    }
}
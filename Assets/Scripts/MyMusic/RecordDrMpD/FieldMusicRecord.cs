using System;
using System.Collections;
using DrumPad;
using UnityEngine;
using UnityEngine.UI;

namespace MyMusic.RecordDrMpD
{
    internal class FieldMusicRecord : MonoBehaviour, IInitialization
    {
        [SerializeField] private Text textBpmTemp;
        
        private bool _isWasInitDrMpD;
        
        private int _bpmTemp = 10;

        private float _tickPerMinute;

        protected internal event Action OnTickHandler;

        protected internal int BpmTemp
        {
            set
            {
                _bpmTemp = value;
                ChangeBpmTick(_bpmTemp);
            }
        }

        public void InitializationDrMpD()
        {
            ChangeBpmTick(_bpmTemp);
            
            _isWasInitDrMpD = true;
            
            GameManagerDrum.GaM.MyMusicManager.RecordManager.OnSelectNewMusicPack += OnSelectNewStyle;
            
            StartCoroutine(TickBpmLoopRoutine());
        }

        private void OnEnable()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            if(!_isWasInitDrMpD) return;
            
            StartCoroutine(TickBpmLoopRoutine());
        }

        private void OnDisable()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            if(!_isWasInitDrMpD) return;
            
            StopCoroutine(TickBpmLoopRoutine());
        }

        private void OnSelectNewStyle()
        {
            BpmTemp = GameManagerDrum.GaM.MyMusicManager.RecordManager.MusicPackElement.DataContent.NormalBpm;
        }
        
        private void ChangeBpmTick(int newBpmDrMpD)
        {
            _tickPerMinute = 60.0f / newBpmDrMpD / 4;
            textBpmTemp.text = $"{newBpmDrMpD} BPM";
        }

        IEnumerator TickBpmLoopRoutine()
        {
            OnTickHandler?.Invoke();
            while (_isWasInitDrMpD)
            {
                yield return new WaitForSeconds(_tickPerMinute);
                OnTickHandler?.Invoke();
            }
        }
    }
}
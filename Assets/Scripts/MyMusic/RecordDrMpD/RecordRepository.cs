using System;
using SettingsDrMpD;
using UnityEngine;

namespace MyMusic.RecordDrMpD
{
    internal class RecordRepository
    { 
        internal event Action<PadIndexName, AudioClip> OnButtonSelect;
        internal PadIndexName IndexPadSelect { get; private set; }
        internal AudioClip SelectAudioClip { get; private set; }
        internal Sprite PadSprite { get; private set; }
        internal Color PadColor { get; private set; }

        internal void SelectPadElement(PadIndexName padIndexDrMpD, AudioClip clipSelect, Sprite padSprite, Color padColor)
        {
            IndexPadSelect = padIndexDrMpD;
            SelectAudioClip = clipSelect;
            PadSprite = padSprite;
            PadColor = padColor;

            OnButtonSelect?.Invoke(padIndexDrMpD, clipSelect);
        }
    }
}
using DrumPad;
using UnityEngine;

namespace MyMusic.RecordDrMpD
{
    internal class CanvasListenerElement : MonoBehaviour, IInitialization
    {
        private CanvasGroup _canvasGroupPadListener;
        
        public void InitializationDrMpD()
        {
            _canvasGroupPadListener = GetComponent<CanvasGroup>();

            GameManagerDrum.GaM.MyMusicManager.RecordManager.OnSelectActivePads += OnSelectPadElement;
        }

        private void OnSelectPadElement(bool isActivePadDrMpD)
        {
            _canvasGroupPadListener.blocksRaycasts = isActivePadDrMpD;
            _canvasGroupPadListener.interactable = isActivePadDrMpD;
            _canvasGroupPadListener.alpha = isActivePadDrMpD ? 1.0f : 0.0f;
        }
    }
}
using System.Collections;
using DrumPad;
using UnityEngine;
using UnityEngine.UI;

namespace MyMusic.RecordDrMpD
{
    public class RecordButton : MonoBehaviour, IInitialization
    {
        [SerializeField] private Sprite[] recordSprites;

        private Button _buttonClickRecord;
        private Image _imageRecord;

        private bool _isRecordNow;

        internal bool IsRecordNow
        {
            get => _isRecordNow;

            set
            {
                _isRecordNow = value;

                _imageRecord.sprite = _isRecordNow ? recordSprites[0] : recordSprites[1];
            }
        }

        public void InitializationDrMpD()
        {
            _buttonClickRecord = GetComponentInChildren<Button>();
            _imageRecord = _buttonClickRecord.image;

            _buttonClickRecord.onClick.AddListener(ChangeStatusRecord);
        }

        private void ChangeStatusRecord()
        {
            if (IsRecordNow)
            {
                GameManagerDrum.GaM.MyMusicManager.RecordManager.AudioCapture.StopCapture();
            }
            else
            {
                // _buttonClickRecord.enabled = false;
                // StartCoroutine(WaitRecordDelay());
                GameManagerDrum.GaM.MyMusicManager.RecordManager.AudioCapture.StartCapture();
            }

            IsRecordNow = !IsRecordNow;
        }

        private IEnumerator WaitRecordDelay()
        {
            yield return new WaitForSeconds(1f);
            _buttonClickRecord.enabled = true;
            yield break;
        }
    }
}
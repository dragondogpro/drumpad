using DrumPad;
using SettingsDrMpD;
using UnityEngine;
using UnityEngine.UI;

namespace MyMusic.RecordDrMpD
{
    internal class PadButtonSelect : MonoBehaviour, IInitialization
    {
        [SerializeField] private Color colorOnButtonPad;
        [SerializeField] private GameObject borderSelect;
        [SerializeField] private GameObject pauseSelect;
        [SerializeField] private GameObject playedSelect;
        [SerializeField] private PadIndexName padIndexName;

        private bool _isSelect;
        private bool _isWasInitDrMpD;
        private bool _isHaveActiveTrack;
        private bool _isPauseTrack;

        private Button _clickButtonPad;

        private RecordManager _recordManager;

        private AudioClip _myAudioClip;

        public Color ColorOnButtonPad => colorOnButtonPad;
        public PadIndexName PadIndexName => padIndexName;
        public AudioClip MyAudioClip => _myAudioClip;
        public Sprite SpriteOnButton => _clickButtonPad.image.sprite;

        public void InitializationDrMpD()
        {
            _recordManager = GameManagerDrum.GaM.MyMusicManager.RecordManager;

            _clickButtonPad = GetComponent<Button>();

            _isWasInitDrMpD = true;
            
            _recordManager.RecordRepository.OnButtonSelect += OnSelectButtonDrMpD;
            _recordManager.TickView.OnSelectTickHandler += OnSelectTick;
            _recordManager.OnResetTrackHandler += ResetAllTick;
            _recordManager.OnSelectNewMusicPack += OnSelectNewStyle;
            _clickButtonPad.onClick.AddListener(SelectButtonDrMpD);
            
            borderSelect.SetActive(_isSelect);
        }

        private void OnEnable()
        {
            if(!_isWasInitDrMpD) return;
            
            _recordManager.RecordRepository.OnButtonSelect += OnSelectButtonDrMpD;
            _recordManager.TickView.OnSelectTickHandler += OnSelectTick;
            _recordManager.OnResetTrackHandler += ResetAllTick;
            _clickButtonPad.onClick.AddListener(SelectButtonDrMpD);
        }

        private void OnDisable()
        {
            if(!_isWasInitDrMpD) return;
            
            _clickButtonPad.onClick.RemoveListener(SelectButtonDrMpD);
            _recordManager.RecordRepository.OnButtonSelect -= OnSelectButtonDrMpD;
            _recordManager.TickView.OnSelectTickHandler -= OnSelectTick;
            _recordManager.OnResetTrackHandler -= ResetAllTick;
        }

        private void SelectButtonDrMpD()
        {
            Sprite myColorDrMpD = _clickButtonPad.image.sprite;
            
            if (_isHaveActiveTrack)
            {
                if (_isSelect)
                {
                    _isPauseTrack = !_isPauseTrack;

                    foreach (var listTickRec in _recordManager.TickView.TickRepoMap[padIndexName])
                    {
                        listTickRec.IsPauseTrack = _isPauseTrack;
                    }

                    pauseSelect.SetActive(_isPauseTrack);
                    playedSelect.SetActive(!_isPauseTrack);
                }
            }
            else
            {
                GameManagerDrum.GaM.AudioPlayerRecord.PlayAudioClipMusic(_myAudioClip, padIndexName);
            }
            
            _recordManager.RecordRepository.SelectPadElement(padIndexName, _myAudioClip, myColorDrMpD, colorOnButtonPad);
        }

        private void OnSelectButtonDrMpD(PadIndexName indexDrMpD, AudioClip clipSelect)
        {
            _isSelect = indexDrMpD == padIndexName;
            OnSelectTick();
            borderSelect.SetActive(_isSelect);
        }

        private void OnSelectTick()
        {
            _isHaveActiveTrack = false;
            
            foreach (var listTickRec in _recordManager.TickView.TickRepoMap[padIndexName])
            {
                _isHaveActiveTrack |= listTickRec.IsHaveTrack;
            }
            
            playedSelect.SetActive(_isHaveActiveTrack);
            if(!_isHaveActiveTrack)
            {
                foreach (var listTickRec in _recordManager.TickView.TickRepoMap[padIndexName])
                {
                    listTickRec.IsPauseTrack = _isHaveActiveTrack;
                }
                
                _isPauseTrack = _isHaveActiveTrack;
                pauseSelect.SetActive(_isHaveActiveTrack);
            }
        }

        private void OnSelectNewStyle()
        {
            
            _myAudioClip = _recordManager.MusicPackElement.GetAudioClip(padIndexName);
        }

        private void ResetAllTick()
        {
            _isHaveActiveTrack = false;
            _isPauseTrack = false;
            
            playedSelect.SetActive(_isHaveActiveTrack);
            pauseSelect.SetActive(_isPauseTrack);
        }
    }
}
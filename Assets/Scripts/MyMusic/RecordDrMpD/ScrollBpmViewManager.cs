using DrumPad;
using UnityEngine;
using UnityEngine.Serialization;

namespace MyMusic.RecordDrMpD
{
    internal class ScrollBpmViewManager : MonoBehaviour, IInitialization, IManager
    {
        [FormerlySerializedAs("contentHolder")] [SerializeField] private Transform contentHolderDrMpD;
        
        [SerializeField] private int countOfElements = 260/5;
        [SerializeField] private int baseElementNumber = 5;
        [FormerlySerializedAs("emptyObject")] [SerializeField] private GameObject emptyObjectDrMpD;
        
        internal float OneStepSize { get; private set; }
        internal int BaseElementNumber => baseElementNumber;

        [SerializeField] private ScrollBpmViewElement prefabScroll;
        [SerializeField] private ScrollBpmViewRootHolder scrollRootHolder;
        
        internal ScrollBpmViewRootHolder ScrollRootHolder => scrollRootHolder;
        
        internal RecordManager RecordManager { get; private set; }

        public void InitializationDrMpD()
        {
            RecordManager = GameManagerDrum.GaM.MyMusicManager.RecordManager;
            
            scrollRootHolder.InitializationDrMpD();

            OneStepSize = 1.0f / (countOfElements - 1);
            
            InitElements();
        }

        private void InitElements()
        {
            Instantiate(emptyObjectDrMpD, contentHolderDrMpD);
            for (int i = 0; i < countOfElements; i++)
            {
                ScrollBpmViewElement prefabScrollElement = Instantiate(prefabScroll, contentHolderDrMpD);
                prefabScrollElement.Initialization(this, i);
            }
            
            RecordManager.OnSelectNewMusicPack += OnSelectNewStyle;
            
            Instantiate(emptyObjectDrMpD, contentHolderDrMpD);
        }

        private void OnSelectNewStyle()
        {
            float prefCurrentBpm = RecordManager.MusicPackElement.DataContent.NormalBpm - baseElementNumber;
            prefCurrentBpm /= baseElementNumber;
            prefCurrentBpm *= OneStepSize;
            ScrollRootHolder.ScrollBarElement.value = prefCurrentBpm;
        }
    }
}
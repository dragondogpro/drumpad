using System;
using DrumPad;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MyMusic.RecordDrMpD
{
    internal class ScrollBpmViewRootHolder : ScrollRect, IInitialization
    {
        internal event Action<float> OnValueChangeEnd;
        internal event Action<float> OnValueChangeNow;
        
        internal Scrollbar ScrollBarElement => _scrollBarElement;
        
        private Scrollbar _scrollBarElement;

        private Vector2 _velocity;

        private bool _isDragEnd;
        
        public void InitializationDrMpD()
        {
            _scrollBarElement = horizontalScrollbar;
            _scrollBarElement.onValueChanged.AddListener(OnChangeValueNow);
        }

        public override void OnInitializePotentialDrag(PointerEventData eventData)
        {
            base.OnInitializePotentialDrag(eventData);

            if (_isDragEnd)
            {
                _isDragEnd = false;
                OnValueChangeEnd?.Invoke(_scrollBarElement.value);
            }
        }
        
        public override void OnEndDrag(PointerEventData eventData)
        {
            base.OnEndDrag(eventData);
            _isDragEnd = true;
        }

        private void OnChangeValueNow(float valueNowDrMpD)
        {
            _velocity = velocity;
            OnValueChangeNow?.Invoke(valueNowDrMpD);

            if (_isDragEnd && Math.Abs(_velocity.x) <= 27)
            {
                _isDragEnd = false;
                OnValueChangeEnd?.Invoke(_scrollBarElement.value);
            }
        }
    }
}
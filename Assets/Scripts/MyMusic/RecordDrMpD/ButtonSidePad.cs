using System;
using DrumPad;
using UnityEngine;
using UnityEngine.UI;

namespace MyMusic.RecordDrMpD
{
    internal class ButtonSidePad : MonoBehaviour, IInitialization
    {
        internal event Action<bool> OnActiveSide;
        
        //Sides
        [SerializeField] private Image sidesImage;
        [SerializeField] private Sprite[] spriteOnSides;

        private CanvasGroup _canvasGroupPad;
        private Button _buttonActiveSide;

        private bool _isActiveSideA = true;

        public void InitializationDrMpD()
        {
            _canvasGroupPad = GetComponent<CanvasGroup>();
            _buttonActiveSide = GetComponent<Button>();
            
            _buttonActiveSide.onClick.AddListener(ClickOnSideButton);
            GameManagerDrum.GaM.MyMusicManager.RecordManager.OnSelectActivePads += OnActivePad;
        }
        
        private void OnActivePad(bool isActiveDrMpD)
        {
            if (!isActiveDrMpD)
            {
                _canvasGroupPad.alpha = 1.0f;
                _canvasGroupPad.interactable = true;
                _canvasGroupPad.blocksRaycasts = true;

                _isActiveSideA = true;
                OnActiveSide?.Invoke(_isActiveSideA);
                sidesImage.sprite = spriteOnSides[0];
            }
            else
            {
                _canvasGroupPad.alpha = 0.0f;
                _canvasGroupPad.interactable = false;
                _canvasGroupPad.blocksRaycasts = false;
            }
        }

        private void ClickOnSideButton()
        {
            _isActiveSideA = !_isActiveSideA;
            sidesImage.sprite = _isActiveSideA ? spriteOnSides[0] : spriteOnSides[1];

            OnActiveSide?.Invoke(_isActiveSideA);
        }
    }
}
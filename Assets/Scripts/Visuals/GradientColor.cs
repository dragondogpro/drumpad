using UnityEngine;

namespace DrumPad.Visuals
{
    public class GradientColor : MonoBehaviour
    {
        [SerializeField] private Gradient gradColor;
        [SerializeField, Range(0.0f, 1.0f)] private float gradientValue;

        [SerializeField] private SpriteRenderer spriteRend;

        private void Update()
        {
            spriteRend.color = gradColor.Evaluate(gradientValue);
        }
    }
}
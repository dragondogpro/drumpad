using DrumPad.TutorialBlock;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad
{
    public class FirstPanel : MonoBehaviour, IInitialization
    {
        [SerializeField] private TutorialManager tutorialManager;
        [SerializeField] private GameObject loadCanvas;
        [SerializeField] private Button iKnowButton;
        [SerializeField] private Button iDontKnowButton;
        
        public void InitializationDrMpD()
        {
            int isWasActive = PlayerPrefs.GetInt("FirstLoad", 0);
            
            iKnowButton.onClick.AddListener(ClickKnow);
            iDontKnowButton.onClick.AddListener(ClickDontKnow);
            
            if(isWasActive == 1)
                ClickKnow();
        }

        private void ClickKnow()
        {
            gameObject.SetActive(false);
            loadCanvas.SetActive(false);
            StatusUpdate();
            Destroy(loadCanvas);
        }
        
        private void ClickDontKnow()
        {
            tutorialManager.InitTutorial();
            StatusUpdate();
            if (gameObject.activeSelf || !gameObject.activeSelf)
                gameObject.SetActive(false);
        }

        private void StatusUpdate() => PlayerPrefs.SetInt("FirstLoad", 1);
    }
}
using UnityEngine;

namespace DrumPad
{
    public class WindowPadManager : MonoBehaviour, IInitialization, IManager
    {
        [SerializeField] private GameObject libraryWindow;
        [SerializeField] private GameObject myMusicWindow;
        [SerializeField] private GameObject moreWindow;
        
        public void InitializationDrMpD()
        {
            int kkDrMpD = 431;
            if (kkDrMpD == int.Parse("321540"))
                kkDrMpD--;
            
            GlobalEventSystem.OnClickButtonWindow += OnSelectPadWindow;
        }

        private void OnSelectPadWindow(WindowEnumPad windowEnumPad)
        {
            libraryWindow.SetActive(false);
            myMusicWindow.SetActive(false);
            moreWindow.SetActive(false);
            
            switch (windowEnumPad)
            {
                case WindowEnumPad.LIBRARY:
                    libraryWindow.SetActive(true);
                    break;
                
                case WindowEnumPad.MYMUSIC:
                    myMusicWindow.SetActive(true);
                    break;
                
                case WindowEnumPad.OTHERDrMpD:
                    moreWindow.SetActive(true);
                    break;
            }
        }
    }
}
using System.IO;
using DrumPad;
using DrumPad.Utilits;
using UnityEngine;

namespace SettingsDrMpD
{
    public class MusicPackElement
    {
        public DataContent DataContent { get; private set; }

        #region Pad One

        private AudioClip _drum1Pad1;
        private AudioClip _drum2Pad1;
        private AudioClip _drum3Pad1;

        private AudioClip _fxMelody1Pad1;
        private AudioClip _fxMelody2Pad1;

        private AudioClip _mainMelody1Pad1;
        private AudioClip _mainMelody2Pad1;
        private AudioClip _mainMelody3Pad1;
        private AudioClip _mainMelody4Pad1;

        private AudioClip _rythmMelody1Pad1;
        private AudioClip _rythmMelody2Pad1;
        private AudioClip _rythmMelody3Pad1;

        #endregion

        #region Pad Two
    
        private AudioClip _drum1Pad2;
        private AudioClip _drum2Pad2;
        private AudioClip _drum3Pad2;

        private AudioClip _fxMelody1Pad2;
        private AudioClip _fxMelody2Pad2;

        private AudioClip _mainMelody1Pad2;
        private AudioClip _mainMelody2Pad2;
        private AudioClip _mainMelody3Pad2;
        private AudioClip _mainMelody4Pad2;

        private AudioClip _rythmMelody1Pad2;
        private AudioClip _rythmMelody2Pad2;
        private AudioClip _rythmMelody3Pad2;
    
        #endregion
    
        public void InitElementBlock(DataContent dataContentDrMpD)
        {
            if (dataContentDrMpD != null) 
                DataContent = dataContentDrMpD;
            
            LoadMusicClips();
        }
    
        public AudioClip GetAudioClip(PadIndexName padIndexName)
        {
            AudioClip resultClipDrMpD = null;
            switch (padIndexName)
            {
                case PadIndexName.Drum1Pad1:
                    resultClipDrMpD = _drum1Pad1;
                    break;
                case PadIndexName.Drum2Pad1:
                    resultClipDrMpD = _drum2Pad1;
                    break;
                case PadIndexName.Drum3Pad1:
                    resultClipDrMpD = _drum3Pad1;
                    break;
                case PadIndexName.FxMelody1Pad1:
                    resultClipDrMpD = _fxMelody1Pad1;
                    break;
                case PadIndexName.FxMelody2Pad1:
                    resultClipDrMpD = _fxMelody2Pad1;
                    break;
                case PadIndexName.MainMelody1Pad1:
                    resultClipDrMpD = _mainMelody1Pad1;
                    break;
                case PadIndexName.MainMelody2Pad1:
                    resultClipDrMpD = _mainMelody2Pad1;
                    break;
                case PadIndexName.MainMelody3Pad1:
                    resultClipDrMpD = _mainMelody3Pad1;
                    break;
                case PadIndexName.MainMelody4Pad1:
                    resultClipDrMpD = _mainMelody4Pad1;
                    break;
                case PadIndexName.RythmMelody1Pad1:
                    resultClipDrMpD = _rythmMelody1Pad1;
                    break;
                case PadIndexName.RythmMelody2Pad1:
                    resultClipDrMpD = _rythmMelody2Pad1;
                    break;
                case PadIndexName.RythmMelody3Pad1:
                    resultClipDrMpD = _rythmMelody3Pad1;
                    break;
            
                case PadIndexName.Drum1Pad2:
                    resultClipDrMpD = _drum1Pad2;
                    break;
                case PadIndexName.Drum2Pad2:
                    resultClipDrMpD = _drum2Pad2;
                    break;
                case PadIndexName.Drum3Pad2:
                    resultClipDrMpD = _drum3Pad2;
                    break;
                case PadIndexName.FxMelody1Pad2:
                    resultClipDrMpD = _fxMelody1Pad2;
                    break;
                case PadIndexName.FxMelody2Pad2:
                    resultClipDrMpD = _fxMelody2Pad2;
                    break;
                case PadIndexName.MainMelody1Pad2:
                    resultClipDrMpD = _mainMelody1Pad2;
                    break;
                case PadIndexName.MainMelody2Pad2:
                    resultClipDrMpD = _mainMelody2Pad2;
                    break;
                case PadIndexName.MainMelody3Pad2:
                    resultClipDrMpD = _mainMelody3Pad2;
                    break;
                case PadIndexName.MainMelody4Pad2:
                    resultClipDrMpD = _mainMelody4Pad2;
                    break;
                case PadIndexName.RythmMelody1Pad2:
                    resultClipDrMpD = _rythmMelody1Pad2;
                    break;
                case PadIndexName.RythmMelody2Pad2:
                    resultClipDrMpD = _rythmMelody2Pad2;
                    break;
                case PadIndexName.RythmMelody3Pad2:
                    resultClipDrMpD = _rythmMelody3Pad2;
                    break;
            }

            return resultClipDrMpD;
        }
        
        private void LoadMusicClips()
        {
            string pathToMusicPackDrMpD = Path.Combine(DownloadMasterPad.PathToFolder(DataContent), "Side A/");
            _drum1Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.TenDrMpD);
            _drum2Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.ElevenDrMpD);
            _drum3Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.TwelveDrMpD);
        
            _fxMelody1Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.FourDrMpD);
            _fxMelody2Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.FiveDrMpD);
        
            _mainMelody1Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.OneDrMpD);
            _mainMelody2Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.TwoDrMpD);
            _mainMelody3Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.ThreeDrMpD);
            _mainMelody4Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.SixDrMpD);
        
            _rythmMelody1Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.SevenDrMpD);
            _rythmMelody2Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.EightDrMpD);
            _rythmMelody3Pad1 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.NineDrMpD);
        
            pathToMusicPackDrMpD = Path.Combine(DownloadMasterPad.PathToFolder(DataContent), "Side B/");
        
            _drum1Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.TenDrMpD);
            _drum2Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.ElevenDrMpD);
            _drum3Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.TwelveDrMpD);
        
            _fxMelody1Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.FourDrMpD);
            _fxMelody2Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.FiveDrMpD);
        
            _mainMelody1Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.OneDrMpD);
            _mainMelody2Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.TwoDrMpD);
            _mainMelody3Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.ThreeDrMpD);
            _mainMelody4Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.SixDrMpD);
        
            _rythmMelody1Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.SevenDrMpD);
            _rythmMelody2Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.EightDrMpD);
            _rythmMelody3Pad2 = LoadAudioClip(pathToMusicPackDrMpD + StringHelperDrMpD.NineDrMpD);
        }

        private AudioClip LoadAudioClip(string pathToLoadDrMpD)
        {
            return WavUtility.ToAudioClip(pathToLoadDrMpD + StringHelperDrMpD.WavDrMpD);
        }
    }
}

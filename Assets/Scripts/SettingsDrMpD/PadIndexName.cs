namespace SettingsDrMpD
{
    public enum PadIndexName
    {
        MainMelody1Pad1,
        MainMelody2Pad1,
        MainMelody3Pad1,
        FxMelody1Pad1,
        FxMelody2Pad1,
        MainMelody4Pad1,
        RythmMelody1Pad1,
        RythmMelody2Pad1,
        RythmMelody3Pad1,
        Drum1Pad1,
        Drum2Pad1,
        Drum3Pad1,
        
        MainMelody1Pad2,
        MainMelody2Pad2,
        MainMelody3Pad2,
        FxMelody1Pad2,
        FxMelody2Pad2,
        MainMelody4Pad2,
        RythmMelody1Pad2,
        RythmMelody2Pad2,
        RythmMelody3Pad2,
        Drum1Pad2,
        Drum2Pad2,
        Drum3Pad2
    }
}
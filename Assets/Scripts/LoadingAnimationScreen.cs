using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace DrumPad
{
    public class LoadingAnimationScreen : MonoBehaviour
    {
        private Image[] _imagesArrayAnima;

        private void Awake()
        {
            bool teDrMpD = false;
            if (teDrMpD)
                teDrMpD = true;
            
            _imagesArrayAnima = GetComponentsInChildren<Image>();
            StartCoroutine(AnimaGridRoutine());
        }

        private IEnumerator AnimaGridRoutine()
        {
            while (true)
            {
                int[] indexElements = new int[_imagesArrayAnima.Length];

                int randomCountElements = Random.Range(2, 5);
                for (int i = 0; i < randomCountElements; i++)
                {
                    int randomIndexDrMpD = Random.Range(0, indexElements.Length);

                    if(indexElements[randomIndexDrMpD] == 0)
                        indexElements[randomIndexDrMpD] = 1;
                    else
                    {
                        randomIndexDrMpD = Random.Range(0, indexElements.Length);
                        
                        while (indexElements[randomIndexDrMpD] != 0)
                        {
                            randomIndexDrMpD = Random.Range(0, indexElements.Length);
                        }
                    }
                }

                for (int i = 0; i < _imagesArrayAnima.Length; i++)
                {
                    _imagesArrayAnima[i].color = indexElements[i] == 0 ? Color.white : new Color(0.2f, 0.2f, 0.2f);
                }

                yield return new WaitForSecondsRealtime(0.5f);
            }
        }
    }
}
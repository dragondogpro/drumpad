# Drum Pad - BeatMaker

## Getting started
Привет, скорее всего проект не до конца был доделан, так как наш саунд дизайнер был занят)

И поэтому мне пришлось некоторые штуки упростить.
В проекте есть уроки и сейчас они общие на все стиле, но в идеале должны быть под каждый стиль свои.
И еще есть небольшие траблы с некоторыми стилями, то не правильная кодировка, и поэтому звук не правильно считывается и шумит,
то трабла с звуковым файлом и он не проигрывается, ну или просто отсутсвуют файлы для стиля. 
Все это поправимо, не переживай, но стоит дождаться саунд дизайнера и вместе с ним проделать работу над устранением ошибок)

## Styles:
Все референсы к стилям храняться в файле MusicStyles.json
Его структура довольно проста
Для примера возмем один блок
```
{
  "AllStyles": [
    {
      "KindOfStyle": "Hip-Hop", //сюда прописывается тип музыки
      "Styles": [               //сюда все подтипы
        "Hip-Hop 10s/140",      //каждый подтип несет в себе название и его нормальный БПМ
        "Hip-Hop 90s/90"
      ]
    },
    {
      "KindOfStyle": "Drum And Bass",
      "Styles": [
        "DnB Rollerz/174",
        "Iiquid DNB/170",
        "Neurofunk/174",
        "Neurofunk vol 2/174",
        "Neurofunk vol 3/172",
        "Old school DNB/175",
        "Raggae jungle/174",
```

В дальнейшем просто нужно будет добавить недостающие стиле в этот файл и блоки на сервер

### Music Block
Каждый блок состоит из нескольких файлов
```
Side A.zip  // блок музыки стороны А на паде
Side B.zip  // блок музыки стороны B на паде
```
Картинка к каждому блоку можно найти в папке проекта по пути

```
Assets/2D/SpritesForStyles
```
Там находятся все картинки для каждого стиля, они будут подтягиватся по названию и индексу
Соотвественно название блока и индекс по порядку как идет в файле MusicStyles.json
Они упаковываются в атлас, дабы не 1 DrawCall вместо 70)

## Lessons:

В данным момент все уроки идут из файла LessonsAll.json
Но нужно логика так была задумана, чтобы брать файл у каждого урока отдельно, который бы хранился в папке стиля

Подгрузку файла урока можно поменять в файле по пути 
```
Scripts/MyMusic/Lessons/LessonsManager.cs
```
У данного файла (класса) есть метод (на 55 строке)
```
public void SelectLessonsWindow(bool isActive);
```
В котором можно заменить строку
```
string pathToLessons = Path.Combine(ConstantHolderPad.GetRootPath(), "MusicStyles","LessonsAll.json");
```
На строку
```
string pathToLessons = Path.Combine(ConstantHolderPad.GetRootPath(), "MusicStyles", musPack.DataContent.KindName, musPack.DataContent.StyleName, "lessons.json");
```
И файл будет браться из папки стиля, при условии что ты его туда скачаешь.
Это можно будет сделать легко, просто изменив метод в файле

```
Scripts/JsonBlock/JsonManager.cs

CallBackLoadJson(string jsonFile);
```
На вот такой:

```
        private async void CallBackLoadJson(string jsonFile)
        {
            JsonStyleList = JsonUtility.FromJson<JsonStyleList>(jsonFile);
            
            List<Task> treadPool = new List<Task>();
            
            foreach (var styleList in JsonStyleList.AllStyles)
            {
                DataElements dataContents = new DataElements();

                int indexCount = 1;
                
                foreach (var style in styleList.Styles)
                {
                    string[] dataSplit = style.Split('/');
                    int normalBpm = 0;
                    if (int.TryParse(dataSplit[^1], out int result))
                        normalBpm = result;
                
                    DataContent dataContent = new DataContent
                    {
                        KindName = styleList.KindOfStyle,
                        StyleName = dataSplit[0],
                        NormalBpm = normalBpm
                    };

                    dataContents.DataMap.Add(dataSplit[0], dataContent);
                    
                    dataContent.SpriteElement =
                         GameManagerDrum.GaM.GetSpriteFromAtlas(dataContent.KindName, indexCount);
                    
                    indexCount++;
     
                    if (!DownloadMasterPad.FileExist(dataContent, "lessons.json"))
                    {
                        treadPool.Add(DownloadMasterPad.LoadTextFile(dataContent, "lessons.json"));
                    }
                }

                if(!GameManagerDrum.GaM.DataContentsMap.ContainsKey(styleList.KindOfStyle))
                    GameManagerDrum.GaM.DataContentsMap.Add(styleList.KindOfStyle, dataContents);
            }

            await Task.WhenAll(treadPool.ToArray());

            ContinueLoadJsons();
        }
```

Еще по поводу уроков:
Чтобы их сделать нужно создать файл с названием lessons.json и положить его в папку нужного стиля

```
{
  "LessonsDictionary":
  {
    "0": // Индекс урока (начинается с нулевого как в массиве)
    {
      "1":[9], // вот тут Key - это тик на таймлайне, 
               //а Value - это индекс звука который будет проигрываться 
               //(тут тоже индекс минус 1 записывай) 12 звуков 0-11
      "3":[10],
      "5":[11],
      "7":[10],
      "9":[9],
      "11":[10],
      "13":[11],
      "15":[10],
      "17":[9],
      "19":[10],
      "21":[11],
      "23":[]
    },
    "1":
    {
      "1":[6],
      "17":[7],
      "33":[8],
      "49":[]
    },
    "2":
    {
      "1":[0, 3],
      "9":[4],
      "17":[1, 3],
      "25":[4],
      "33":[2, 3],
      "41":[4],
      "49":[]
    },
    "3":
    {
      "1":[9, 0],
      "5":[11],
      "9":[9],
      "13":[11],
      "17":[9, 1],
      "21":[11],
      "25":[9],
      "29":[11],
      "33":[]
    },
    "4":
    {
      "1":[0, 6, 9],
      "5":[11],
      "9":[9],
      "13":[11, 7],
      "17":[9],
      "21":[11],
      "25":[9],
      "29":[8, 11],
      "33":[9, 1],
      "37":[11],
      "41":[9],
      "45":[11],
      "49":[]
    }
  }
}
```
## Other:

Нужно будет перед релизом изменить еще одну штуку,
а именно поменять айдишник в методе ShareApp (сейчас там тестовый приложения гугла:)

Его можно найти тут:
```
Scripts/MoreBlock/ShareButton.cs

URL_SHARE_APP; // сюда пропиши нужный айдишник
(вот тут нужно будет подумать где его достать, но вангуй, что где-то в консоле)
```

Еще правильную иерархию файло можно глянуть на сервере амазона
А сам пак все на данный момент подготовленых файло лежит на моем компе
По пути:
```
/Users/editor/UnityAll/Drum Pad - BeatMaker ALL/Drum Pad Blocks
```
Хотя этот блок лежит на сервере

Peace.Love.DrumPad:)